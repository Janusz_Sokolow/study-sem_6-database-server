(function () {
    'use strict';
    App
            .directive('questionStars', function () {
                return {
                    templateUrl: 'shared/questionnaire/questionnaire-stars.html',
                    controller: QuestionnareStartController,
                    scope: {
                        countOfOptions: "=",
                        label: "@",
                        data: "=",
                        variable: "@"
                    }
                }
            })
            .directive('questionDescription', function () {
                return {
                    templateUrl: 'shared/questionnaire/questionnaire-description.html',
                    scope: {
                        label: "@",
                        data: "=",
                        variable: "@"
                    }
                }
            })
            .directive('questionBoolean', function () {
                return {
                    templateUrl: 'shared/questionnaire/questionnaire-boolean.html',
                    scope: {
                        label: "@",
                        data: "=",
                        variable: "@"
                    }
                }
            });

    QuestionnareStartController.$inject = ['$scope'];
    function QuestionnareStartController($scope) {
        console.log('INIT -- QuestionnareStartController', $scope.countOfOptions);

        $scope.setStar = setStar;
        $scope.isLessStar = isLessStar;


        $scope.options = [];
        for (var i = 0; i < parseInt($scope.countOfOptions); i++) {
            $scope.options.push(i);
        }

        function setStar(number) {
            $scope.data[$scope.variable] = $scope.options.length - parseInt(number);
        }
        function isLessStar(number) {
            return number >= $scope.options.length - $scope.data[$scope.variable];
        }
    }
})()