App
        .controller('NewsController', NewsController)
        .directive('news', function () {
            return {
                templateUrl: 'shared/news/news-view.html',
                controller: 'NewsController'
            }
        })
        .directive('newSimple', function () {
            return {
                templateUrl: '/shared/news/new-simple.html',
                scope: {
                    oneNew: "="
                }
            }
        })
        .directive('newCoord', function () {
            return {
                templateUrl: '/shared/news/new-coord.html',
                scope: {
                    oneNew: "="
                },
                resolve: ['Access', function (Access) {
                        return Access.hasRole('COORDINATOR');
                    }],
                controller: ['$scope', 'messageService', function ($scope, messageService) {

                        $scope.removeNew = removeNew;

                        function removeNew(nnew) {
                            console.log(" removeNew nnew ", nnew);
                            messageService.removeMessages(nnew.id);
                             nnew.removed = true;
                        }
                    }]
            }
        })

NewsController.$inject = ['$scope', '$uibModal', 'messageService'];
function NewsController($scope, $uibModal, messageService) {

    $scope.createNew = createNew;
    init();
    function init() {
        messageService.findMessages().then(function (messages) {
            $scope.news = messages;
            $scope.news.forEach(function (n, ind) {
                n.isOpen = ind < 3;
            });
        })
    }
    ;
    function createNew() {
        var modalInstance = $uibModal.open({
            templateUrl: 'shared/news/createModal.html',
            controller: 'CreateNewController'
        });
        modalInstance.result.then(function (nnew) {
            console.log("RESULT nnew ", nnew);
            messageService.createMessage(nnew).then(function (result) {
                console.log(" set  nnew ", result);
                init();
            })

        }, function (err) {
            console.log("RESULT err ", err);
        })
    }

}