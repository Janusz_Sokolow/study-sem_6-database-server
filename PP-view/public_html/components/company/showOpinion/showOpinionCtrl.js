(function () {
    'use strict';

    App.controller('ShowCompanyOpinionsController', ShowCompanyOpinionsController)

    ShowCompanyOpinionsController.$inject = ['$scope', 'companyService', 'QUESTIONNAIRE'];

    function ShowCompanyOpinionsController($scope, companyService, QUESTIONNAIRE) {
        console.log('INIT -- ShowCompanyOpinionsController');

        $scope.companies = [];
        $scope.selectedCompany = {};
        $scope.opinions;
        $scope.findOpinions = findOpinions;

        var order = ["DESCRIPTION", "STARS", "BOOLEAN"];


        init();

        function init() {
            loadAvailableCompanies();
            $scope.$watch('selectedCompany', function (newValue, oldValue) {
                if (newValue.id)
                    findOpinions(newValue);
            })
        }

        function findOpinions(company) {
            console.log("Szukam opinie dla firmy ", company);
            companyService.findCompanyStats(company.id).then(function (stats) {
                console.log("Znalazłem opinie dla firmy " + company.companyName + " -> ", stats);
                $scope.opinions = [];
                angular.forEach(QUESTIONNAIRE, function (question) {
                    var value;
                    if (question.type == "DESCRIPTION") {
                        value = stats[question.id];
                    } else {
                        var sum = 0;
                        if (question.type == "STARS") {
                            stats[question.id].forEach(function (v) {
                                sum += parseInt(v);
                            });
                            value = sum / stats[question.id].length;
                        } else {
                            stats[question.id].forEach(function (v) {
                                sum += v == "true" ? 1 : 0;
                            });
                            value = sum / stats[question.id].length * 100;
                        }
                    }
                    $scope.opinions.push({
                        key: question.id,
                        values: value,
                        type: question.type,
                        isOpen: false
                    });
                    $scope.opinions.sort(function (a, b) {
                        return order.indexOf(a.type) > order.indexOf(b.type);
                    })
                })
            });
        }


        function loadAvailableCompanies(selectCompany) {
            companyService.findCompanyAbleToApply().then(function (result) {
                $scope.companies = result;
            }, function (reply) {

            });
        }
        $scope.companies = [];
    }

})();