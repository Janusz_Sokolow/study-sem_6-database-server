App
        .controller('MainController', MainController)
        .controller('UnregistratedController', UnregistratedController);

MainController.$inject = ['$scope', '$state', 'UserProfile', 'Access', 'practiceService']

function MainController($scope, $state, UserProfile, Access, practiceService) {
    console.log("INIT == MainController");
    
    UserProfile.then(function(response){
        $scope.userName = response.username;
    });

    Access.hasRole('COORDINATOR').then(function (resp) {
        console.log(resp);
        $scope.isKoord = true;
        if (resp === Access.OK) {
            $state.go('.coordinator');
        }
    }, function (resp) {
        console.log("reject ", resp);
        if (resp === Access.UNAUTHORIZED) {
            $state.go('.unregistrated');
        } else if (resp === Access.FORBIDDEN) {
            $state.go('.student');
            practiceService.getPracticeOwn().then(function (reply) {
                if (Object.keys(reply).length === 0) {
                    $scope.practice = null;
                    console.log("YOU HAVE NO STARTED PRACTICE");
                } else {
                    $scope.practice = reply;
                    console.log("IT'S YOUR PRACTICE", reply);
                }
            });
        }
    });

}


UnregistratedController.$inject = ['$scope']
function UnregistratedController($scope) {
    console.log("INIT == UnregistratedController");

}