(function () {
    'use strict';

    App.controller('ShowPracticeController', ShowPracticeController)

    ShowPracticeController.$inject = ['practiceService', 'documentService', '$uibModal', '$stateParams', '$scope', '$timeout'];

    function ShowPracticeController(practiceService, documentService, $uibModal, $stateParams, $scope, $timeout) {
        var practiceId = $stateParams.practiceId || $scope.practice.ID;
        console.log('INIT -- ShowPracticeController  ' + practiceId);

        $scope.describe = describe;
        $scope.uploadFile = uploadFile;
        $scope.generateCard = generateCard;
        $scope.finishPractice = finishPractice;
        $scope.openBHPModal = openBHPModal;
        $scope.removeDocument = removeDocument;

        $scope.practice_Id = practiceId;

        if (!$scope.practice) {
            practiceService.getPractice(practiceId).then(function (practice) {
                $scope.practice = practice;
            });
        }

        function describe(practicePartID) {
            $scope.practicePartID = practicePartID;
            var modalInstance = $uibModal.open({
                templateUrl: 'components/practice/shared/describe-view.html',
                controller: 'DescribePracticeController',
                scope: $scope
            });

            modalInstance.result.then(function (finishData) {
                delete $scope.practicePartID;
                practiceService.getPractice(practiceId).then(function (practice) {
                    $scope.practice.descriptions = practice.descriptions;
                });
            }, function (err) {
                console.log("RESULT err ", err);
                delete $scope.practicePartID;
            });
        }
        function uploadFile(practicePartID) {
            var modalInstance = $uibModal.open({
                templateUrl: 'components/practice/shared/upload-view.html',
                controller: 'UploadDocumentController',
                scope: $scope
            });

            modalInstance.result.then(function (finishData) {
                delete $scope.practicePartID;
                $timeout(function () {
                    practiceService.getPractice(practiceId).then(function (practice) {
                        $scope.practice.descriptions = practice.descriptions;
                    });
                }, 2000);
            }, function (err) {
                console.log("RESULT err ", err);
                delete $scope.practicePartID;
            });
        }



        function generateCard() {
            practiceService.generatePracticeCard(practiceId).then(function (pdfName) {
                if (pdfName) {
                    console.log("window", window.location);

                    window.open(window.location.origin + "/" + pdfName, "_blank");
                }
            });
        }

        function finishPractice() {
            $uibModal.open({
                templateUrl: 'components/practice/finish/finish-practice-view.html',
                controller: "FinishPracticeController",
                size: 'lg',
                keyboard: false
            }).result.then(function (finishData) {
                console.log("RESULT finishData ", finishData);
                var clearObj = {};
                angular.forEach(finishData.lessonsSelected, function (lessonObj, key) {
                    clearObj[key] = lessonObj.description;
                });
                finishData.lessonsSelected = clearObj;

                practiceService.finishPractice(practiceId, $scope.practice.company_id,
                        finishData.lessonsSelected,
                        finishData.questionnaire)
                        .then(function () {
                            console.log(" set  finishData ", finishData);
                            practiceService.getPractice(practiceId).then(function (practice) {
                                $scope.practice.status = practice.status;
                            });
                            generateCard();
                        });

            }, function (err) {
                console.log("RESULT err ", err);
            })
        }

        function removeDocument(documentId, listIndex) {
            console.log("RESP ", documentId, listIndex);
            documentService.removeDocument(documentId).then(function (ok) {
                console.log("RESP ", ok);
                if (ok) {
                    $scope.practice.documents.splice(listIndex, 1);
                }
            });
        }

        function openBHPModal() {
            var modalInstance = $uibModal.open({
                templateUrl: 'components/practice/shared/BHP-view.html',
                controller: ['$scope', function ($scope) {
                        $scope.BHP = {
                            date: new Date(),
                            city: '',
                            employee: ''
                        };
                        $scope.cancel = function () {
                            modalInstance.dismiss();
                        }
                        $scope.submit = function () {
                            modalInstance.close({
                                date: $scope.BHP.date,
                                city: $scope.BHP.city,
                                employee: $scope.BHP.employee
                            });
                        }
                    }]
            });

            modalInstance.result.then(function (BHP) {
                console.log("RESULT BHP ", BHP);
                BHP.date = BHP.date.getTime();
                practiceService.setBHP(practiceId, BHP).then(function () {
                    console.log(" set  BHP ", BHP);
                    $scope.practice.bhpEmployee = BHP.employee;
                    $scope.practice.bhpCity = BHP.city;
                    $scope.practice.bhpDate = BHP.date;
                })

            }, function (err) {
                console.log("RESULT err ", err);
            });
        }
    }
})()