(function () {
    'use strict';

    App.controller('FinishPracticeController', FinishPracticeController)

    FinishPracticeController.$inject = ['$uibModalInstance', 'QUESTIONNAIRE', 'LESSONS', '$scope'];

    function FinishPracticeController($uibModalInstance, QUESTIONNAIRE, LESSONS, $scope) {
        console.log('INIT -- FinishPracticeController  ');

        $scope.stage = 1;
        $scope.cancel = cancel;
        $scope.submit = submit;
        $scope.questions = QUESTIONNAIRE;
        $scope.lessons = LESSONS;
        $scope.lessonsSelected = {};
        $scope.lessonSelected = lessonSelected;
        $scope.test = [];
        $scope.questionnaire = {
            questions: {}
        };
        $scope.finishData = {
            date: new Date(),
            opinionStart: 0,
            opinion: undefined
        };


        function cancel() {
            if (--$scope.stage == 0) {
                $uibModalInstance.dismiss();
            }
        }
        function submit() {

            if ($scope.stage++ >= 2) {
                $uibModalInstance.close({
                    lessonsSelected : $scope.lessonsSelected,
                    questionnaire: $scope.questionnaire.questions
                });
                console.log("END DATA==== ", {
                    lessonsSelected : $scope.lessonsSelected,
                    questionnaire: $scope.questionnaire.questions
                })
            }
        }

        function lessonSelected(lesson) {
            console.log("Lesson selected ", lesson);
            if (lesson.ticked) {
                if (!$scope.lessonsSelected[lesson.index]) {
                    $scope.lessonsSelected[lesson.index] = lesson;
                    $scope.lessonsSelected[lesson.index].active = true;
                } else {
                    $scope.lessonsSelected[lesson.index].active = true;
                }
            } else {
                $scope.lessonsSelected[lesson.index].active = false;
            }
        }
    }
})()