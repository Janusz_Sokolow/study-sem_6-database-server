(function () {
    'use strict';

    App.controller('ListPracticeController', ListPracticeController)

    ListPracticeController.$inject = ['$scope', '$uibModal', 'practiceService', 'FACULTIETIES', 'PRACTICE_STATUSES'];

    function ListPracticeController($scope, $uibModal, practiceService, FACULTIETIES, PRACTICE_STATUSES) {
        console.log('INIT -- ListPracticeController');

        $scope.rejectPractice = rejectPractice;
        $scope.aprovePractice = aprovePractice;

        $scope.filter = {
            dateFrom: undefined,
            dateTo: undefined,
            speciality: '',
            status: '',
            phrase: ''
        };

        $scope.availableFacultities = FACULTIETIES.all;
        $scope.availableStatuses = PRACTICE_STATUSES;

        (function () {
            practiceService.findPractice().then(function (result) {
                result.forEach(function (item) {
                    var dateElements = item.dateFrom.split("-");
                    item.dateFrom = new Date(
                            dateElements[0],
                            dateElements[1],
                            dateElements[2]
                            );
                })
                $scope.foundPractices = result;
            });
        })();


        function aprovePractice(practice) {
            practiceService.setStatus(practice.ID, "APPROVED").then(function (ok) {
                if (ok) {
                    practice.status = "APPROVED";
                }
            });
        }

        function rejectPractice(practice) {
            var modalInstance = $uibModal.open({
                templateUrl: 'components/practice/shared/message-description.html',
                controller: ['$scope', function ($scope) {
                        $scope.statuses = ["APPROVED", "REJECTED", "ARCHIVE"];
                        $scope.description = {
                            selectedStatus: '',
                            message: ''
                        }
                        $scope.cancel = function () {
                            modalInstance.dismiss();
                        }
                        $scope.submit = function () {
                            modalInstance.close({
                                newStatus: $scope.description.selectedStatus,
                                message: $scope.description.message
                            });
                        }
                    }]
            });
            modalInstance.result.then(function (result) {
                console.log("RESULT ", result);
                practiceService.setStatus(practice.ID, result.newStatus, result.message).then(function (ok) {
                    if (ok) {
                        practice.status = result.newStatus;
                    }
                });
            }, function (err) {
                console.error("ERR", err);
            })
        }
    }
})()