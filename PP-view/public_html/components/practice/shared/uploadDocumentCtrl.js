(function () {
    'use strict';

    App.controller('UploadDocumentController', UploadDocumentController)

    UploadDocumentController.$inject = ['$scope', '$rootScope', 'FILE_TYPES', '$uibModalInstance', 'practiceService', 'ValidationService'];

    function UploadDocumentController($scope, $rootScope, FILE_TYPES, $uibModalInstance, practiceService, ValidationService) {
        console.log('INIT - UploadDocumentController ', $scope);

        $scope.FILE_UPLOAD_BROADCAST_MSG = "FILE_UPLOAD" + $scope.practice_Id;
        $scope.FILE_TYPES = FILE_TYPES;
        $scope.files = {
            selected: [],
            type: ''
        };
        $scope.maxSize;

        $scope.$watch('files.type', function (newValue, oldValue) {
            console.log("Type changed -> " + newValue + " :: oldValue " + oldValue);
            $scope.maxSize = $scope.files.type == 'OTHER' ? 5 : 1;
            console.log($scope.maxSize);
            if ($scope.files.selected.length > $scope.maxSize) {
                $scope.files.selected.splice($scope.maxSize, $scope.files.selected.length - $scope.maxSize);
            }
        });

        (function init() {
        })();

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        }
        $scope.submit = function () {
            console.log('submit - UploadDocumentController ');

            $rootScope.$broadcast($scope.FILE_UPLOAD_BROADCAST_MSG,
                    {
                        files: $scope.files.selected,
                        practiceId: $scope.practice_Id,
                        type: $scope.files.type
                    }, sendData);
        }

        function sendData(response) {
            console.log('sendData - UploadDocumentController ', response);
            $uibModalInstance.close();
        }

    }
})()