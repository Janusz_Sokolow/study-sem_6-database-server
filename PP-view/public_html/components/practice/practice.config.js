(function () {
    App
            .directive('practiceStatus', function () {
                return {
                    templateUrl: "components/practice/shared/practice-status.html",
                    scope: {
                        status: "="
                    }
                }
            })
            .directive('practiceList', function () {
                return {
                    templateUrl: "components/practice/list/list-view.html",
                    controller: "ListPracticeController"
                }
            })
            .directive('practiceStart', function () {
                return {
                    templateUrl: "components/practice/start/start-view.html",
                    controller: "StartPracticeController",
                    scope: {
                        nickName: "="
                    }
                }
            })
            .directive('practiceShow', function () {
                return {
                    templateUrl: "components/practice/show/show-view.html",
                    controller: "ShowPracticeController",
                    scope: {
                        practice: "=",
                        practiceId: "="
                    }
                }
            });
})();