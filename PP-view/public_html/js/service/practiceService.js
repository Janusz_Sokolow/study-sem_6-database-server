

App.service('practiceService', ['eventbusMessageService', '$q', function (eventbusMessageService, $q) {
        return {
            startPractice: startPractice,
            findPractice: findPractice,
            getPractice: getPractice,
            getPracticeOwn: getPracticeOwn,
            setStatus: setStatus,
            setBHP: setBHP,
            finishPractice: finishPractice,
//            removePractice: removePractice,
            newPracticeDescription: newPracticeDescription,
            findPracticeDescriptions: findPracticeDescriptions,
            generatePracticeCard: generatePracticeCard
        }

        function startPractice(studentName, newPractice) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.start",
                data: {
                    companyId: newPractice.companyId,
//                    hours: newPractice.hours,
                    type: newPractice.type,
                    dateFrom: newPractice.dateFrom.getTime(),
                    dateTo: newPractice.dateTo.getTime(),
                    studentNickname: studentName
                }
            }).getValue('ok');
        }
        function setStatus(practiceId, newStatus, statusMessage) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.status",
                data: {
                    practiceId: practiceId,
                    status: newStatus,
                    statusMessage: statusMessage
                }
            }).getValue('ok');
        }
        function setBHP(practiceId, BHP) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.setBHP",
                data: {
                    practiceId: parseInt(practiceId),
                    BHPCity: BHP.city,
                    BHPDate: BHP.date,
                    BHPEmployee: BHP.employee
                }
            }).getValue('ok');
        }
        function finishPractice(practiceId, companyId, selectedLessons, questionnaire) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.finish",
                data: {
                    practiceId: parseInt(practiceId),
                    companyId: parseInt(companyId),
                    selectedLessons: selectedLessons,
                    questionnaire: questionnaire
                }
            }).getValue('ok');
        }

        function findPractice() {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.find"
            }).getValue('practiceList');
        }

        function getPractice(practiceId) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.show",
                data: {
                    practiceId: parseInt(practiceId)
                }
            }).getValue('practice');
        }

        function getPracticeOwn() {
            var deferred = $q.defer();
            eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.own"
            }, deferred).then(function (data) {
                console.log("SUCCESS - Prawidłowo pobrano praktykę", data);
                deferred.resolve(data.practice);
            });
            return deferred.promise;
        }

        function findPracticeDescriptions(practiceId) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.part.find",
                data: {
                    practiceId: parseInt(practiceId)
                }
            }).getValue('descriptionsList');
        }

//        function removePractice(practiceId) {
//            return eventbusMessageService.send({
//                address: "pl.gda.pg.mif.practice.practice.remove",
//                data: {id: practiceId}
//            }).getValue('ok');
//        }

        function newPracticeDescription(practiceId, description) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.part.describe",
                data: {
                    practiceId: parseInt(practiceId),
                    week: parseInt(description.week),
                    note: description.note,
                    skills: description.skills
                }
            }).getValue('ok');
        }

        function generatePracticeCard(practiceId) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.practice.generate",
                data: {
                    practiceId: parseInt(practiceId)
                }
            }).getValue('pdfSrc');
        }
    }]);