App.service('messageService', ['eventbusMessageService', '$q', function (eventbusMessageService, $q) {
        return {
            createMessage: createMessage,
            findMessages: findMessages,
            removeMessages: removeMessages
        }

        function createMessage(newMessage) {
            return  eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.message.create",
                data: {
                    title: newMessage.title,
                    content: newMessage.content
                }
            }).getValue('ok');
        }

        function findMessages() {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.message.find",
                data: {}
            }).getValue('messagesList')
        }
        function removeMessages(id) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.message.remove",
                data: {
                    id: id
                }
            }).getValue('ok')
        }
    }]);