//App.factory("$translateStaticFilesLoader", ["$q", "$http",
//    function (a, b) {
//        return function (c) {
//            if (!c || !angular.isString(c.prefix) || !angular.isString(c.suffix))
//                throw new Error("Couldn't load static files, no prefix or suffix specified!");
//            var d = a.defer();
//            return b({
//                url: [c.prefix, c.key, c.suffix].join(""),
//                method: "GET",
//                params: ""
//            }).success(function (a) {
//                d.resolve(a);
//            }).error(function () {
//                d.reject(c.key);
//            }), d.promise;
//        }
//    }]);

App.config(['$provide', function ($provide) {
        $provide.decorator('$q', function ($delegate) {
            var defer = $delegate.defer,
                    when = $delegate.when,
                    reject = $delegate.reject,
                    all = $delegate.all;

            function decoratePromise(promise) {

                var then = promise.then;

                // assigns the value given to .then on promise resolution to the given object under the given varName
                promise.getValue = function (propertyName) {
                    return promise.then(function (value) {
                        if (value[propertyName] !== undefined ) {
                            return value[propertyName];
                        } else {
                            return "NO DATA";
                        }
                    });
                };
                return promise;
            }

            $delegate.defer = function () {
                var deferred = defer();
                decoratePromise(deferred.promise);
                return deferred;
            };

            $delegate.when = function () {
                return decoratePromise(when.apply(this, arguments));
            };

            $delegate.reject = function () {
                return decoratePromise(reject.apply(this, arguments));
            };

            $delegate.all = function () {
                return decoratePromise(all.apply(this, arguments));
            };

            return $delegate;
        });
    }]);