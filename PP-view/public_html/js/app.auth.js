App
        .factory("Access", ["$q", "UserProfile",
            function ($q, UserProfile) {
                var Access = {
                    OK: 200,
                    UNAUTHORIZED: 401,
                    FORBIDDEN: 403,
                    hasRole: _hasRole,
                    hasAnyRole: _hasAnyRole,
                    isAnonymous: _isAnonymous,
                    isAuthenticated: _isAuthenticated
                };
                function _hasRole(role) {
                    var deferred = $q.defer();
                    UserProfile.then(function (userProfile) {
                        if (userProfile.$hasRole(role)) {
                            deferred.resolve(Access.OK);
                        } else if (userProfile.$isAnonymous()) {
                            deferred.reject(Access.UNAUTHORIZED);
                        } else {
                            deferred.reject(Access.FORBIDDEN);
                        }
                    });
                    return deferred.promise;
                }
                function _hasAnyRole(roles) {
                    var deferred = $q.defer();
                    UserProfile.then(function (userProfile) {
                        if (userProfile.$hasAnyRole(roles)) {
                            deferred.resolve(Access.OK);
                        } else if (userProfile.$isAnonymous()) {
                            deferred.reject(Access.UNAUTHORIZED);
                        } else {
                            deferred.reject(Access.FORBIDDEN);
                        }
                    });
                    return deferred.promise;
                }
                function _isAnonymous() {
                    var deferred = $q.defer();
                    UserProfile.then(function (userProfile) {
                        if (userProfile.$isAnonymous()) {
                            deferred.resolve(Access.OK);
                        } else {
                            deferred.reject(Access.FORBIDDEN);
                        }
                    });
                    return deferred.promise;
                }
                function _isAuthenticated() {
                    var deferred = $q.defer();
                    UserProfile.then(function (userProfile) {
                        if (userProfile.$isAuthenticated()) {
                            deferred.resolve(Access.OK);
                        } else {
                            deferred.reject(Access.UNAUTHORIZED);
                        }
                    });
                    return deferred.promise;
                }
                return Access;
            }])
        .factory("UserProfile", ["$q", "User",
            function ($q, User) {

                var userProfile = {};
                var fetchUserProfile = function () {
                    var deferred = $q.defer();
                    User.profile(function (response) {

                        for (var prop in userProfile) {
                            if (userProfile.hasOwnProperty(prop)) {
                                delete userProfile[prop];
                            }
                        }

                        deferred.resolve(angular.extend(userProfile, response, {
                            $refresh: fetchUserProfile,
                            $hasRole: function (role) {
                                return userProfile.containsRoles.indexOf(role) >= 0;
                            },
                            $hasAnyRole: function (roles) {
                                return !!userProfile.containsRoles.filter(function (role) {
                                    return roles.indexOf(role) >= 0;
                                }).length;
                            },
                            $isAnonymous: function () {
                                return userProfile.anonymous;
                            },
                            $isAuthenticated: function () {
                                return !userProfile.anonymous;
                            }

                        }));
                    });
                    return deferred.promise;
                };
                return fetchUserProfile();
            }])
        .factory("User", ["$resource",
            function ($resource) {
                console.log("TEST === USER");
                return $resource("/api/permissions", {}, {
                    profile: {
                        method: "GET"
                    }
                });
            }]);