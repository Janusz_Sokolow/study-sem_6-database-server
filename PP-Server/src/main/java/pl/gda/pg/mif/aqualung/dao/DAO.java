package pl.gda.pg.mif.aqualung.dao;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public abstract class DAO {
    
    private static final Logger log = LogManager.getLogger(DAO.class);

    protected static JsonObject collectResult(JsonArray rows, String... columns) {
        JsonObject result = new JsonObject();
        for (int i = 0, n = columns.length; i < n; i++) {
            String[] names = columns[i].split("\\.");
            result.put(names[names.length - 1], rows.getValue(i));
        }
        return result;
    }
}
