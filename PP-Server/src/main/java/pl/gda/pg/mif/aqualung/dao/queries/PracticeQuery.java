package pl.gda.pg.mif.aqualung.dao.queries;

import io.vertx.core.json.JsonArray;
import pl.gda.pg.mif.aqualung.consts.PracticeStatus;
import pl.gda.pg.mif.aqualung.dao.struct.Company;
import pl.gda.pg.mif.aqualung.dao.struct.Practice;
import pl.gda.pg.mif.aqualung.dao.struct.PracticePart;
import pl.gda.pg.mif.aqualung.dao.struct.Student;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class PracticeQuery extends Queries {

//    INSERT INTO bar (description, foo_id) VALUES
//    ( 'testing',     (SELECT id from foo WHERE type='blue') ),
//    ( 'another row', (SELECT id from foo WHERE type='red' ) );
    public static String startPractice(long dateFrom, long dateTo, String type, int companyId, String studentNickname) {
        return "INSERT INTO " + Practice.TABLE_NAME
                + "(" + colectColumns(Practice.DATE_FROM, Practice.DATE_TO, Practice.TYPE, Practice.STATUS, Practice.COMPANY_ID, Practice.STUDENT_PG_INDEX)
                + ") " + colectValues(convertLongToDate(dateFrom), convertLongToDate(dateTo), type, PracticeStatus.NEW.name(), companyId,
                        "( SELECT " + Student.PG_INDEX + " FROM " + Student.TABLE_NAME + where(Student.NICK_NAME, studentNickname) + ")");
    }

    public static String setStatus(long practiceId, PracticeStatus status, String message) {
        return "UPDATE " + Practice.TABLE_NAME
                + setFew(
                        new String[]{Practice.T_STATUS, Practice.T_MESSAGE},
                        new Object[]{status.name(), message})
                + where(Practice.T_ID, practiceId) + ";";
    }

    public static String setBHP(long practiceId, String BHPCity, long BHPDate, String BHPEmployee) {
        return "UPDATE " + Practice.TABLE_NAME
                + setFew(
                        new String[]{Practice.T_BHP_CITY, Practice.T_BHP_DATE, Practice.T_BHP_EMPLOYEE},
                        new Object[]{BHPCity, convertLongToDate(BHPDate), BHPEmployee})
                + where(Practice.T_ID, practiceId) + ";";
    }

    public static String finish(long practiceId) {
        return "UPDATE " + Practice.TABLE_NAME
                + set(Practice.STATUS, PracticeStatus.FINISHED.name())
                + where(Practice.T_ID, practiceId) + ";";
    }

    public static String addDescribePractice(long practiceId, int week, String note, JsonArray skills) {
        return "REPLACE INTO " + PracticePart.TABLE_NAME
                + "(" + colectColumns(PracticePart.PRACTICE_ID, PracticePart.STAGE, PracticePart.DESCRIPTION, PracticePart.SKILLS, PracticePart.CREATE_DATE)
                + ") " + colectValues(practiceId, week, note, skills.encode(), dateNow()) + ";";
    }

    public static String getDescribePractice(long practiceId, int week, String... columns) {
        return "SELECT " + colectColumns(columns) + " FROM " + PracticePart.TABLE_NAME
                + where(PracticePart.PRACTICE_ID, practiceId) + and(PracticePart.STAGE, week) + ";";
    }

    public static String findDescription(long practiceId, String... columns) {
        return "SELECT " + colectColumns(columns) + " FROM " + PracticePart.TABLE_NAME
                + where(PracticePart.PRACTICE_ID, practiceId) + ";";
    }

    public static String getPractice(long practiceId, String... columns) {
        return "SELECT " + colectColumns(columns) + " FROM " + PracticePart.TABLE_NAME
                + " RIGHT OUTER JOIN " + Practice.TABLE_NAME + on(PracticePart.T_PRACTICE_ID, Practice.T_ID)
                + where(Practice.T_ID, practiceId);
    }

    public static String getPracticeByStudentId(long studentId, String... columns) {
//        SELECT * FROM practicepart RIGHT OUTER JOIN practice ON practice.id = practicepart.practice_id;

//      SELECT * FROM practice RIGHT OUTER JOIN practicePart ON practicePart.practice_id=practice.id WHERE practice.student_pgIndex=-1494229388
        return "SELECT " + colectColumns(columns) + " FROM " + PracticePart.TABLE_NAME
                + " RIGHT OUTER JOIN " + Practice.TABLE_NAME + on(PracticePart.T_PRACTICE_ID, Practice.T_ID)
                + where(Practice.STUDENT_PG_INDEX, studentId);
    }

    public static String findPractice(String facultity, String... columns) {
        return "SELECT " + colectColumns(columns) + " FROM " + Practice.TABLE_NAME
                + " LEFT JOIN " + Company.TABLE_NAME + on(Practice.T_COMPANY_ID, Company.T_ID)
                + " LEFT JOIN " + Student.TABLE_NAME + on(Practice.T_STUDENT_PG_INDEX, Student.T_PG_INDEX)
                + where(Student.T_SPECIALITY, facultity)
                + " ORDER BY " + Practice.T_DATE_FROM;
    }

    public static String removePractice(int id) {
        return "DELETE FROM  " + Practice.TABLE_NAME
                + where(Practice.ID, id);
    }
}
