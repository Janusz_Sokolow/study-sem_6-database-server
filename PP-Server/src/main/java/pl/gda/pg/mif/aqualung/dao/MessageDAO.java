package pl.gda.pg.mif.aqualung.dao;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.UpdateResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.dao.queries.MessageQuery;
import pl.gda.pg.mif.aqualung.providers.DatabaseQueryProvider;
import pl.gda.pg.mif.aqualung.providers.DatabaseUpdateProvider;
import pl.gda.pg.mif.aqualung.dao.struct.Message;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class MessageDAO {

    private static final Logger log = LogManager.getLogger(MessageDAO.class);

    public static void insertMessage(Vertx vertx, String title, String content, String author, Handler<UpdateResult> handler) {
        log.debug("insertMessage :: Przygotowuję zapytanie");
        String sqlUpdate = MessageQuery.insertMessage(title, content, author);

        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }

    public static void getMessage(Vertx vertx, int messageId, Handler<JsonObject> handler) {
        log.debug("getMessage :: Przygotowuję zapytanie");
        String[] collumns = Message.PUBLIC_PARAMS;
        String sqlUpdate = MessageQuery.getMessageById(messageId, collumns);
        DatabaseQueryProvider.query(vertx, sqlUpdate, resultHandler -> {
            JsonObject result;
            if (resultHandler != null && resultHandler.getRows().size() > 0) {
                result = resultHandler.getRows().get(0);
            } else {
                result = null;
            }
            handler.handle(result);
        });
    }

    public static void findMessage(Vertx vertx, Handler<JsonArray> handler) {
        log.debug("findMessage :: Przygotowuję zapytanie");
        String sqlUpdate = MessageQuery.findMessage(Message.PUBLIC_PARAMS);

        DatabaseQueryProvider.query(vertx, sqlUpdate, queryResult -> {
            JsonArray result;
            if (queryResult != null) {
                handler.handle(new JsonArray(queryResult.getRows()));
            } else {
                handler.handle(null);
            }
        });
    }

    public static void removeMessageById(Vertx vertx, int id, Handler<UpdateResult> handler) {
        log.debug("removeMessageById :: Przygotowuję zapytanie");
        String sqlUpdate = MessageQuery.removeMessage(id);

        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }
}
