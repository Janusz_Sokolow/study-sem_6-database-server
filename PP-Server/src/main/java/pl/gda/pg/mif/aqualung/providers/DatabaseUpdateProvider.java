package pl.gda.pg.mif.aqualung.providers;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.ext.sql.UpdateResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class DatabaseUpdateProvider {

    private static final Logger log = LogManager.getLogger(DatabaseUpdateProvider.class);

    public static void updateQuery(Vertx vertx, String sqlQuery, Handler<UpdateResult> handler) {
        DatabaseClient.getConnection(vertx, connection -> {
            if (connection != null) {
                connection.update(sqlQuery, updateResult -> {
                    if (updateResult.succeeded()) {
                        log.debug("Pomyślnie wykonano zapytanie: " + sqlQuery);
                        log.debug("Pomyślnie wykonano zapytanie: ");
                    } else {
                        log.error("Błąd podczas wykonywania zapytania: " + sqlQuery, updateResult.cause());
                    }
                    handler.handle(updateResult.result());
                    log.debug("Zamykam połączenie z bazą danych");
                    connection.close();
                });
            } else {
                handler.handle(null);
            }
        });
    }
}
