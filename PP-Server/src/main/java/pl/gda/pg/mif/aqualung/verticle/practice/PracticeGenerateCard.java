package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.consts.DocumentTypes;
import pl.gda.pg.mif.aqualung.dao.DocumentDAO;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.dao.struct.Company;
import pl.gda.pg.mif.aqualung.dao.struct.Practice;
import pl.gda.pg.mif.aqualung.dao.struct.Student;
import pl.gda.pg.mif.aqualung.providers.MessageManager;
import pl.gda.pg.mif.aqualung.providers.generators.FinishPracticeGenerator;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class PracticeGenerateCard extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticeGenerateCard.class);
    private static final String address = MESSAGES_PREFIX + ".practice.generate";

    private static final String IN_PRACTICE_ID = "practiceId";

    private static final String OUT_PDF_SRC = "pdfSrc";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        int practiceId = data.getInteger(IN_PRACTICE_ID);

//        boolean isCoord = data.getJsonObject(Principal.USER).getJsonArray(Principal.ROLES).contains(Roles.COORDINATOR);
        PracticeDAO.getPractice(vertx, practiceId, practice -> {
            JsonObject student = practice.getJsonObject("student");
            JsonObject company = practice.getJsonObject("company");
            String documentPath = FinishPracticeGenerator.generate(
                    student.getString(Student.NAME) + " " + student.getString(Student.SURNAME),
                    student.getInteger(Student.PG_INDEX).toString(),
                    student.getString(Student.SPECIALITY),
                    company.getString(Company.COMPANY_NAME),
                    company.getString(Company.ADDRESS),
                    practice.getString(Practice.DATE_FROM),
                    practice.getString(Practice.DATE_TO),
                    practice.getString(Practice.BHP_CITY),
                    practice.getString(Practice.BHP_DATE),
                    practice.getString(Practice.BHP_EMPLOYEE),
                    practice.getJsonArray("descriptions")
            );

            DocumentDAO.insertDocument(vertx, practiceId, documentPath, student.getString(Student.NAME) + "_" + student.getString(Student.SURNAME) + "-" +  DocumentTypes.PRACTICE_CARD.name(), DocumentTypes.PRACTICE_CARD, res -> {
                MessageManager.replyValue(msg, OUT_PDF_SRC, documentPath);
            });

        });
    };
}
