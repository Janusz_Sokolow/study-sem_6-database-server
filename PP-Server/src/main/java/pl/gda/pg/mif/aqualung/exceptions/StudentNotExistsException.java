package pl.gda.pg.mif.aqualung.exceptions;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class StudentNotExistsException extends PracticeException{
    
    public static final String message = "practice_exceptions.student.not_exists"; 
    public StudentNotExistsException() {
        super(message);
    }
}
