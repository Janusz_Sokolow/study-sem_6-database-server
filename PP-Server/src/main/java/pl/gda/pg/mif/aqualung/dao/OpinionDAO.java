package pl.gda.pg.mif.aqualung.dao;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.Iterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.dao.queries.OpinionQuery;
import pl.gda.pg.mif.aqualung.dao.struct.CompanyOpinion;
import pl.gda.pg.mif.aqualung.providers.DatabaseQueryProvider;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class OpinionDAO {

    private static final Logger log = LogManager.getLogger(OpinionDAO.class);

    public static void findCompanyOpinion(Vertx vertx, int companyId, Handler<JsonObject> handler) {
        log.debug("findCompanyOpinion :: Przygotowuję zapytanie");
        
        // Przygotowanie zapytania bazodanowego
        String query = OpinionQuery.findCompanyOpinion(companyId);
        // Wykonanie zapytania
        DatabaseQueryProvider.query(vertx, query, resultSet -> {
            log.debug("findCompanyOpinion :: otrzymałem wynik");
            Iterator<JsonObject> iterator = resultSet.getRows().iterator();
            JsonObject result = new JsonObject();
            // Rozpoczęcie iteracji po otrzymanych obiektach bazodanowych
            while (iterator.hasNext()) {
                JsonObject next = iterator.next();
                String opinionKey = next.getString(CompanyOpinion.KEY);
                String opinionValue = next.getString(CompanyOpinion.VALUE);
                // Pobranie wcześniej dodanych wartości do danego klucza
                //      lub jeśli ich brak stworzenie nowej listy
                JsonArray values = result.getJsonArray(opinionKey, new JsonArray());
                // Dodanie wartości do listy
                values.add(opinionValue);
                // Zapisanie listy do zwracanej odpowiedzi
                result.put(opinionKey, values);
            }
            // Asynchroniczne wysłanie odpowiedzi 
            //      w postaci kluczy i listą powiązanych z nimi wartościami
            handler.handle(result);
        });
    }
}
