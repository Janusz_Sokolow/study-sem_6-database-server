
package pl.gda.pg.mif.aqualung.exceptions;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class MyDataBaseException extends PracticeException{

    public MyDataBaseException() {
        super("Błąd");
    }    
    
    public MyDataBaseException(String message) {
        super(message);
    }
}