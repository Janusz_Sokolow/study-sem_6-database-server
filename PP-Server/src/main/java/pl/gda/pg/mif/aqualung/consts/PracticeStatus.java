package pl.gda.pg.mif.aqualung.consts;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public enum PracticeStatus {

    NEW, APPROVED, REJECTED, DURING, FINISHED, ARCHIVE;
}
