package pl.gda.pg.mif.aqualung.verticle.company;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.OpinionDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class CompanyStats extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(CompanyStats.class);
    private static final String ADDRESS = MESSAGES_PREFIX + ".company.getStats";
    
    private static final String IN_COMPANY_ID = "companyId";
    private static final String OUT_COMPANIES_LIST = "companiesList";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(ADDRESS, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);
        // Wydobycie wejściowego parametru identyfikującego firmę
        int companyId = data.getInteger(IN_COMPANY_ID);
        // Wydobycie danych z warstwy bazodanowej
        OpinionDAO.findCompanyOpinion(vertx, companyId, res -> {
            // Wysłanie danych wyjściowych
            MessageManager.replyValue(msg, OUT_COMPANIES_LIST, res);
        }); 
    }

}
    