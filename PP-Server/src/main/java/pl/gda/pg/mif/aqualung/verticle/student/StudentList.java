package pl.gda.pg.mif.aqualung.verticle.student;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.StudentDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class StudentList extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(StudentList.class);
    private static final String address = MESSAGES_PREFIX + ".student.find";

    private static final String OUT_STUDENT_LIST = "studentsList";
    
    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        StudentDAO.findStudent(vertx, res -> {
            MessageManager.replyValue(msg, OUT_STUDENT_LIST, res);
        });
    }

}
