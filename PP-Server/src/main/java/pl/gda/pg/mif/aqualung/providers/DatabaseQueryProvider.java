package pl.gda.pg.mif.aqualung.providers;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.ext.sql.ResultSet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class DatabaseQueryProvider {

    private static final Logger log = LogManager.getLogger(DatabaseQueryProvider.class);

    public static void query(Vertx vertx, String sqlQuery, Handler<ResultSet> handler) {
        DatabaseClient.getConnection(vertx, connection -> {
            if (connection != null) {
                connection.query(sqlQuery, updateResult -> {
                    if (updateResult.succeeded()) {
                        log.debug("Pomyślnie wykonano zapytanie: " + sqlQuery);
                    } else {
                        log.error("Błąd podczas wykonywania zapytania: " + sqlQuery, updateResult.cause());
                    }
                    handler.handle(updateResult.result());
                    log.debug("Zamykam połączenie z bazą danych");
                    connection.close();
                });
            } else {
                handler.handle(null);
            }
        });
    }
}
