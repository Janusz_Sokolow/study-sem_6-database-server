package pl.gda.pg.mif.aqualung.dao.queries;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public abstract class Queries {

    protected static String colectColumns(String... args) {
        String result = " ";
        for (int i = 0; i < args.length; i++) {
            result += args[i] + ",";
        }
        result = result.substring(0, result.length() - 1);
        return result;
    }

    protected static String colectValuesPartly(Object... args) {
        String result = " VALUES (";
        for (int i = 0; i < args.length; i++) {
            if ((args[i] != null && args[i] instanceof String && ((String) args[i]).indexOf("SELECT") < 0)) {
                result += safeString(args[i]) + ",";
            } else if (args[i] != null && args[i] instanceof Boolean) {
                if ((boolean) args[i]) {
                    result += 1 + ",";
                } else {
                    result += 0 + ",";
                }
            } else {
                result += args[i] + ",";
            }
        }
        return result;
    }

    protected static String colectValues(Object... args) {
        String result = colectValuesPartly(args);
        result = result.substring(0, result.length() - 1);
        result += ")";
        return result;
    }

    protected static String where(String key, Object value) {
        if (value != null && (value instanceof String)) {
            return " WHERE " + key + "=" +safeString(value) + " ";
        } else {
            return " WHERE " + key + "=" + value + " ";
        }
    }

    protected static String set(String key, Object value) {
        if (value != null && (value instanceof String)) {
            return " SET " + key + "=" + safeString(value);
        } else {
            return " SET " + key + "=" + value;
        }
    }

    protected static String setFew(String[] keys, Object[] values) {
        String statment = " SET ";
        if (keys.length != values.length) {
            // MEGA ERROR
        }
        for (int i = 0, n = keys.length; i < n; i++) {
            statment += keys[i] + "=";
            Object value = values[i];
            if (value != null && (value instanceof String)) {
                statment += safeString(value);
            } else {
                statment += value;
            }
            if (i != n - 1) {
                statment += ",";
            }
        }

        return statment;
    }

    protected static String and(String key, Object value) {
        if (value != null && (value instanceof String)) {
            return " AND " + key + "=" + safeString(value);
        } else {
            return " AND " + key + "=" + value;
        }
    }

    protected static String on(String key, Object value) {
//        if (value != null && (value instanceof String)) {
//            return " ON " + key + "=" + "'" + value + "'";
//        } else {
        return " ON " + key + "=" + value;
//        }
    }

    public static String convertLongToDate(long time) {
        Date dt = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(dt);
    }

    public static String convertLongToDay(long time) {
        Date dt = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(dt);
    }

    public static String dateNow() {
        return convertLongToDate(System.currentTimeMillis());
    }

    public static String dayNow() {
        return convertLongToDay(System.currentTimeMillis());
    }
    
    protected static String safeString(Object string) {
        return "'" +((String) string).replace("'", "\\'") + "'";
    }
}
