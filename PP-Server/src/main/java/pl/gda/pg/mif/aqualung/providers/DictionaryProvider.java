package pl.gda.pg.mif.aqualung.providers;

import io.vertx.core.json.JsonObject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.helpers.ConfigHelper;
import pl.gda.pg.mif.aqualung.helpers.FileHelper;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz
 * Sokołow, Student of Gdańsk University of Technology
 */
public class DictionaryProvider {

    private static final Logger log = LogManager.getLogger(DictionaryProvider.class);

    private static HashMap<String, HashMap<String, String>> dictionary = new HashMap<>();

    private static JsonObject dictionaryOptions = ConfigHelper.getPortalConfig().getJsonObject("dictionary");

    public static String getTranslation(String key) {
        return getTranslation(key, "pl");

    }

    public static String getTranslation(String key, String local) {
        log.debug("Loading translation " + local + " :: " + key);
        if (!dictionary.containsKey(local)) {
            try {
                loadLocal(local);
            } catch (IOException ex) {
                getTranslation(key, "pl");
            }
        }

        HashMap<String, String> localDictionary = dictionary.get(local);

        log.debug("Known translations : " + localDictionary.keySet());
        if (localDictionary.containsKey(key)) {
            return localDictionary.get(key);
        } else {
            return "UNKNOWN DATA";
        }
    }

    private static void loadLocal(String local) throws IOException {
        String location = dictionaryOptions.getString("prefix");
        location += local;
        location += dictionaryOptions.getString("sufix");
        byte[] res = FileHelper.getFileWithUtil(location);

        dictionary.put(local, flatJson("", new JsonObject(new String(res))));
    }

    private static HashMap<String, String> flatJson(String prefix, JsonObject object) {
        HashMap<String, String> result = new HashMap<>();

        Iterator<Map.Entry<String, Object>> iterator = object.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> next = iterator.next();
            if (next.getValue() instanceof JsonObject) {
                result.putAll(flatJson(next.getKey() + ".", (JsonObject) next.getValue()));
            } else {

                result.put(prefix + next.getKey(), (String) next.getValue());
            }
        }
        return result;
    }
}
