package pl.gda.pg.mif.aqualung.helpers;

import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Config;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology janusz
 */
public class ConfigHelper {

    private static final Logger log = LogManager.getLogger(ConfigHelper.class);
    private static JsonObject configuration = null;
    private static JsonObject portalConfiguration = null;

    public static JsonObject getConfig() {
        if (configuration == null) {
            byte[] byteData = FileHelper.getFileWithUtil(Config.CONFIG_FILE);
            String stringData = new String(byteData);
            configuration = new JsonObject(stringData);
            log.debug("Pobrałem pliki konfiguracyjne : " + configuration.encodePrettily());
        }
        return configuration;
    }

    public static JsonObject getPortalConfig() {
        if (portalConfiguration == null) {
            byte[] byteData = FileHelper.getFileWithUtil(Config.PORTAL_CONFIG_FILE);
            String stringData = new String(byteData);
            portalConfiguration = new JsonObject(stringData);
            log.debug("Pobrałem pliki konfiguracyjne : " + portalConfiguration.encodePrettily());
        }
        return portalConfiguration;
    }
    
    public static String getPortalCity() {
        return getPortalConfig().getString("city");
    }
}
