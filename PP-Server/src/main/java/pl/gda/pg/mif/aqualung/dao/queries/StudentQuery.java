package pl.gda.pg.mif.aqualung.dao.queries;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Specialities;
import pl.gda.pg.mif.aqualung.dao.struct.Student;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class StudentQuery extends Queries {

    private static final Logger log = LogManager.getLogger(StudentQuery.class);

    public static String getStudentById(int index, String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Student.TABLE_NAME
                + where(Student.PG_INDEX, index);
    }

    public static String getStudentByNickName(String nickName, String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Student.TABLE_NAME
                + where(Student.NICK_NAME, nickName);
    }

    public static String getStudentByNickName(String nickName, String password, String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Student.TABLE_NAME
                + where(Student.NICK_NAME, nickName) 
                + and(Student.PASSWORD, password);
    }

    public static String getStudentByPGIndex(long PGIndex, String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Student.TABLE_NAME
                + where(Student.PG_INDEX, PGIndex);
    }

    public static String insertStudent(int index, String nickName, String password, String name, String surname, String eMail, Specialities speciality) {
        return "INSERT INTO " + Student.TABLE_NAME
                + "(" + colectColumns(Student.PG_INDEX, Student.NICK_NAME, Student.PASSWORD, Student.NAME, Student.SURNAME, Student.EMAIL, Student.SPECIALITY)
                + ") "
                + colectValues(index, nickName, password, name, surname, eMail, speciality.name())
                + ";";
    }

    public static String dropStudentByIndexUpdate(int index) {
        return "DELETE FROM " + Student.TABLE_NAME
                + where(Student.PG_INDEX, index);
    }

    /**
     * @param index
     * @param name
     * @param surname
     *
     * "UPDATE <STUDENT> SET <NAME>=<<NEW_NAME>>,<SURNAME>=<<NEW_SURNAME>> WHERE
     * <PG_INDEX>=<<PG_INDEX>>
     */
    public static String updateStudentNameSurnameByIndexUpdate(int index, String name, String surname, String mail, Specialities speciality) {
        return "UPDATE " + Student.TABLE_NAME
                + " SET " + Student.NAME + "='" + name + "'," + Student.SURNAME + "='" + surname + "'," + Student.EMAIL + "='" + mail + "'," + Student.SPECIALITY + "='" + speciality.name() + "'"
                + where(Student.PG_INDEX, index);
    }

    public static String findStudents(String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Student.TABLE_NAME;
    }

    public static String findStudenstBySpeciality(Specialities speciality, String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Student.TABLE_NAME
                + where(Student.SPECIALITY,  speciality.name());
    }
}
