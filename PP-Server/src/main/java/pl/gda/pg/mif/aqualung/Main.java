package pl.gda.pg.mif.aqualung;

import io.vertx.core.Vertx;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.verticle.company.CompanyCreate;
import pl.gda.pg.mif.aqualung.verticle.company.CompanyExistsName;
import pl.gda.pg.mif.aqualung.verticle.company.CompanyList;
import pl.gda.pg.mif.aqualung.verticle.company.CompanyStats;
import pl.gda.pg.mif.aqualung.verticle.document.DocumentRemove;
import pl.gda.pg.mif.aqualung.verticle.in.LogPerson;
import pl.gda.pg.mif.aqualung.verticle.message.MessageCreate;
import pl.gda.pg.mif.aqualung.verticle.message.MessageList;
import pl.gda.pg.mif.aqualung.verticle.message.MessageRemove;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticeFind;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticeFinish;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticeGenerateCard;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticePartAdd;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticePartFind;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticePartGet;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticeSetBHP;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticeShow;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticeShowOwn;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticeStart;
import pl.gda.pg.mif.aqualung.verticle.practice.PracticeStatus;
import pl.gda.pg.mif.aqualung.verticle.student.StudentCreate;
import pl.gda.pg.mif.aqualung.verticle.student.StudentExistsNickname;
import pl.gda.pg.mif.aqualung.verticle.student.StudentExistsPGIndex;
import pl.gda.pg.mif.aqualung.verticle.student.StudentGet;
import pl.gda.pg.mif.aqualung.verticle.student.StudentList;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class Main {
    
    private static final Logger log = LogManager.getLogger(Main.class);

    public static void main(String args[]) throws Exception {
        log.debug("Start main");
        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(Server.class.getCanonicalName());
        
        start(vertx);
    }

    public static void start(Vertx vertx) throws Exception {
        log.debug("Verticle startuje");
        
        vertx.deployVerticle(LogPerson.class.getCanonicalName());
        
        vertx.deployVerticle(StudentCreate.class.getCanonicalName());
        vertx.deployVerticle(StudentList.class.getCanonicalName());
        vertx.deployVerticle(StudentGet.class.getCanonicalName());
        vertx.deployVerticle(StudentExistsNickname.class.getCanonicalName());
        vertx.deployVerticle(StudentExistsPGIndex.class.getCanonicalName());
//        vertx.deployVerticle(StudentUpdate.class.getCanonicalName());
        
        vertx.deployVerticle(CompanyCreate.class.getCanonicalName());
        vertx.deployVerticle(CompanyList.class.getCanonicalName());
        vertx.deployVerticle(CompanyStats.class.getCanonicalName());
        vertx.deployVerticle(CompanyExistsName.class.getCanonicalName());
        
        vertx.deployVerticle(MessageCreate.class.getCanonicalName());
        vertx.deployVerticle(MessageList.class.getCanonicalName());
        vertx.deployVerticle(MessageRemove.class.getCanonicalName());
        
        vertx.deployVerticle(PracticeStart.class.getCanonicalName());
        vertx.deployVerticle(PracticeFind.class.getCanonicalName());
//        vertx.deployVerticle(PracticeRemove.class.getCanonicalName());
        vertx.deployVerticle(PracticePartAdd.class.getCanonicalName());
        vertx.deployVerticle(PracticePartGet.class.getCanonicalName());
        vertx.deployVerticle(PracticePartFind.class.getCanonicalName());
        vertx.deployVerticle(PracticeShow.class.getCanonicalName());
        vertx.deployVerticle(PracticeShowOwn.class.getCanonicalName());
        vertx.deployVerticle(PracticeStatus.class.getCanonicalName());
        vertx.deployVerticle(PracticeSetBHP.class.getCanonicalName());
        vertx.deployVerticle(PracticeFinish.class.getCanonicalName());
        vertx.deployVerticle(PracticeGenerateCard.class.getCanonicalName());
        
        vertx.deployVerticle(DocumentRemove.class.getCanonicalName());
    }
}
