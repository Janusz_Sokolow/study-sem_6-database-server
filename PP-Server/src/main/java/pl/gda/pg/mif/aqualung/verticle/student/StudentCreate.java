package pl.gda.pg.mif.aqualung.verticle.student;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.consts.Specialities;
import pl.gda.pg.mif.aqualung.dao.StudentDAO;
import pl.gda.pg.mif.aqualung.providers.auth.PasswordEncriptor;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class StudentCreate extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(StudentCreate.class);
    private static final String address = MESSAGES_PREFIX + ".student.create";

    private static final String IN_NICKNAME = "nickname";
    private static final String IN_PASSWORD = "password";
    private static final String IN_PG_INDEX = "PGIndex";
    private static final String IN_FIRST_NAME = "firstName";
    private static final String IN_LAST_NAME = "lastName";
    private static final String IN_MAIL = "mail";
    private static final String IN_SPECIALITY = "speciality";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        String nickname = data.getString(IN_NICKNAME);
        String name = data.getString(IN_FIRST_NAME);
        String surname = data.getString(IN_LAST_NAME);
        String mail = data.getString(IN_MAIL);
        Specialities speciality = Specialities.valueOf(data.getString(IN_SPECIALITY));
        String password = PasswordEncriptor.encript(data.getString(IN_PASSWORD));
        Integer GUTIndex = data.getInteger(IN_PG_INDEX);

        StudentDAO.insertStudent(vertx, GUTIndex, nickname, password, name, surname, mail, speciality, res -> {
            MessageManager.replySimple(msg, res);
        });
    }
}
