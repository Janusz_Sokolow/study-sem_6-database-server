package pl.gda.pg.mif.aqualung.providers.auth;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import java.util.HashSet;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology janusz
 */
public class Principal implements User {

    private static final Logger log = LogManager.getLogger(Principal.class);

    public static final String USER = "__USER__";
    public static final String ID = "id";
    public static final String USERNAME = "username";
    public static final String FACULTITY = "facultity";
    public static final String ROLES = "containsRoles";
    public static final String ANONYMOUS = "anonymous";
    public static final String USER_AGENT = "userAgent";

    private long id;
    private String username = "";
    private String userAgent = "";
    private String facultity = "";
    private Set<String> containsRoles = new HashSet<>();
    private boolean anonymous = false;
    public static String PRINCIPAL = "__PRINCIPAL__";

    public Principal() {
    }

    @Override
    public User isAuthorised(String authority, Handler<AsyncResult<Boolean>> resultHandler) {
        if ("LOGGED".equals(authority)) {
            resultHandler.handle(Future.succeededFuture(containsRoles.size() > 0));
        } else if (containsRoles.contains(authority)) {
            resultHandler.handle(Future.succeededFuture(true));
        } else {
            resultHandler.handle(Future.succeededFuture(false));
        }
        return this;
    }

    @Override
    public User clearCache() {
        containsRoles.clear();
        return this;
    }

    @Override
    public JsonObject principal() {
        JsonArray roles = new JsonArray();
        containsRoles.forEach(role -> {
            roles.add(role);
        });

        return new JsonObject()
                .put(USERNAME, username)
                .put(ID, id)
                .put(ROLES, roles)
                .put(FACULTITY, facultity)
                .put(USER_AGENT, userAgent)
                .put(ANONYMOUS, this.anonymous);
    }

    public static JsonObject anonymousPrincipal() {
        return new JsonObject()
                .put(USERNAME, "")
                .put(ROLES, new JsonArray())
                .put(ANONYMOUS, Boolean.TRUE);
    }

    @Override
    public void setAuthProvider(AuthProvider authProvider) {

    }

    public Principal setId(long id) {
        this.id = id;
        return this;
    }

    public long getId() {
        return this.id;
    }

    public Principal setFacultity(String facultitys) {
        this.facultity = facultitys;
        return this;
    }

    public Principal setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
        return this;
    }

    public Principal setUsername(String username) {
        this.username = username;
        return this;
    }

    public Principal setUserAgent(String agent) {
        this.userAgent = agent;
        return this;
    }

    public String getUserAgent() {
        return this.userAgent;
    }

    public Principal setContainsRoles(JsonArray rolesArray) {
        Set<String> roles = new HashSet<>();
        rolesArray.forEach(role -> {
            roles.add((String) role);
        });
        this.containsRoles.clear();
        this.containsRoles = roles;
        return this;
    }

}
