package pl.gda.pg.mif.aqualung.verticle.company;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.CompanyDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class CompanyExistsName extends AbstractVerticle {
    
    private static final Logger log = LogManager.getLogger(CompanyExistsName.class);
    private static final String address = MESSAGES_PREFIX + ".company.existsName";
    
    private static final String IN_NAME = "companyName";
    
    private static final String OUT_EXISTS = "exists";
    
    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }
    
    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);
        
        String companyName = data.getString(IN_NAME);
        CompanyDAO.getCompany(vertx, companyName, company -> {
            Boolean isExistng = company != null;
            MessageManager.replyValue(msg, OUT_EXISTS, isExistng);
        });
    }
    
}
