/*
 * Klasa odpowiada za stworzenie i przeslanie zapytania modyfikującego 
 * tablicę Student w bazie danych.
 * Także ona odpowiada za bezpieczeństwo i poprawnośc wykonanych operacji
 */
package pl.gda.pg.mif.aqualung.dao;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.UpdateResult;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.PracticeStatus;
import pl.gda.pg.mif.aqualung.dao.queries.OpinionQuery;
import pl.gda.pg.mif.aqualung.dao.queries.PracticeQuery;
import pl.gda.pg.mif.aqualung.dao.struct.Company;
import pl.gda.pg.mif.aqualung.dao.struct.Practice;
import pl.gda.pg.mif.aqualung.dao.struct.PracticePart;
import pl.gda.pg.mif.aqualung.dao.struct.Student;
import pl.gda.pg.mif.aqualung.providers.DatabaseQueryProvider;
import pl.gda.pg.mif.aqualung.providers.DatabaseUpdateProvider;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz
 * Sokołow
 */
public class PracticeDAO extends DAO {

    private static final Logger log = LogManager.getLogger(PracticeDAO.class);

    public static void startPractice(Vertx vertx, long dateFrom, long dateTo, String type, int companyId, String studentNickname, Handler<UpdateResult> handler) {
        log.debug("startPractice :: Przygotowuję zapytanie");
        String sqlUpdate = PracticeQuery.startPractice(dateFrom, dateTo, type, companyId, studentNickname);
        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }

    public static void setStatus(Vertx vertx, long practiceId, String statusName, String message, Handler<UpdateResult> handler) {
        log.debug("setStatus :: Przygotowuję zapytanie");
        String sqlUpdate = PracticeQuery.setStatus(practiceId, PracticeStatus.valueOf(statusName), message);
        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }

    public static void setBHP(Vertx vertx, long practiceId, String BHPCity, long BHPDate, String BHPEmployee, Handler<UpdateResult> handler) {
        log.debug("setBHP :: Przygotowuję zapytanie");
        String sqlUpdate = PracticeQuery.setBHP(practiceId, BHPCity, BHPDate, BHPEmployee);
        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }

    public static void finishPractice(Vertx vertx, long practiceId, long companyId, JsonObject companyOpinions, JsonObject subjectOpinions, Handler<UpdateResult> handler) {
        log.debug("finishPractice :: Przygotowuję zapytanie");

        String query = OpinionQuery.insertCompanyOpinion(companyOpinions, companyId);

        DatabaseUpdateProvider.updateQuery(vertx, query, res -> {
            if (subjectOpinions.fieldNames().size() > 0) {
                String subjectQuery = OpinionQuery.insertSubjectOpinion(subjectOpinions);
                DatabaseUpdateProvider.updateQuery(vertx, subjectQuery, res2 -> {
                    log.debug("Pomyślnie dodano opinie o przedmiotach");
                });
            }
            DatabaseUpdateProvider.updateQuery(vertx, PracticeQuery.finish(practiceId), handler);
        });
    }

    public static void describePractice(Vertx vertx, long practiceId, int week, String note, JsonArray skills, Handler<UpdateResult> handler) {
        log.debug("describePractice :: Przygotowuję zapytanie");
        String sqlUpdate = PracticeQuery.addDescribePractice(practiceId, week, note, skills);
        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }

    public static void getPracticePart(Vertx vertx, long practiceId, int week, Handler<JsonObject> handler) {
        log.debug("getPracticePart :: Przygotowuję zapytanie");
        String sqlUpdate = PracticeQuery.getDescribePractice(practiceId, week, PracticePart.ID);
        DatabaseQueryProvider.query(vertx, sqlUpdate, queryHandler -> {
            if (queryHandler != null) {
                JsonArray row = queryHandler.getResults().get(0);

                handler.handle(new JsonObject()
                        .put(PracticePart.ID, row.getValue(0)));
            } else {
                handler.handle(null);
            }
        });
    }

    public static void findPracticePart(Vertx vertx, long practiceId, Handler<JsonArray> handler) {
        log.debug("findPracticePart :: Przygotowuję zapytanie");
        String sqlUpdate = PracticeQuery.findDescription(practiceId, PracticePart.DESCRIPTION, PracticePart.ID, PracticePart.SKILLS, PracticePart.STAGE);
        DatabaseQueryProvider.query(vertx, sqlUpdate, queryHandler -> {
            if (queryHandler != null) {
                JsonArray result = new JsonArray(queryHandler.getRows());

                result.forEach(practicePart -> {
                    parseSkills((JsonObject) practicePart);
                });
                handler.handle(result);
            } else {
                handler.handle(null);
            }
        });
    }

    public static void findPractice(Vertx vertx, String facultity, Handler<JsonArray> handler) {
        String[] collumns = Stream.concat(
                Arrays.stream(Practice.PUBLIC_PARAMS),
                Arrays.stream(new String[]{Company.T_COMPANY_NAME, Student.T_NICK_NAME, Student.T_NAME, Student.T_SURNAME, Student.T_PG_INDEX})
        ).toArray(String[]::new);

        String sqlUpdate = PracticeQuery.findPractice(facultity, collumns);

        DatabaseQueryProvider.query(vertx, sqlUpdate, queryHandler -> {
            JsonArray result;
            if (queryHandler == null) {
                result = null;
            } else {
                result = new JsonArray(queryHandler.getRows());
            }
            handler.handle(result);
        });
    }

    public static void getPractice(Vertx vertx, long id, Handler<JsonObject> handler) {

        String[] collumnsPP = {PracticePart.ID, PracticePart.CREATE_DATE, PracticePart.DESCRIPTION, PracticePart.SKILLS, PracticePart.STAGE};
        String[] collumnsP = Practice.PUBLIC_PARAMS;
        String sqlQuery = PracticeQuery.getPractice(id, Stream.concat(Arrays.stream(collumnsPP), Arrays.stream(collumnsP))
                .toArray(String[]::new));
        DatabaseQueryProvider.query(vertx, sqlQuery, queryHandler -> {
            if (queryHandler == null) {
                handler.handle(null);
            } else if (queryHandler.getNumRows() == 0) {
                handler.handle(new JsonObject());
            } else {
                StudentDAO.getStudentByPGIndex(vertx, queryHandler.getRows().get(0).getInteger(Practice.STUDENT_PG_INDEX), student -> {
                    CompanyDAO.getCompanyById(vertx, queryHandler.getRows().get(0).getInteger(Practice.COMPANY_ID), company -> {
                        DocumentDAO.findDocumentsByPracticeId(vertx, queryHandler.getRows().get(0).getInteger(Practice.ID), documents -> {
                            handler.handle(
                                    parsePracticeToGet(queryHandler, collumnsPP, collumnsP)
                                    .put("company", company)
                                    .put("student", student)
                                    .put("documents", documents)
                            );
                        });
                    });
                });
            }
        });
    }

    public static void getPracticeByUserId(Vertx vertx, long id, Handler<JsonObject> handler) {

        String[] collumnsPP = {PracticePart.ID, PracticePart.CREATE_DATE, PracticePart.DESCRIPTION, PracticePart.SKILLS, PracticePart.STAGE};
        String[] collumnsP = Practice.PUBLIC_PARAMS;
        String sqlQuery = PracticeQuery.getPracticeByStudentId(id, Stream.concat(Arrays.stream(collumnsPP), Arrays.stream(collumnsP))
                .toArray(String[]::new));

        DatabaseQueryProvider.query(vertx, sqlQuery, queryHandler -> {

            if (queryHandler == null) {
                handler.handle(null);
            } else if (queryHandler.getNumRows() == 0) {
                handler.handle(new JsonObject());
            } else {
                StudentDAO.getStudentByPGIndex(vertx, queryHandler.getRows().get(0).getInteger(Practice.STUDENT_PG_INDEX), student -> {
                    CompanyDAO.getCompanyById(vertx, queryHandler.getRows().get(0).getInteger(Practice.COMPANY_ID), company -> {
                        DocumentDAO.findDocumentsByPracticeId(vertx, queryHandler.getRows().get(0).getInteger(Practice.ID), documents -> {
                            handler.handle(
                                    parsePracticeToGet(queryHandler, collumnsPP, collumnsP)
                                    .put("company", company)
                                    .put("student", student)
                                    .put("documents", documents)
                            );
                        });
                    });
                });
            }
        });
    }

    private static JsonObject parsePracticeToGet(ResultSet queryHandler, String[] collumnsPP, String[] collumnsP) {
        JsonObject result;
        result = new JsonObject();
        JsonArray descriptions = new JsonArray(queryHandler.getRows());
        JsonObject practiceObj = descriptions.getJsonObject(0);
        // Clearify descriptions 
        result.put("descriptions", new JsonArray(descriptions.stream()
                .filter(obj -> {
                    return ((JsonObject) obj).getInteger(PracticePart.ID) != null;
                })
                .map(obj -> {
                    JsonObject practicePart = new JsonObject();
                    for (int j = 0, i = collumnsPP.length; j < i; j++) {
                        practicePart.put(collumnsPP[j], ((JsonObject) obj).getValue(collumnsPP[j]));
                    }

                    parseSkills(practicePart);

                    return practicePart;
                })
                .collect(Collectors.toList())));

        log.debug("rows " + descriptions);

        log.debug("results " + queryHandler.getResults().toString());

        for (int j = 0, i = collumnsP.length; j < i; j++) {
            result.put(collumnsP[j], practiceObj.getValue(collumnsP[j]));
        }
        log.debug("TEST ==== " + queryHandler.getRows().get(0).encodePrettily());

        result.put(Practice.ID, queryHandler.getRows().get(0).getInteger(Practice.ID));

        return result;
    }

    public static void removePracticeById(Vertx vertx, int id, Handler<UpdateResult> handler) {
        String sqlUpdate = PracticeQuery.removePractice(id);

        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }

    public static void parseSkills(JsonObject practicePart) {

        // Parse String into JsonArray
        if (practicePart.getString(PracticePart.SKILLS) != null) {
            practicePart.put(PracticePart.SKILLS, new JsonArray(practicePart.getString(PracticePart.SKILLS)));
        }
    }
}
