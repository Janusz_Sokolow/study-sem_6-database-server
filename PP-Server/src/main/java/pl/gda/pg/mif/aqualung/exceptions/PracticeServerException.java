package pl.gda.pg.mif.aqualung.exceptions;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class PracticeServerException extends PracticeException {

    public static final String message = "practice_exceptions.server_exception";

    public PracticeServerException() {
        super(message);
    }
;
}
