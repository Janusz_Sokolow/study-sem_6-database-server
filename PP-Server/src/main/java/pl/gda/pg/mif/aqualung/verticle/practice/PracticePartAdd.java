package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class PracticePartAdd extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticePartAdd.class);
    private static final String address = MESSAGES_PREFIX + ".practice.part.describe";

    private static final String IN_PRACTICE_ID = "practiceId";
    private static final String IN_WEEK = "week";
    private static final String IN_NOTE = "note";
    private static final String IN_SKILLS = "skills";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        int practiceId = data.getInteger(IN_PRACTICE_ID);
        int week = data.getInteger(IN_WEEK);
        String note = data.getString(IN_NOTE);
        JsonArray skills = data.getJsonArray(IN_SKILLS);

        log.debug("practiceId : " + practiceId);
        PracticeDAO.describePractice(vertx, practiceId, week, note, skills, res -> {
            MessageManager.replySimple(msg, res);
        });
    }
}
