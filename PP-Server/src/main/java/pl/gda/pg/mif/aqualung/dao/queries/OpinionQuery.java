package pl.gda.pg.mif.aqualung.dao.queries;

import io.vertx.core.json.JsonObject;
import java.util.Iterator;
import static pl.gda.pg.mif.aqualung.dao.queries.Queries.colectColumns;
import pl.gda.pg.mif.aqualung.dao.struct.CompanyOpinion;
import pl.gda.pg.mif.aqualung.dao.struct.SubjectOpinion;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class OpinionQuery extends Queries {

    public static String insertSubjectOpinion(JsonObject subjectOpinions) {

        StringBuilder builder = new StringBuilder();

        builder.append("INSERT INTO ");
        builder.append(SubjectOpinion.TABLE_NAME);
        builder.append(" (");
        builder.append(colectColumns(SubjectOpinion.INDEX, SubjectOpinion.DESCRIPTION));
        builder.append(") VALUES ");

        Iterator<String> companyIterator = subjectOpinions.fieldNames().iterator();
        while (companyIterator.hasNext()) {
            String key = companyIterator.next();
            builder.append("(");
            builder.append(safeString(key));
            builder.append(",");
            builder.append(safeString(subjectOpinions.getValue(key).toString()));
            builder.append("),");
        }
        int lastCommaIndex = builder.lastIndexOf(",");
        builder.replace(lastCommaIndex, lastCommaIndex + 1, " ");
        return builder.toString();
    }

    public static String insertCompanyOpinion(JsonObject companies, long companyId) {

        StringBuilder builder = new StringBuilder();

        builder.append("INSERT INTO ");
        builder.append(CompanyOpinion.TABLE_NAME);
        builder.append(" (");
        builder.append(colectColumns(CompanyOpinion.KEY, CompanyOpinion.VALUE, CompanyOpinion.COMPANY));
        builder.append(") VALUES ");

        Iterator<String> companyIterator = companies.fieldNames().iterator();
        while (companyIterator.hasNext()) {
            String key = companyIterator.next();
            builder.append("(");
            builder.append(safeString(key));
            builder.append(",");
            builder.append(safeString(companies.getValue(key).toString()));
            builder.append(",");
            builder.append(companyId);
            builder.append("),");
        }
        int lastCommaIndex = builder.lastIndexOf(",");
        builder.replace(lastCommaIndex, lastCommaIndex + 1, " ");
        return builder.toString();
    }

    public static String findCompanyOpinion(int companyId) {
        return "SELECT " + colectColumns(CompanyOpinion.KEY, CompanyOpinion.VALUE)
                + " FROM " + CompanyOpinion.TABLE_NAME
                + where(CompanyOpinion.COMPANY, companyId);
    }
}
