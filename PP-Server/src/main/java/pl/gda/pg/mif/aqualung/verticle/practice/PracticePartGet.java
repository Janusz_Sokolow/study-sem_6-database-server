package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class PracticePartGet extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticePartGet.class);
    private static final String address = MESSAGES_PREFIX + ".practice.part.get";

    private static final String IN_PRACTICE_ID = "practiceId";
    private static final String IN_WEEK = "week";
    
    
    private static final String OUT_PRACTICE_PART = "practicePart";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        int practiceId = data.getInteger(IN_PRACTICE_ID);
        int week = data.getInteger(IN_WEEK);

        PracticeDAO.getPracticePart(vertx, practiceId, week, res -> {
            log.debug("Pobieranie opisu praktyki zakończono : " + res.encodePrettily());
            MessageManager.replyValue(msg, OUT_PRACTICE_PART, res);
        });
    }
}
