package pl.gda.pg.mif.aqualung.verticle.in;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.StudentDAO;
import pl.gda.pg.mif.aqualung.dao.struct.Student;
import pl.gda.pg.mif.aqualung.exceptions.StudentNotExistsException;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class LogPerson extends AbstractVerticle {

    public static final String ADDRESS = MESSAGES_PREFIX + ".private.log";
    private static final Logger log = LogManager.getLogger(LogPerson.class);

    public static final String IN_USER_NAME = "userName";
    public static final String IN_PASSWORD = "password";

    public static final String OUT_USER_ID = Student.PG_INDEX;
    public static final String OUT_USER_NAME = "username";

    @Override
    public void start() {
        log.debug(" start");
        vertx.eventBus().consumer(ADDRESS, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject params = msg.body();
        log.debug(" - handler", params);

        String name = params.getString(IN_USER_NAME);
        String pass = params.getString(IN_PASSWORD);

        StudentDAO.getStudent(vertx, name, pass, person -> {
            if (person != null) {
                msg.reply(new JsonObject()
                        .put(OUT_USER_ID, person.getValue(Student.PG_INDEX))
                        .put(OUT_USER_NAME, person.getValue(Student.NICK_NAME)));
            } else {
                MessageManager.replyError(msg, new StudentNotExistsException());
            }
        });
    }
}
