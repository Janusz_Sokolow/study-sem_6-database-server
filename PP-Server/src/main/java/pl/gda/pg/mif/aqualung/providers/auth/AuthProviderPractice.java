package pl.gda.pg.mif.aqualung.providers.auth;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Config;
import pl.gda.pg.mif.aqualung.consts.Roles;
import pl.gda.pg.mif.aqualung.exceptions.LoginDataNotRecognized;
import pl.gda.pg.mif.aqualung.exceptions.StudentNotExistsException;
import pl.gda.pg.mif.aqualung.helpers.ConfigHelper;
import pl.gda.pg.mif.aqualung.providers.MessageManager;
import pl.gda.pg.mif.aqualung.verticle.in.LogPerson;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class AuthProviderPractice implements AuthProvider {

    private static final Logger log = LogManager.getLogger(AuthProviderPractice.class);
    Vertx vertx;
    JsonObject portalOptions;

    public AuthProviderPractice(Vertx vertx) {
        this.vertx = vertx;
        portalOptions = ConfigHelper.getPortalConfig();
    }

    @Override
    public void authenticate(JsonObject loginData, Handler<AsyncResult<User>> hndlr) {

        String userName = loginData.getString(LogPerson.IN_USER_NAME);
        String password = loginData.getString(LogPerson.IN_PASSWORD);
        vertx.eventBus().send(LogPerson.ADDRESS, loginData, res -> {
            log.debug("RESPONSE :: " + res.succeeded());
            log.debug((JsonObject) res.result().body());

            JsonObject coordinatorData = portalOptions.getJsonObject(Config.PORTAL_COORDINATOR);
//            log.debug("coordinatorData :: " + portalOptions.encodePrettily());
//            log.debug("coordinatorData :: " + coordinatorData.encodePrettily());

            JsonObject responseBody = (JsonObject) res.result().body();
            if (res.succeeded() && !((JsonObject) res.result().body()).containsKey(MessageManager.ERROR_FIELD)) {
                Principal principal = new Principal()
                        .setContainsRoles(new JsonArray().add(Roles.STUDENT))
                        .setId(responseBody.getInteger(LogPerson.OUT_USER_ID))
                        .setUsername(responseBody.getString(LogPerson.OUT_USER_NAME));
                hndlr.handle(Future.succeededFuture(principal));
            } else if (responseBody.containsKey(MessageManager.ERROR_FIELD)
                    && responseBody.getString(MessageManager.ERROR_FIELD).equals((new StudentNotExistsException()).getMessage())) {
                if (coordinatorData.getString(Config.PORTAL_COORDINATOR_LOGIN).equals(userName)
                        && coordinatorData.getString(Config.PORTAL_COORDINATOR_PASS).equals(password)) {
                    Principal principal = new Principal()
                            .setContainsRoles(new JsonArray().add(Roles.COORDINATOR))
                            .setId(0)
                            .setFacultity(coordinatorData.getString(Config.PORTAL_COORDINATOR_FACULTITY))
                            .setUsername(coordinatorData.getString(Config.PORTAL_COORDINATOR_NAME));
                    hndlr.handle(Future.succeededFuture(principal));
                } else {
                    hndlr.handle(Future.failedFuture(new LoginDataNotRecognized()));
                }
            } else {
                hndlr.handle(Future.failedFuture(new LoginDataNotRecognized()));
            }
        });
    }
}
