package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.providers.auth.Principal;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class PracticeFind extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticeFind.class);
    private static final String address = MESSAGES_PREFIX + ".practice.find";

    private static final String OUT_PRACTICE_LIST = "practiceList";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);
        
        
        String facultity = data.getJsonObject(Principal.USER).getString(Principal.FACULTITY);
        log.debug(" Ładuję listę praktyk dla wydziału " + facultity);
        

        PracticeDAO.findPractice(vertx, facultity, res -> {
            MessageManager.replyValue(msg, OUT_PRACTICE_LIST, res);
        });
    }
}
