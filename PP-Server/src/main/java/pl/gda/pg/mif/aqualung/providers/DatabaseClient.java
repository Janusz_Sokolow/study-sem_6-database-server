            package pl.gda.pg.mif.aqualung.providers;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.AsyncSQLClient;
import io.vertx.ext.asyncsql.MySQLClient;
import io.vertx.ext.sql.SQLConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Config;
import pl.gda.pg.mif.aqualung.helpers.ConfigHelper;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology student
 *     
 *  
 *     CREATE USER IF NOT EXISTS 'practiceuser'@'localhost' IDENTIFIED BY 'inzJS2017portal';
 *     GRANT ALL PRIVILEGES ON practiceportal.* To 'practiceuser'@'localhost' IDENTIFIED BY 'inzJS2017portal';
 */
public class DatabaseClient {

    private static final Logger log = LogManager.getLogger(DatabaseClient.class);

    private static final JsonObject mySQLClientConfig =  ConfigHelper.getConfig().getJsonObject(Config.DB_CONFIG);

    private static AsyncSQLClient mySQLClient = null;

    public static final AsyncSQLClient getClient(Vertx vertx) {
        if (mySQLClient == null) {
            mySQLClient = MySQLClient.createShared(vertx, mySQLClientConfig);
        }
        return mySQLClient;
    }

    public static void getConnection(Vertx vertx, Handler<SQLConnection> resultHandle) {
        log.debug("Tworzę połączenie : ");
        getClient(vertx).getConnection((res -> {
            if (res.failed()) {
                log.error("Otworzenie połączenia z bazą danych nie powiodło się.", res.cause());
            }
            resultHandle.handle(res.result());
        }));
    }
}
