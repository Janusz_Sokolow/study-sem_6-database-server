package pl.gda.pg.mif.aqualung.consts;



/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology janusz
 */
public class Config {

// Config file
    public static final String CONFIG_FILE = "config.json";
    public static final String PORTAL_CONFIG_FILE = "portalConfig.json";
    
    public static final String SERVER_OPTIONS = "httpServerOptions";
    
    public static final String DB_CONFIG = "database";
    
    public static final String VIEW_CONFIG = "serviceView";
    public static final String VIEW_CONFIG_ROOT = "webRoot";
    public static final String VIEW_CONFIG_INDEX = "indexPath";
    
    
    public static final String FILE_CONFIG = "fileConfig";
    public static final String SERVER_CONFIG_PDF_PATH = "pdfPath";
    public static final String SERVER_CONFIG_PDF_SRC = "pdfSrc";
    public static final String SERVER_CONFIG_PDF_IN = "pdfIn";
    public static final String SERVER_CONFIG_PDF_OUT = "pdfOut";
    
    
    public static final String PORTAL_COORDINATOR = "coordynator";
    public static final String PORTAL_COORDINATOR_NAME = "name";
    public static final String PORTAL_COORDINATOR_FACULTITY = "facultity";
    public static final String PORTAL_COORDINATOR_DEPARTMENT = "department";
    public static final String PORTAL_COORDINATOR_LOGIN = "login";
    public static final String PORTAL_COORDINATOR_PASS = "password";
    
    public static final String RESOURCES = "src/main/resources/";
    
    public static final String MESSAGES_PREFIX = "pl.gda.pg.mif.practice";
}