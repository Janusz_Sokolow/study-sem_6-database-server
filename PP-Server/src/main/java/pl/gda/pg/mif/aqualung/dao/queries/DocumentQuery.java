package pl.gda.pg.mif.aqualung.dao.queries;

import pl.gda.pg.mif.aqualung.consts.DocumentTypes;
import static pl.gda.pg.mif.aqualung.dao.queries.Queries.colectColumns;
import pl.gda.pg.mif.aqualung.dao.struct.Document;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class DocumentQuery extends Queries {

    public static String insertDocument(int practiceId, String path, String name, DocumentTypes type) {
        return "INSERT  INTO " + Document.TABLE_NAME
                + " (" + colectColumns(Document.PRACTICE_ID, Document.NAME, Document.PATH, Document.TYPE) + ") "
                + colectValues(practiceId, name, path, type.name())
                + "  ON DUPLICATE KEY UPDATE "
                + Document.PATH + "=" + safeString(path) + ","
                + Document.NAME + "=" + safeString(name);
    }

    public static String findMessageById(int practiceId, String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Document.TABLE_NAME
                + where(Document.PRACTICE_ID, practiceId);
    }

    public static String removeDocument(int id) {
        return "DELETE FROM  " + Document.TABLE_NAME
                + where(Document.ID, id);
    }
}
