package pl.gda.pg.mif.aqualung.dao;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.UpdateResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.DocumentTypes;
import pl.gda.pg.mif.aqualung.dao.queries.DocumentQuery;
import pl.gda.pg.mif.aqualung.dao.struct.Document;
import pl.gda.pg.mif.aqualung.providers.DatabaseQueryProvider;
import pl.gda.pg.mif.aqualung.providers.DatabaseUpdateProvider;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class DocumentDAO {

    private static final Logger log = LogManager.getLogger(DocumentDAO.class);

    public static void insertDocument(Vertx vertx, int practiceId, String path, String name, DocumentTypes type, Handler<UpdateResult> handler) {
        log.debug("insertDocument :: Przygotowuję zapytanie");
        String sqlUpdate = DocumentQuery.insertDocument(practiceId, path, name, type);

        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }
    
    public static void removeDocumentById(Vertx vertx, int id, Handler<UpdateResult> handler) {
        log.debug("removeDocumentById :: Przygotowuję zapytanie");
        String sqlUpdate = DocumentQuery.removeDocument(id);

        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }

    public static void findDocumentsByPracticeId(Vertx vertx, int practiceId, Handler<JsonArray> handler) {
        log.debug("findDocumentsByPracticeId :: Przygotowuję zapytanie");
        String sqlUpdate = DocumentQuery.findMessageById(practiceId, Document.PUBLIC_PARAMS);

        DatabaseQueryProvider.query(vertx, sqlUpdate, queryResult -> {
            if (queryResult != null) {
                handler.handle(new JsonArray(queryResult.getRows()));
            } else {
                handler.handle(null);
            }
        });
    }
}
