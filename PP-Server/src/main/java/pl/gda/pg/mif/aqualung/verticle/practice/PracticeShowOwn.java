package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;
import pl.gda.pg.mif.aqualung.providers.auth.Principal;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class PracticeShowOwn extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticeShowOwn.class);
    private static final String address = MESSAGES_PREFIX + ".practice.own";

    private static final String OUT_DESCRIPTIONS_LIST = "practice";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);
        
        Long userId = data.getJsonObject(Principal.USER).getLong(Principal.ID);

        PracticeDAO.getPracticeByUserId(vertx, userId, res -> {
//            log.debug("Praktyka posiada opisów: " + res);
            MessageManager.replyValue(msg, OUT_DESCRIPTIONS_LIST, res);
        });
    }
;

}
