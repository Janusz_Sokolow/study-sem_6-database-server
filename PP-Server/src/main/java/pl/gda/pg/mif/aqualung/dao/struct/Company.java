package pl.gda.pg.mif.aqualung.dao.struct;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class Company {
    public static final String TABLE_NAME = "company";
    
    public static final String ID = "id";
    public static final String COMPANY_NAME = "companyName";
    public static final String ADDRESS = "address";
    
    
    
    public static final String T_ID = TABLE_NAME + "." + ID;
    public static final String T_COMPANY_NAME = TABLE_NAME + "." + COMPANY_NAME;
    public static final String T_ADDRESS = TABLE_NAME + "." + ADDRESS;
}



//CREATE TABLE `test`.`company` (
//  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
//  `companyName` VARCHAR(45) NOT NULL,
//  `phoneNumber` VARCHAR(45) NULL,
//  PRIMARY KEY (`id`),
//  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
//  UNIQUE INDEX `companyName_UNIQUE` (`companyName` ASC));


