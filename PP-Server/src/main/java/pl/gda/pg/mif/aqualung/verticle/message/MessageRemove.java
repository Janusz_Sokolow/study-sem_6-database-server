package pl.gda.pg.mif.aqualung.verticle.message;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.MessageDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class MessageRemove extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(MessageRemove.class);
    private static final String address = MESSAGES_PREFIX + ".message.remove";

    private static final String IN_ID = "id";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        Integer id = data.getInteger(IN_ID);

        MessageDAO.removeMessageById(vertx, id, res -> {
            MessageManager.replySimple(msg, res);
        });
    }

}
