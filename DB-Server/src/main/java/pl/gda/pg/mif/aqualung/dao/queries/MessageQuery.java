package pl.gda.pg.mif.aqualung.dao.queries;

import static pl.gda.pg.mif.aqualung.dao.queries.Queries.colectColumns;
import pl.gda.pg.mif.aqualung.dao.struct.Message;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class MessageQuery extends Queries {

    public static String insertMessage(String title, String content, String author) {
        return "INSERT INTO " + Message.TABLE_NAME
                + " (" + colectColumns(Message.CONTENT, Message.TITLE, Message.AUTHOR, Message.CREATE_DATE) + ") "
                + colectValues(content, title, author, dateNow())
                + ";";
    }

    public static String getMessageById(int messageId, String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Message.TABLE_NAME
                + where(Message.T_ID, messageId);
    }

    public static String findMessage(String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Message.TABLE_NAME;
    }

    public static String removeMessage(int id) {
        return "DELETE FROM  " + Message.TABLE_NAME
                + where(Message.ID, id);
    }
}
