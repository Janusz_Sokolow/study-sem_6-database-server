package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.consts.ContractType;
import pl.gda.pg.mif.aqualung.consts.DocumentTypes;
import pl.gda.pg.mif.aqualung.dao.CompanyDAO;
import pl.gda.pg.mif.aqualung.dao.DocumentDAO;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.dao.StudentDAO;
import pl.gda.pg.mif.aqualung.dao.queries.Queries;
import pl.gda.pg.mif.aqualung.dao.struct.Company;
import pl.gda.pg.mif.aqualung.dao.struct.Student;
import pl.gda.pg.mif.aqualung.providers.DictionaryProvider;
import pl.gda.pg.mif.aqualung.providers.MessageManager;
import pl.gda.pg.mif.aqualung.providers.auth.Principal;
import pl.gda.pg.mif.aqualung.providers.generators.PracticeAgreementGenerator;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class PracticeStart extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticeStart.class);
    private static final String address = MESSAGES_PREFIX + ".practice.start";

    private static final String IN_COMPANY_ID = "companyId";
    private static final String IN_TYPE = "type";
    private static final String IN_DATE_FROM = "dateFrom";
    private static final String IN_DATE_TO = "dateTo";
    private static final String IN_STUDENT_NAME = "studentNickname";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        long dateFrom = data.getLong(IN_DATE_FROM);
        long dateTo = data.getLong(IN_DATE_TO);
        String type = data.getString(IN_TYPE);
        Integer companyId = data.getInteger(IN_COMPANY_ID);
        String studentName;
        if (data.getString(IN_STUDENT_NAME) == null) {
            studentName = data.getJsonObject(Principal.USER).getString(Principal.USERNAME);
        } else {
            studentName = data.getString(IN_STUDENT_NAME);
        }

        PracticeDAO.startPractice(vertx, dateFrom, dateTo, type, companyId, studentName, res -> {
            if (type != ContractType.CONTRACT_PG.name()) {
                Integer practiceId = res.getKeys().getInteger(0);
                StudentDAO.getStudent(vertx, studentName, student -> {
                    CompanyDAO.getCompanyById(vertx, companyId, company -> {
                        String companyName = company.getString(Company.COMPANY_NAME);
                        String agreementPath = PracticeAgreementGenerator.generate(
                                student.getString(Student.NAME),
                                student.getString(Student.SURNAME),
                                student.getInteger(Student.PG_INDEX).toString(),
                                companyName,
                                Queries.convertLongToDay(dateFrom),
                                Queries.convertLongToDay(dateTo),
                                DictionaryProvider.getTranslation("work_types." + type));
                        DocumentDAO.insertDocument(vertx, practiceId, agreementPath, 
                                 student.getString(Student.NAME) + "_" + student.getString(Student.SURNAME) + "-" +DocumentTypes.AGREEMENT.name(), DocumentTypes.AGREEMENT, r -> {

                        });
                    });
                });
                MessageManager.replySimple(msg, res);
            }
        });
    }
}
