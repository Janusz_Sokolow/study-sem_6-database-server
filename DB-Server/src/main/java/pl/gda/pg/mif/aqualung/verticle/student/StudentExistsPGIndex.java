package pl.gda.pg.mif.aqualung.verticle.student;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.StudentDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class StudentExistsPGIndex extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(StudentExistsPGIndex.class);
    private static final String address = MESSAGES_PREFIX + ".student.existsPGIndex";

    private static final String IN_PG_INDEX = "PGIndex";

    private static final String OUT_EXISTS = "exists";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        int pgIndex = data.getInteger(IN_PG_INDEX);
        log.debug("Index: " + pgIndex);
        StudentDAO.getStudentByPGIndex(vertx, pgIndex, student -> {
            Boolean isExistng = student != null;
            MessageManager.replyValue(msg, OUT_EXISTS, isExistng);
        });
    }

}
