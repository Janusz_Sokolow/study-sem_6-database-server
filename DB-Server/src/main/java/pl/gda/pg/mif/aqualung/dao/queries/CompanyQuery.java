package pl.gda.pg.mif.aqualung.dao.queries;

import static pl.gda.pg.mif.aqualung.dao.queries.Queries.colectColumns;
import pl.gda.pg.mif.aqualung.dao.struct.Company;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class CompanyQuery extends Queries {

    public static String insertCompany(String companyName, String address) {
        return "INSERT INTO " + Company.TABLE_NAME
                + " (" + colectColumns(Company.COMPANY_NAME, Company.ADDRESS) + ") "
                + colectValues(companyName, address)
                + ";";
    }

    public static String getCompanyByName(String companyName, String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Company.TABLE_NAME
                + where(Company.T_COMPANY_NAME, companyName);
    }

    public static String getCompanyById(int companyId, String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Company.TABLE_NAME
                + where(Company.T_ID, companyId);
    }

    public static String findCompany(String... columns) {
        return "SELECT " + colectColumns(columns)
                + " FROM " + Company.TABLE_NAME;
    }
}
