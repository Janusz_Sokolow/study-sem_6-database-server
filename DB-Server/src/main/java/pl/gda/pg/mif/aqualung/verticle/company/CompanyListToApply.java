package pl.gda.pg.mif.aqualung.verticle.company;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.CompanyDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class CompanyListToApply extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(CompanyListToApply.class);
    private static final String address = MESSAGES_PREFIX + ".company.findAbleToApply";

    private static final String OUT_COMPANIES_LIST = "companiesList";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        CompanyDAO.findCompanyAbleToApply(vertx, res -> {
            MessageManager.replyValue(msg, OUT_COMPANIES_LIST, res);
        });
    }

}
