package pl.gda.pg.mif.aqualung.verticle.student;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.StudentDAO;
import pl.gda.pg.mif.aqualung.exceptions.StudentNotExistsException;
import pl.gda.pg.mif.aqualung.providers.MessageManager;
import pl.gda.pg.mif.aqualung.dao.struct.Student;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class StudentGet extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(StudentGet.class);
    private static final String address = MESSAGES_PREFIX + ".student.get";

    private static final String IN_NICKNAME = "nickName";

    private static final String OUT_STUDENT = "student";

    private static final String OUT_NICKNAME = "nickname";
    private static final String OUT_PG_INDEX = "PGIndex";
    private static final String OUT_FIRST_NAME = "firstName";
    private static final String OUT_LAST_NAME = "lastName";
    private static final String OUT_MAIL = "mail";
    private static final String OUT_SPECIALITY = "speciality";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        String nickname = data.getString(IN_NICKNAME);
        StudentDAO.getStudent(vertx, nickname, student -> {
            if (student.getInteger(Student.PG_INDEX) == null) {
                MessageManager.replyError(msg, new StudentNotExistsException());
            } else {
                JsonObject reply = new JsonObject();
                reply.put(OUT_FIRST_NAME, student.getString(Student.NAME))
                        .put(OUT_LAST_NAME, student.getString(Student.SURNAME))
                        .put(OUT_MAIL, student.getString(Student.EMAIL))
                        .put(OUT_NICKNAME, student.getString(Student.NICK_NAME))
                        .put(OUT_SPECIALITY, student.getString(Student.SPECIALITY))
                        .put(OUT_PG_INDEX, student.getInteger(Student.PG_INDEX));

                MessageManager.replyValue(msg, OUT_STUDENT, reply);
            }
        });
    }

}
