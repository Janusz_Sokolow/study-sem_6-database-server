package pl.gda.pg.mif.aqualung.dao.struct;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class Message {

    public static final String TABLE_NAME = "message";

    public static final String ID = "id";
    public static final String TITLE = "title";
    public static final String CONTENT = "content";
    public static final String AUTHOR = "author";
    public static final String CREATE_DATE = "createDate";

    public static final String[] PUBLIC_PARAMS = {
        ID,
        TITLE,
        CONTENT,
        AUTHOR,
        CREATE_DATE
    };

    public static final String T_ID = TABLE_NAME + "." + ID;
    public static final String T_TITLE = TABLE_NAME + "." + TITLE;
    public static final String T_CONTENT = TABLE_NAME + "." + CONTENT;
    public static final String T_AUTHOR = TABLE_NAME + "." + AUTHOR;
    public static final String T_CREATE_DATE = TABLE_NAME + "." + CREATE_DATE;

}
