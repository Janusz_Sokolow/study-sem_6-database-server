package pl.gda.pg.mif.aqualung.handlers;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import java.io.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Config;
import pl.gda.pg.mif.aqualung.helpers.ConfigHelper;
import pl.gda.pg.mif.aqualung.helpers.ConfigHelper;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class StaticHandler implements Handler<RoutingContext>{

    private static final Logger log = LogManager.getLogger(LoginUser.class);

    Vertx vertx;
    JsonObject viewConfig;

    public StaticHandler(Vertx vertx) {
        this.vertx = vertx;
        viewConfig = ConfigHelper.getConfig().getJsonObject(Config.VIEW_CONFIG);
    }

    @Override
    public void handle(RoutingContext rc) {
        String filename;
        HttpServerRequest req = rc.request();

        String webroot = viewConfig.getString(Config.VIEW_CONFIG_ROOT);
        if (req.path().equals("/")) {
            filename = viewConfig.getString(Config.VIEW_CONFIG_INDEX);
        } else {
            filename = req.path();
        }

        File file = new File(webroot + filename);

        if (filename != null && file.exists()) {
            req.response().sendFile(webroot + filename);
        } else {
            log.warn("----- Nie mogę znaleźć pliku : " + filename);
            rc.fail(404);
        }
    }
    
}
