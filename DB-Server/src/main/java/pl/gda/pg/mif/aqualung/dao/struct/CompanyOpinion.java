package pl.gda.pg.mif.aqualung.dao.struct;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class CompanyOpinion {

    public static final String TABLE_NAME = "companyopinion";

    public static final String ID = "id";
    public static final String COMPANY = "companyId";
    public static final String KEY = "opinionKey";
    public static final String VALUE = "opinionValue";

    public static final String[] PUBLIC_PARAMS = {
        ID,
        KEY,
        VALUE
    };

    public static final String T_ID = TABLE_NAME + "." + ID;
    public static final String T_COMPANY = TABLE_NAME + "." + COMPANY;
    public static final String T_KEY = TABLE_NAME + "." + KEY;
    public static final String T_VALUE = TABLE_NAME + "." + VALUE;

}
