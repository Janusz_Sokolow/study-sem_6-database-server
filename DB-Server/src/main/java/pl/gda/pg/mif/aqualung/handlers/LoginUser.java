package pl.gda.pg.mif.aqualung.handlers;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.providers.auth.AuthProviderPractice;
import pl.gda.pg.mif.aqualung.providers.auth.PasswordEncriptor;
import pl.gda.pg.mif.aqualung.providers.auth.Principal;
import pl.gda.pg.mif.aqualung.verticle.in.LogPerson;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz
 * Sokołow, Student of Gdańsk University of Technology
 */
public class LoginUser implements Handler<RoutingContext> {

    private static final Logger log = LogManager.getLogger(LoginUser.class);
    private static final String LOGGING_REQUESTS = "LOGGING_TRIES";
    private static final int LOGGING_REQUESTS_TIME = 60 * 1000;
    private static final int LOGGING_REQUESTS_AMOUNT = 3;

    private Vertx vertx;

    private AuthProviderPractice authProvider;

    public LoginUser(Vertx vertx) {
        this.vertx = vertx;
        authProvider = new AuthProviderPractice(vertx);
    }

    @Override
    public void handle(RoutingContext routingContext) {

        Session session = routingContext.session();

        HttpServerRequest request = routingContext.request();
        HttpServerResponse response = routingContext.response();

        request.setExpectMultipart(true);
        request.endHandler(end -> {
            request.formAttributes();
            String userName = request.getFormAttribute("login");
            String password = PasswordEncriptor.encript(request.getFormAttribute("password"));
            String userAgent = request.getHeader(HttpHeaders.USER_AGENT);

            log.debug("login -> " + userName);

            if (isPassesRequestAmountControl(session)) {
                authProvider.authenticate(new JsonObject()
                        .put(LogPerson.IN_USER_NAME, userName)
                        .put(LogPerson.IN_PASSWORD, password), user -> {
                    if (user.succeeded()) {
                        ((Principal) user.result()).setUserAgent(userAgent);
                        session.put(Principal.USER, user.result());
                        response.setStatusCode(200).end();
                    } else {
                        responseWithError(routingContext, 302);
                    }
                });
            } else {
                responseWithError(routingContext, 429);
            }
        });
    }

    private boolean isPassesRequestAmountControl(Session session) {
        List<Long> logInRequests = session.get(LOGGING_REQUESTS);
        if (logInRequests == null) {
            logInRequests = new ArrayList<>();
            session.put(LOGGING_REQUESTS, logInRequests);
        } else {
            logInRequests = logInRequests
                    .stream()
                    .filter(logginTry
                            -> System.currentTimeMillis() - logginTry < LOGGING_REQUESTS_TIME)
                    .collect(Collectors.toList());
            if (logInRequests.size() >= LOGGING_REQUESTS_AMOUNT) {
                return false;
            }
        }
        return true;
    }

    private void addRequestToRequestsAmount(Session session) {
        List<Long> logInRequests = session.get(LOGGING_REQUESTS);
        logInRequests.add(System.currentTimeMillis());
        session.put(LOGGING_REQUESTS, logInRequests);
    }

    private void responseWithError(RoutingContext rc, int statusCode) {
        addRequestToRequestsAmount(rc.session());
        String responseText;
        switch (statusCode) {
            case 302:
                responseText = "ERROR";
                break;
            case 303:
                responseText = "ERROR";
                break;
            case 407:
                responseText = "OVER REQUESTED";
                break;
            case 500:
                responseText = "SERVER ERROR";
                break;
            default:
                responseText = "ERROR";
        }
        rc.response().setStatusCode(statusCode).end(responseText);
    }

}
