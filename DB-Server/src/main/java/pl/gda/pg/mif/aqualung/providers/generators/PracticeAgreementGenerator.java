package pl.gda.pg.mif.aqualung.providers.generators;

import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import io.vertx.core.json.JsonObject;
import java.io.FileOutputStream;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Config;
import pl.gda.pg.mif.aqualung.dao.queries.Queries;
import pl.gda.pg.mif.aqualung.helpers.ConfigHelper;
import pl.gda.pg.mif.aqualung.providers.DictionaryProvider;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology kongo &&
 * Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class PracticeAgreementGenerator {

    private static final String BASIC_CARD = "Podanie_o_zaakceptowanie_pracy.pdf";
    private static final String OUTPUT_PREFIX = ConfigHelper.getConfig().getJsonObject(Config.FILE_CONFIG).getString(Config.SERVER_CONFIG_PDF_SRC) + "pdf/";
    private static final int gapLeft = 0xa; // 10
    private static final int gapDown = 0x1; // 01

    private static final int FONT_SIZE = 10;
    private static BaseFont baseFont;
    private static PdfReader basicReader;

    private static final Logger log = LogManager.getLogger(PracticeAgreementGenerator.class);

    private static JsonObject coordinatorData = ConfigHelper.getPortalConfig().getJsonObject(Config.PORTAL_COORDINATOR);

    public static String generate(
            String firstName,
            String lastName,
            String PG_Index,
            String employerName,
            String practiceStart,
            String practiceFinish,
            String workType
    ) {
        log.debug("Rozpoczynam generowanie...");
        try {
            if (baseFont == null) {
                baseFont = BaseFont.createFont("GraviolaSoft-Medium.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            }
            if (basicReader == null) {
                basicReader = new PdfReader(BASIC_CARD);
            }
            String studentName = firstName + "_" + lastName;
            String cardName = "studentPractice/" + studentName + "_agreement_" + UUID.randomUUID().toString() + ".pdf";

            String output = OUTPUT_PREFIX + cardName;

            log.debug(".. do pliku " + output + "...");
            PdfStamper pdfStamper = new PdfStamper(basicReader, new FileOutputStream(output));
            PdfContentByte firstPage = pdfStamper.getUnderContent(1);
            firstPage.beginText();
            firstPage.setFontAndSize(baseFont, FONT_SIZE);

            log.debug(".. początek generowania pierwszej strony...");
            generateFirstPage(firstPage, firstName, lastName, PG_Index, employerName, practiceStart, practiceFinish, workType);
            log.debug(".. pierwsza strona wygenerowana ...");

            pdfStamper.close();

            return "pdf/" + cardName;
        } catch (Exception e) {
            log.error("Rozpoczynam generowanie...", e);
            return null;
        }
    }

    private static void generateFirstPage(
            PdfContentByte firstPage,
            String firstName,
            String lastName,
            String PG_Index,
            String employerName,
            String practiceStart,
            String practiceFinish,
            String workType
    ) throws NullPointerException {
        // Student
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, firstName + " " + lastName, 55 + gapLeft, 759 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, "Index PG:  " + PG_Index, 55 + gapLeft, 722 + gapDown, 0);

        // Koordynator
        firstPage.showTextAligned(PdfContentByte.ALIGN_RIGHT,
                coordinatorData.getString(Config.PORTAL_COORDINATOR_NAME),
                520 + gapLeft, 688 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_RIGHT,
                DictionaryProvider.getTranslation("department." + coordinatorData.getString(Config.PORTAL_COORDINATOR_DEPARTMENT) + "_short"),
                520 + gapLeft, 671 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_RIGHT,
                DictionaryProvider.getTranslation("facultities." + coordinatorData.getString(Config.PORTAL_COORDINATOR_FACULTITY)),
                520 + gapLeft, 654 + gapDown, 0);
        // Pracodawca
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, employerName, 71 + gapLeft, 546 + gapDown, 0);
        // poczatek praktyk
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, practiceStart, 112 + gapLeft, 530 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, practiceFinish, 190 + gapLeft, 530 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, workType, 360 + gapLeft, 530 + gapDown, 0);

        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, ConfigHelper.getPortalCity() + ", " + Queries.dayNow(), 430 + gapLeft, 777 + gapDown, 0);
        firstPage.endText();
    }
}
