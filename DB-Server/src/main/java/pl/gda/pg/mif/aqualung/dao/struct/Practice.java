
package pl.gda.pg.mif.aqualung.dao.struct;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class Practice {
    
    public static final String TABLE_NAME = "practice";
    
    public static final String ID = "ID";
   
    public static final String DATE_FROM = "dateFrom";
    public static final String DATE_TO = "dateTo";
    public static final String TYPE = "type";
    public static final String STATUS = "status";
    public static final String MESSAGE = "statusMessage";
    public static final String BHP_CITY = "bhpCity";
    public static final String BHP_DATE = "bhpDate";
    public static final String BHP_EMPLOYEE = "bhpEmployee";
    
    public static final String COMPANY_ID = "company_id";
    public static final String STUDENT_PG_INDEX = "student_pgIndex";
    
     public static final String[] PUBLIC_PARAMS = {
         TABLE_NAME + '.' + ID, 
         DATE_FROM, 
         DATE_TO, 
         COMPANY_ID, 
         STUDENT_PG_INDEX,
         STATUS, 
         TYPE,
         MESSAGE,
         BHP_CITY,
         BHP_DATE,
         BHP_EMPLOYEE
     };
     
    public static final String T_ID = TABLE_NAME + "." + ID;
   
    public static final String T_DATE_FROM = TABLE_NAME + "." + DATE_FROM;
    public static final String T_DATE_TO = TABLE_NAME + "." + DATE_TO;
//    public static final String T_HOURS = TABLE_NAME + "." + HOURS;
    public static final String T_TYPE = TABLE_NAME + "." + TYPE;
    public static final String T_STATUS = TABLE_NAME + "." + STATUS;
    public static final String T_MESSAGE = TABLE_NAME + "." + MESSAGE;
    public static final String T_BHP_CITY = TABLE_NAME + "." + BHP_CITY;
    public static final String T_BHP_DATE = TABLE_NAME + "." + BHP_DATE;
    public static final String T_BHP_EMPLOYEE = TABLE_NAME + "." + BHP_EMPLOYEE;
    
    public static final String T_COMPANY_ID = TABLE_NAME + "." + COMPANY_ID;
    public static final String T_STUDENT_PG_INDEX = TABLE_NAME + "." + STUDENT_PG_INDEX;
}