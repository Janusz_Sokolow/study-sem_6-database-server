package pl.gda.pg.mif.aqualung.dao.struct;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class PracticePart {

    public static final String TABLE_NAME = "practicePart";

    public static final String ID = "PP_ID";

    public static final String CREATE_DATE = "dateCreate";
    public static final String PRACTICE_ID = "practice_id";
    public static final String STAGE = "practiceStage";
    public static final String SKILLS = "newSkills";
    public static final String DESCRIPTION = "workDescription";

    public static final String T_ID = TABLE_NAME + "." + ID;
    public static final String T_CREATE_DATE = TABLE_NAME + "." + CREATE_DATE;
    public static final String T_PRACTICE_ID = TABLE_NAME + "." + PRACTICE_ID;
    public static final String T_WEEK = TABLE_NAME + "." + STAGE;
    public static final String T_SKILLS = TABLE_NAME + "." + SKILLS;
    public static final String T_DESCRIPTION = TABLE_NAME + "." + DESCRIPTION;
}
