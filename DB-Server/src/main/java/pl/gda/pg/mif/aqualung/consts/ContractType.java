package pl.gda.pg.mif.aqualung.consts;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public enum ContractType {
    CONTRACT_PG,
    CONTRACT_OF_EMPLOYMENT,
    CONTRACT_WORK,
    CONTRACT_COMMISSION
}
