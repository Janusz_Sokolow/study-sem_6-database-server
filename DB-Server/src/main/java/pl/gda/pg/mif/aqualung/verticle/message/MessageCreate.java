    package pl.gda.pg.mif.aqualung.verticle.message;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.providers.auth.Principal;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.MessageDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class MessageCreate extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(MessageCreate.class);
    private static final String address = MESSAGES_PREFIX + ".message.create";

    private static final String IN_TITLE = "title";
    private static final String IN_CONTENT = "content";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        String author = data.getJsonObject(Principal.USER).getString(Principal.USERNAME);
        String content = data.getString(IN_CONTENT);
        String title = data.getString(IN_TITLE);

        MessageDAO.insertMessage(vertx, title, content, author, res -> {
            MessageManager.replySimple(msg, res);
        });
    }

}
