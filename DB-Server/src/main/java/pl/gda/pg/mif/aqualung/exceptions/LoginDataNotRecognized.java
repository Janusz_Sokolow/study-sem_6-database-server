package pl.gda.pg.mif.aqualung.exceptions;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class LoginDataNotRecognized  extends PracticeException{

    public LoginDataNotRecognized() {
        super("LoginDataNotRecognized");
    }
}
