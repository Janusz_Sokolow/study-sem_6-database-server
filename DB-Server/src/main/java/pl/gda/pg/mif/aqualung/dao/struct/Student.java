package pl.gda.pg.mif.aqualung.dao.struct;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class Student {

    public static final String TABLE_NAME = "student";

    /**
     * Musi być unikatowym numerem, zgodym z ideksem politechnicznym
     */
    public static final String PG_INDEX = "PG_INDEX";
    public static final String NICK_NAME = "nickName";
    public static final String PASSWORD = "password";

    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String SPECIALITY = "speciality";
    public static final String EMAIL = "email";

    public static final String[] PUBLIC_PARAMS ={
            PG_INDEX,
            NICK_NAME,
            NAME,
            SURNAME,
            SPECIALITY,
            EMAIL
    };
    

    public static final String T_PG_INDEX = TABLE_NAME + "." + PG_INDEX;
    public static final String T_NICK_NAME = TABLE_NAME + "." + NICK_NAME;
    public static final String T_PASSWORD = TABLE_NAME + "." + PASSWORD;

    public static final String T_NAME = TABLE_NAME + "." + NAME;
    public static final String T_SURNAME = TABLE_NAME + "." + SURNAME;
    public static final String T_SPECIALITY = TABLE_NAME + "." + SPECIALITY;
    public static final String T_EMAIL = TABLE_NAME + "." + EMAIL;
}

//DROP SCHEMA IF EXISTS `practiceportal`;
//CREATE SCHEMA `practiceportal` ;
//
//DROP TABLE IF EXISTS `practiceportal`.`student`;
//CREATE TABLE `practiceportal`.`student`(
//		`PG_INDEX` INT NOT NULL PRIMARY KEY,
//        `nickName` VARCHAR(20),
//        `password` VARCHAR(64), 
//        `name` VARCHAR(24),
//        `surname` VARCHAR(36),
//        `speciality` VARCHAR(36),
//        `email` VARCHAR(36));
//
//
//DROP TABLE IF EXISTS `practiceportal`.`company`;
//CREATE TABLE `practiceportal`.`company` (
//  `id` INT NOT NULL  PRIMARY KEY AUTO_INCREMENT,
//  `companyName` VARCHAR(45) NOT NULL,
//  `phoneNumber` VARCHAR(45) NULL);
//
//
//DROP TABLE IF EXISTS `practiceportal`.`practice`;
//CREATE TABLE `practiceportal`.`practice` (
//  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
//  `dateFrom` DATE NULL,
//  `dateTo` DATE NULL,
//  `hours` INT UNSIGNED NOT NULL,
//  `student_pgIndex` INT ,
//  `company_id` INT,
//  `status` VARCHAR(25) NOT NULL,
//	FOREIGN KEY (`student_pgIndex`) REFERENCES student(`PG_INDEX`)  ON DELETE CASCADE,
//	FOREIGN KEY (`company_id`) REFERENCES company(`id`) ON DELETE SET NULL)
//
//
//DROP TABLE IF EXISTS `practiceportal`.`practicePart`;
//CREATE TABLE `practiceportal`.`practicePart` (
//  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
//  `dateCreate` DATE NULL,
//  `practiceWeek` INT UNSIGNED NOT NULL,
//  `practice_id` INT UNSIGNED,
//  `workDescription` VARCHAR(255) NOT NULL,
//  `newSkills` VARCHAR(255),
//	FOREIGN KEY (`practice_id`) REFERENCES practice(`id`)  ON DELETE CASCADE);
