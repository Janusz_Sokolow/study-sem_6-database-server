package pl.gda.pg.mif.aqualung.providers.auth;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology janusz
 */
public class PasswordEncriptor {

    private static final int iterations = 20 * 487;
    private static final int saltLen = 128;
    private static final String secretKeyAlgorithm = "PBKDF2WithHmacSHA256";
    private static final byte[] salt = "IdB4uyKi3pyocRwwl49rRw==".getBytes();

    private static final Random random = new Random();

    public static String encript(String password) {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, saltLen);
        try {
            SecretKeyFactory f = SecretKeyFactory.getInstance(secretKeyAlgorithm);
            byte[] hash = f.generateSecret(spec).getEncoded();
            Base64.Encoder enc = Base64.getEncoder();
            System.out.printf("hash: %s%n", enc.encodeToString(hash));
            return enc.encodeToString(hash);
        } catch (InvalidKeySpecException | NoSuchAlgorithmException ex) {
            Logger.getLogger(PasswordEncriptor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
