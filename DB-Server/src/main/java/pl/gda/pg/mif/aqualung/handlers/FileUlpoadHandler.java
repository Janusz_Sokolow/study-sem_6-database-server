package pl.gda.pg.mif.aqualung.handlers;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.DocumentTypes;
import pl.gda.pg.mif.aqualung.dao.DocumentDAO;
import pl.gda.pg.mif.aqualung.dao.StudentDAO;
import pl.gda.pg.mif.aqualung.dao.struct.Student;
import pl.gda.pg.mif.aqualung.helpers.FileHelper;
import pl.gda.pg.mif.aqualung.providers.auth.Principal;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology janusz
 *
 * https://github.com/danialfarid/ng-file-upload/blob/master/demo/src/main/java/com/df/angularfileupload/FileUpload.java
 */
public class FileUlpoadHandler implements Handler<RoutingContext> {

    Vertx vertx;
    private static final Logger log = LogManager.getLogger(FileUlpoadHandler.class);

    class SizeEntry {

        public int size;
        public LocalDateTime time;
    }

    static Map<String, SizeEntry> sizeMap = new ConcurrentHashMap<>();
    int counter;

    public FileUlpoadHandler(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void handle(RoutingContext rc) {
        log.debug(" handle ");
        HttpServerRequest req = rc.request();
        HttpServerResponse res = rc.response();
        clearOldValuesInSizeMap();

        String ipAddress = req.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = req.remoteAddress() != null ? req.remoteAddress().host() : null;
        }
        log.debug("ipAddress -> " + ipAddress);

        if (req.method().equals(HttpMethod.GET)) {
            log.debug("  GET --- " + req.getParam("errorCode"));
            if (req.getParam("restart") != null) {
                sizeMap.remove(ipAddress + req.getParam("name"));
            }
            SizeEntry entry = sizeMap.get(ipAddress + req.getParam("name"));
            res.putHeader(HttpHeaders.CONTENT_TYPE, "application/json").end("{\"size\":" + (entry == null ? 0 : entry.size) + "}");
            return;
        }
        if (!req.method().equals(HttpMethod.OPTIONS) && req.getParam("errorCode") != null) {
            log.debug("  OPTIONS --- " + req.getParam("errorCode"));
            res.setStatusCode(Integer.parseInt(req.getParam("errorCode"))).setStatusMessage(req.getParam("errorMessage")).end("", "utf-8");
            return;
        }
        JsonArray result = new JsonArray();
        JsonObject requestHeaders = new JsonObject();

        log.debug("Header - Content-Type -> " + req.getHeader("Content-Type"));
        if (req.getHeader(HttpHeaders.CONTENT_TYPE) != null
                && req.getHeader("Content-Type").startsWith("multipart/form-data")) {

            Set<FileUpload> uploads = rc.fileUploads();
            Iterator<FileUpload> iterator = uploads.iterator();

            int numberOfObjects = uploads.size();
            int objectsProcessed = 0;
            log.debug("uploads size -> " + numberOfObjects);
            while (iterator.hasNext()) {
                JsonObject fileObj = new JsonObject();
                FileUpload item = iterator.next();

                Buffer uploadedFile = vertx.fileSystem().readFileBlocking(item.uploadedFileName());
                byte[] bytes = uploadedFile.getBytes();

                InputStream is = new ByteArrayInputStream(uploadedFile.toString("utf-8").getBytes());

                String documentType = req.getParam("type");
                Integer practiceId = new Integer(req.getParam("practiceId"));
                long studentIndex
                        = ((Principal) rc.session().get(Principal.USER)).getId();
                fileObj.put("fieldName", item.fileName());
                if (item.name() != null) {
                    final String sizeMapKey = ipAddress + item.name();
                    StudentDAO.getStudentByPGIndex(vertx, studentIndex, student -> {
                        String documentName
                                = student.getString(Student.NAME) + "_"
                                + student.getString(Student.SURNAME) + "-"
                                + documentType + "_" + item.fileName();

                        String type = documentName.substring(documentName.lastIndexOf("."));
                        String savedPdfSrc;
                        if (type.contains("pdf")) {
                            savedPdfSrc = FileHelper.savePdf(bytes, documentName);
                        } else {
                            savedPdfSrc = FileHelper.saveImage(bytes, documentName);
                        }
                        fileObj.put("name", savedPdfSrc);
                        fileObj.put("size", size(sizeMapKey, is));
                        DocumentDAO.insertDocument(vertx, practiceId, savedPdfSrc, documentName, DocumentTypes.valueOf(documentType), saved -> {
                            log.debug("Inserting dokument for " + practiceId + " finished. " + saved.getKeys().encode());
                            responseNow(req, res, requestHeaders, result);
                        });
                    });
                } else {
                    log.debug("Dołączam wartość : " + read(is));
                    fileObj.put("value", read(is).replace("\"", "'"));
                }
                result.add(fileObj);
            }
        } else {
            log.warn("Chcę przesłać rozmiar, lecz nie potrafię ;< JESZCZE");
//            log.warn("" + req.);
//            sb.append("{\"size\":\"" + size(ipAddress, req.getInputStream()) + "\"}");
            JsonObject sizeObj = new JsonObject().put("size", 12345);
            result.add(sizeObj);
            responseNow(req, res, requestHeaders, result);
        }
    }

    private void responseNow(HttpServerRequest req, HttpServerResponse res, JsonObject requestHeaders, JsonArray result) {
        Iterator<Map.Entry<String, String>> iterator = req.headers().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> next = iterator.next();
            requestHeaders.put(next.getKey(), next.getValue());
        }

        log.debug(" odpowiadam przez Json: " + new JsonObject().put("result", result).put("requestHeaders", requestHeaders).encodePrettily());
        res.end(new JsonObject().put("result", result).put("requestHeaders", requestHeaders).encode());
    }

    private void clearOldValuesInSizeMap() {
        if (counter++ == 100) {
            for (Map.Entry<String, SizeEntry> entry : sizeMap.entrySet()) {
                if (entry.getValue().time.isBefore(LocalDateTime.now().minusHours(1))) {
                    sizeMap.remove(entry.getKey());
                }
            }
            counter = 0;
        }
    }

    protected int size(String key, InputStream stream) {
        int length = sizeMap.get(key) == null ? 0 : sizeMap.get(key).size;
        try {
            byte[] buffer = new byte[200000];
            int size;
            while ((size = stream.read(buffer)) != -1) {
                length += size;
                SizeEntry entry = new SizeEntry();
                entry.size = length;
                entry.time = LocalDateTime.now();
                sizeMap.put(key, entry);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println(length);
        return length;

    }

    protected String read(InputStream stream) {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                //ignore
            }
        }
        return sb.toString();
    }
}
