package pl.gda.pg.mif.aqualung.handlers;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.sockjs.BridgeEvent;
import io.vertx.ext.web.handler.sockjs.BridgeEventType;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import java.util.Iterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.providers.auth.Principal;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology
 * Janusz.Sokolow
 */
public class EventBusHandler {

    Vertx vertx;
    private static final Logger log = LogManager.getLogger(EventBusHandler.class);
    private BridgeOptions options;

    public EventBusHandler(Vertx vertx) {
        this.vertx = vertx;
    }

    public Handler<RoutingContext> createEventBusBridge(JsonObject portalOptions) {
        options = new BridgeOptions();
        Iterator<Object> inIterator = portalOptions.getJsonObject("socketRules").getJsonArray("in").iterator();
        Iterator<Object> outIterator = portalOptions.getJsonObject("socketRules").getJsonArray("out").iterator();

        while (inIterator.hasNext()) {
            JsonObject next = (JsonObject) inIterator.next();
            options.addInboundPermitted(new PermittedOptions(next));
        }
        while (outIterator.hasNext()) {
            JsonObject next = (JsonObject) outIterator.next();
            options.addInboundPermitted(new PermittedOptions(next));
        }

        return SockJSHandler.create(vertx).bridge(options, this::handleSockJSEvent);
    }

    /**
     * @param evt
     */
    private void handleSockJSEvent(BridgeEvent evt) {

        final JsonObject msg = evt.getRawMessage();

        Principal principal = (Principal) evt.socket().webSession().get(Principal.USER);
        if (msg != null && msg.getString("type") != null && !msg.getString("type").equals("rec")) {
            JsonObject bodyRaw = (JsonObject) msg.getValue("body");
            if (bodyRaw != null && evt.type().equals(BridgeEventType.SEND) && principal != null) {
                bodyRaw.put(Principal.USER, principal.principal());
            }
            msg.put("body", bodyRaw);
        }
        evt.complete(true);
    }
}
