
package pl.gda.pg.mif.aqualung.exceptions;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public abstract class PracticeException extends Exception{

    public PracticeException() {
        super();
    }
    public PracticeException(String message) {
        super(message);
    }
    public PracticeException(Throwable cause) {
        super(cause);
    }
    public PracticeException(String message, Throwable cause) {
        super(message, cause);
    }
}
