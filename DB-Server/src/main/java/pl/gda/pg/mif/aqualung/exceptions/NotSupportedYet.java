package pl.gda.pg.mif.aqualung.exceptions;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class NotSupportedYet extends PracticeException {

    public static final String message = "practice_exceptions.not_suppoerted_yet";

    public NotSupportedYet() {
        super(message);
    }
}
