/*
 * Klasa odpowiada za stworzenie i przeslanie zapytania modyfikującego 
 * tablicę Student w bazie danych.
 * Także ona odpowiada za bezpieczeństwo i poprawnośc wykonanych operacji
 */
package pl.gda.pg.mif.aqualung.dao;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.UpdateResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Specialities;
import pl.gda.pg.mif.aqualung.dao.queries.StudentQuery;
import pl.gda.pg.mif.aqualung.dao.struct.Student;
import pl.gda.pg.mif.aqualung.providers.DatabaseQueryProvider;
import pl.gda.pg.mif.aqualung.providers.DatabaseUpdateProvider;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class StudentDAO {

    private static final Logger log = LogManager.getLogger(StudentDAO.class);

    public static void insertStudent(Vertx vertx, int index, String nickName, String password, String name, String surname, String eMail, Specialities speciality,
            Handler<UpdateResult> handler) {
        String sqlUpdate = StudentQuery.insertStudent(index, nickName, password, name, surname, eMail, speciality);
        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }

    public static void findStudent(Vertx vertx, Handler<JsonArray> handler) {
        String sqlUpdate = StudentQuery.findStudents(
                Student.PUBLIC_PARAMS);
        DatabaseQueryProvider.query(vertx, sqlUpdate, resultHandler -> {
            if (resultHandler == null) {
                handler.handle(null);
            } else {
                JsonArray result = new JsonArray();
                resultHandler.getResults().forEach(row -> {
                    result.add(new JsonObject()
                            .put(Student.PUBLIC_PARAMS[0], row.getValue(0))
                            .put(Student.PUBLIC_PARAMS[1], row.getValue(1))
                            .put(Student.PUBLIC_PARAMS[2], row.getValue(2))
                            .put(Student.PUBLIC_PARAMS[3], row.getValue(3))
                            .put(Student.PUBLIC_PARAMS[4], row.getValue(4))
                    );
                });
                handler.handle(result);
            }
        });
    }

    public static void getStudent(Vertx vertx, String nickName, Handler<JsonObject> handler) {
        String[] collumns = {Student.PG_INDEX, Student.NICK_NAME, Student.NAME, Student.EMAIL, Student.SURNAME, Student.SPECIALITY};
        String sqlUpdate = StudentQuery.getStudentByNickName(nickName, collumns);
        DatabaseQueryProvider.query(vertx, sqlUpdate, resultHandler -> {
            if (resultHandler != null && resultHandler.getRows().size() > 0) {
                handler.handle(resultHandler.getRows().get(0));
            } else {
                handler.handle(null);
            }
        });
    }

    public static void getStudent(Vertx vertx, String nickName, String password, Handler<JsonObject> handler) {
        String sqlUpdate = StudentQuery.getStudentByNickName(nickName, password, 
                Student.PUBLIC_PARAMS);
        DatabaseQueryProvider.query(vertx, sqlUpdate, resultHandler -> {
            if (resultHandler != null && resultHandler.getRows().size() > 0) {
                handler.handle(resultHandler.getRows().get(0));
            } else {
                handler.handle(null);
            }
        });
    }

    public static void getStudentByPGIndex(Vertx vertx, long pgIndex, Handler<JsonObject> handler) {
        String sqlUpdate = StudentQuery.getStudentByPGIndex(pgIndex,
                Student.PUBLIC_PARAMS);
        DatabaseQueryProvider.query(vertx, sqlUpdate, resultHandler -> {
            if (resultHandler != null && resultHandler.getRows().size() > 0) {
                handler.handle(resultHandler.getRows().get(0));
            } else {
                handler.handle(null);
            }
        });
    }

    public void dropStudentByIndex(Vertx vertx, int index) {
        String sqlUpdate = StudentQuery.dropStudentByIndexUpdate(index);
        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, updateResult -> {
        });
    }

    public static void updateStudentNameSurnameByIndex(Vertx vertx, int index, String name, String surname, String mail, Specialities speciality, Handler<UpdateResult> handler) {
        String sqlUpdate = StudentQuery.updateStudentNameSurnameByIndexUpdate(index, name, surname, mail, speciality);
        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);

    }

}
