package pl.gda.pg.mif.aqualung;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Config;
import pl.gda.pg.mif.aqualung.handlers.EventBusHandler;
import pl.gda.pg.mif.aqualung.handlers.FileUlpoadHandler;
import pl.gda.pg.mif.aqualung.handlers.LoginUser;
import pl.gda.pg.mif.aqualung.helpers.ConfigHelper;
import pl.gda.pg.mif.aqualung.providers.auth.Principal;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Waniusza
 */
public class Server extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(Server.class);

    JsonObject serverOptions;
    JsonObject portalOptions;
    JsonObject portalFileOptions;

    private final long SESSION_TIMEUT = 1000 * 60 * 30; // 30 minutes

    @Override
    public void start() throws Exception {
        log.debug("Start Serwer start");
        serverOptions = ConfigHelper.getConfig().getJsonObject(Config.SERVER_OPTIONS);
        portalOptions = ConfigHelper.getPortalConfig();
        portalFileOptions = ConfigHelper.getConfig().getJsonObject(Config.FILE_CONFIG);
        super.start(); //To change body of generated methods, choose Tools | Templates.

        HttpServer httpServer = vertx.createHttpServer(new HttpServerOptions(serverOptions));

        Router router = Router.router(vertx);

        router.route()
                .handler(CookieHandler.create());
        router.route()
                .handler(SessionHandler.create(LocalSessionStore.create(vertx))
                        .setNagHttps(false)
                        .setSessionTimeout(SESSION_TIMEUT)
                        .setCookieHttpOnlyFlag(true));
        router.route().handler(rc -> {
            Session session = rc.session();
            if (session != null
                    && session.get(Principal.USER) != null
                    && !rc.request().getHeader(HttpHeaders.USER_AGENT)
                    .equals(((Principal) session.get(Principal.USER)).getUserAgent())) {
                session.destroy();
                rc.response().setStatusCode(403).end();
            } else {
                rc.next();
            }
        });
        router.route("/ping").handler(rc -> {
            rc.response().end();
        });
        router.route()
                .handler(requestHandler -> {
                    Principal user = (Principal) requestHandler.session().get(Principal.USER);
                    requestHandler.setUser(user);
                    requestHandler.next();
                });

        router.route("/api/auth").handler(new LoginUser(vertx));
        router.route("/api/logout").handler(this::logoutUser);
        router.route("/api/permissions").handler(this::getPermissions);

        router.routeWithRegex(
                "^" + portalFileOptions.getString(Config.SERVER_CONFIG_PDF_PATH) + "(.*\\.(pdf|jpg|png|jpeg))$").handler(
                StaticHandler
                .create(portalFileOptions.getString(Config.SERVER_CONFIG_PDF_SRC))
                .setCachingEnabled(true));

        router.route("/eventbus/*").handler(new EventBusHandler(vertx).createEventBusBridge(portalOptions));

        router.route("/upload/form").handler(BodyHandler.create().setUploadsDirectory("uploads"));
        router.route("/upload/form").handler(new FileUlpoadHandler(vertx));
        router.route("/*").handler(new pl.gda.pg.mif.aqualung.handlers.StaticHandler(vertx));

        httpServer.requestHandler(router::accept).listen();
    }

    private void logoutUser(RoutingContext routingContext) {
        routingContext.session().remove(Principal.USER);
        routingContext.response().setStatusCode(200).end("OK");
    }

    private void getPermissions(RoutingContext routingContext) {
        log.debug("Ładuję użytkownika sesji");
        HttpServerResponse response = routingContext.response();
        Principal user = (Principal) routingContext.user();
        JsonObject result;
        if (user == null) {
            result = Principal.anonymousPrincipal();
        } else {
            result = user.principal();
        }
        response.setStatusCode(200).end(result.encode());
    }
}
