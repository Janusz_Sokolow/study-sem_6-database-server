package pl.gda.pg.mif.aqualung.dao;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.Iterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.dao.queries.OpinionQuery;
import pl.gda.pg.mif.aqualung.dao.struct.CompanyOpinion;
import pl.gda.pg.mif.aqualung.providers.DatabaseQueryProvider;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class OpinionDAO {

    private static final Logger log = LogManager.getLogger(OpinionDAO.class);

    public static void findCompanyOpinion(Vertx vertx, int companyId, Handler<JsonObject> handler) {
        log.debug("findCompanyOpinion :: Przygotowuję zapytanie");
        String query = OpinionQuery.findCompanyOpinion(companyId);

        DatabaseQueryProvider.query(vertx, query, resultSet -> {
            log.debug("findCompanyOpinion :: otrzymałem wynik");
            Iterator<JsonObject> iterator = resultSet.getRows().iterator();
            JsonObject result = new JsonObject();

            while (iterator.hasNext()) {
                JsonObject next = iterator.next();

                JsonArray values = result.getJsonArray(next.getString(CompanyOpinion.KEY), new JsonArray());
                values.add(next.getString(CompanyOpinion.VALUE));
                result.put(next.getString(CompanyOpinion.KEY), values);
            }
            handler.handle(result);
        });
    }
}
