package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.providers.auth.Principal;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.consts.Roles;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.exceptions.PracticeServerException;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class PracticeShow extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticeShow.class);
    private static final String address = MESSAGES_PREFIX + ".practice.show";

    private static final String IN_PRACTICE_ID = "practiceId";

    private static final String OUT_DESCRIPTIONS_LIST = "practice";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        int practiceId = data.getInteger(IN_PRACTICE_ID);
        try {
            boolean isCoord = data.getJsonObject(Principal.USER).getJsonArray(Principal.ROLES).contains(Roles.COORDINATOR);

            PracticeDAO.getPractice(vertx, practiceId, res -> {
                log.debug("Praktyka posiada opisów: " + res);
//            if (res.getInteger(Practice.STUDENT_PG_INDEX).equals(data.))
                MessageManager.replyValue(msg, OUT_DESCRIPTIONS_LIST, res);
            });
        } catch (NullPointerException ex) {
                MessageManager.replyError(msg, new PracticeServerException());

        }

    }
;

}
