package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class PracticeSetBHP extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticeSetBHP.class);
    private static final String address = MESSAGES_PREFIX + ".practice.setBHP";

    private static final String IN_PRACTICE_ID = "practiceId";
    private static final String IN_NEW_BHP_CITY = "BHPCity";
    private static final String IN_NEW_BHP_DATE = "BHPDate";
    private static final String IN_NEW_BHP_EMPLOYEE = "BHPEmployee";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        long id = data.getLong(IN_PRACTICE_ID);
        String BHPCity = data.getString(IN_NEW_BHP_CITY);
        long BHPDate = data.getLong(IN_NEW_BHP_DATE);
        String BHPEmployee = data.getString(IN_NEW_BHP_EMPLOYEE);

        PracticeDAO.setBHP(vertx, id, BHPCity, BHPDate, BHPEmployee, res -> {
            MessageManager.replySimple(msg, res);
        });
    }
}
