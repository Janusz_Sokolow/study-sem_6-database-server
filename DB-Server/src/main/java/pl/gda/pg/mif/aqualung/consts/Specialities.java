package pl.gda.pg.mif.aqualung.consts;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public enum Specialities {

    TECHNICAL_PHYSICS, APPLIED_MATHEMATICS, APPLIED_INFORMATHICS, CONVERTION_OF_ENERGY;
}
