package pl.gda.pg.mif.aqualung.providers;

import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.UpdateResult;
import pl.gda.pg.mif.aqualung.exceptions.PracticeServerException;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class MessageManager {

    public static String ERROR_FIELD = "error";
    public static String CAUSE_FIELD = "cause";
    public static String OK_FIELD = "ok";

    public static void replySimple(Message<JsonObject> msg, UpdateResult res) {
        if (res == null) {
            error(msg, new PracticeServerException());
        } else {
            ok(msg);
        }
    }

    public static void replyValue(Message<JsonObject> msg, String fieldName, Object value) {
        if (value == null) {
            error(msg, new PracticeServerException());
        } else {
            ok(msg, fieldName, value);
        }
    }

    public static void replyError(Message<JsonObject> msg, Exception cause) {
        error(msg, cause);
    }

    private static void ok(Message<JsonObject> msg) {
        msg.reply(new JsonObject()
                .put(OK_FIELD, Boolean.TRUE));
    }

    private static void ok(Message<JsonObject> msg, String fieldName, Object value) {
        if (value instanceof JsonObject && ((JsonObject) value).containsKey(ERROR_FIELD)) {
            error(msg, ((JsonObject) value).getString(CAUSE_FIELD));
        } else {
            msg.reply(new JsonObject()
                    .put(OK_FIELD, Boolean.TRUE)
                    .put(fieldName, value));
        }
    }

    private static void error(Message<JsonObject> msg, Exception cause) {
        msg.reply(new JsonObject()
                .put(OK_FIELD, Boolean.FALSE)
                .put(ERROR_FIELD, cause.getMessage()));
    }

    private static void error(Message<JsonObject> msg, String cause) {
        msg.reply(new JsonObject()
                .put(OK_FIELD, Boolean.FALSE)
                .put(ERROR_FIELD, cause));
    }
}
