package pl.gda.pg.mif.aqualung.providers;

import java.util.Arrays;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class ArrayProvider {

    public static <T> T[] append(T[] arr, T element) {
        final int N = arr.length;
        arr = Arrays.copyOf(arr, N + 1);
        arr[N] = element;
        return arr;
    }

    public static <T> T[] appendAll(T[] arr, T... element) {
        final int N = arr.length;
        arr = Arrays.copyOf(arr, N + element.length);
        for (int i = 0, n = element.length; i < n; i++) {
            arr[N + i] = element[i];
        }
        return arr;
    }
}
