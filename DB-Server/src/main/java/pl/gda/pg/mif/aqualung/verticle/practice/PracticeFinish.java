package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class PracticeFinish extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticeFinish.class);
    private static final String address = MESSAGES_PREFIX + ".practice.finish";

    private static final String IN_COMPANY_ID = "companyId";
    private static final String IN_PRACTICE_ID = "practiceId";
    private static final String IN_SELECTED_LESSONS = "selectedLessons";
    private static final String IN_QUESTIONNAIRE = "questionnaire";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        long practiceId = data.getLong(IN_PRACTICE_ID);
        long companyId = data.getLong(IN_COMPANY_ID);
        JsonObject selectedLessons = data.getJsonObject(IN_SELECTED_LESSONS);
        JsonObject questionnaire = data.getJsonObject(IN_QUESTIONNAIRE);

        PracticeDAO.finishPractice(vertx, practiceId, companyId, questionnaire, selectedLessons, res -> {
            MessageManager.replySimple(msg, res);
        });
    }
}
