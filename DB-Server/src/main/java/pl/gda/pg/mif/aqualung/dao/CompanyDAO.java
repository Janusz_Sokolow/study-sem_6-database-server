/*
 * Klasa odpowiada za stworzenie i przeslanie zapytania modyfikującego 
 * tablicę Company w bazie danych.
 * Także ona odpowiada za bezpieczeństwo i poprawnośc wykonanych operacji
 */
package pl.gda.pg.mif.aqualung.dao;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.UpdateResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.dao.queries.CompanyQuery;
import pl.gda.pg.mif.aqualung.dao.struct.Company;
import pl.gda.pg.mif.aqualung.providers.DatabaseQueryProvider;
import pl.gda.pg.mif.aqualung.providers.DatabaseUpdateProvider;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow
 */
public class CompanyDAO {

    private static final Logger log = LogManager.getLogger(CompanyDAO.class);

    public static void insertCompany(Vertx vertx, String companyNama, String address, Handler<UpdateResult> handler) {
        log.debug("insertCompany ::Przygotowuję zapytanie");
        String sqlUpdate = CompanyQuery.insertCompany(companyNama, address);

        DatabaseUpdateProvider.updateQuery(vertx, sqlUpdate, handler);
    }

    public static void getCompany(Vertx vertx, String companyName, Handler<JsonObject> handler) {
        String[] collumns = {Company.T_COMPANY_NAME};
        String sqlUpdate = CompanyQuery.getCompanyByName(companyName, collumns);
        log.debug("getCompany :: Przygotowałem zapytanie : " + sqlUpdate);
        DatabaseQueryProvider.query(vertx, sqlUpdate, resultHandler -> {
            JsonObject result;
            if (resultHandler != null && resultHandler.getRows().size() > 0) {
                result = resultHandler.getRows().get(0);
            } else {
                result = null;
            }
            handler.handle(result);
        });
    }
        public static void getCompanyById(Vertx vertx, int companyId, Handler<JsonObject> handler) {
        String[] collumns = {Company.T_COMPANY_NAME, Company.T_ADDRESS};
        String sqlUpdate = CompanyQuery.getCompanyById(companyId, collumns);
        log.debug("getCompanyById :: Przygotowałem zapytanie : " + sqlUpdate);
        DatabaseQueryProvider.query(vertx, sqlUpdate, resultHandler -> {
            JsonObject result;
            if (resultHandler != null && resultHandler.getRows().size() > 0) {
                result = resultHandler.getRows().get(0);
            } else {
                result = null;
            }
            handler.handle(result);
        });
    }

    public static void findCompany(Vertx vertx, Handler<JsonArray> handler) {
        String sqlUpdate = CompanyQuery.findCompany(
                Company.ID,
                Company.COMPANY_NAME
        );

        DatabaseQueryProvider.query(vertx, sqlUpdate, queryResult -> {
            JsonArray result;
            if (queryResult != null) {
                result = new JsonArray(queryResult.getRows());
                handler.handle(result);
            } else {
                handler.handle(null);
            }
        });
    }
    
    public static void findCompanyAbleToApply(Vertx vertx, Handler<JsonArray> handler) {
        String sqlUpdate = CompanyQuery.findCompany(
                Company.COMPANY_NAME
        );
        DatabaseQueryProvider.query(vertx, sqlUpdate, queryHandler -> {
            JsonArray result;
            if (queryHandler == null) {
                result = null;
            } else {
                result = new JsonArray();
                queryHandler.getResults().forEach(row -> {
                    result.add(new JsonObject()
                            .put(Company.COMPANY_NAME, row.getValue(0))
                            .put("practiceCount", row.getValue(1))
                            .put("hoursSum", row.getValue(2)));
                });
            }
            handler.handle(result);
        });
    }
}
