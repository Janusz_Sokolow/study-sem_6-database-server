package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class PracticeRemove extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticeRemove.class);
    private static final String address = MESSAGES_PREFIX + ".practice.remove";

    private static final String IN_PRACTICE_ID = "id";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        int id = data.getInteger(IN_PRACTICE_ID);

        log.debug("practiceId : " + id);
        PracticeDAO.removePracticeById(vertx, id, res -> {
            MessageManager.replySimple(msg, res);
        });
    }
}
