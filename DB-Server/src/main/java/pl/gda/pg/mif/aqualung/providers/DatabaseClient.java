package pl.gda.pg.mif.aqualung.providers;

import pl.gda.pg.mif.aqualung.helpers.ConfigHelper;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.AsyncSQLClient;
import io.vertx.ext.asyncsql.MySQLClient;
import io.vertx.ext.sql.SQLConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Config;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology student
 */
public class DatabaseClient {

    private static final Logger log = LogManager.getLogger(DatabaseClient.class);

    private static final JsonObject mySQLClientConfig =  ConfigHelper.getConfig().getJsonObject(Config.DB_CONFIG);

    private static AsyncSQLClient mySQLClient = null;

    public static final AsyncSQLClient getClient(Vertx vertx) {
        if (mySQLClient == null) {
            mySQLClient = MySQLClient.createShared(vertx, mySQLClientConfig);
            log.debug("Prawidłowo uruchowiłem klienta: " + mySQLClientConfig.encodePrettily());
        }
        return mySQLClient;
    }

    public static void getConnection(Vertx vertx, Handler<SQLConnection> resultHandle) {
        log.debug("Tworzę połączenie : ");
        getClient(vertx).getConnection((res -> {
            if (res.failed()) {
                log.error("Otworzenie połączenia z bazą danych nie powiodło się.", res.cause());
            }
            resultHandle.handle(res.result());
        }));
    }
}
