package pl.gda.pg.mif.aqualung.verticle.practice;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.dao.PracticeDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class PracticePartFind extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(PracticePartFind.class);
    private static final String address = MESSAGES_PREFIX + ".practice.part.find";

    private static final String IN_PRACTICE_ID = "practiceId";
    
    private static final String OUT_NEXT_NUMBER = "descriptionsList";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        int practiceId = data.getInteger(IN_PRACTICE_ID);

        PracticeDAO.findPracticePart(vertx, practiceId, res -> {
            log.debug("Praktyka posiada opisów: " + res);
            MessageManager.replyValue(msg, OUT_NEXT_NUMBER, res);
        });
    }
}
