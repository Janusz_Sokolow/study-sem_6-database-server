package pl.gda.pg.mif.aqualung.verticle.student;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static pl.gda.pg.mif.aqualung.consts.Config.MESSAGES_PREFIX;
import pl.gda.pg.mif.aqualung.consts.Specialities;
import pl.gda.pg.mif.aqualung.dao.StudentDAO;
import pl.gda.pg.mif.aqualung.providers.MessageManager;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class StudentUpdate extends AbstractVerticle {

    private static final Logger log = LogManager.getLogger(StudentUpdate.class);
    private static final String address = MESSAGES_PREFIX + ".student.update";

    private static final String IN_PG_INDEX = "PGIndex";
    private static final String IN_FIRST_NAME = "firstName";
    private static final String IN_LAST_NAME = "lastName";
    private static final String IN_MAIL = "mail";
    private static final String IN_SPECIALITY = "speciality";

    @Override
    public void start() throws Exception {
        log.debug(" start");
        vertx.eventBus().consumer(address, this::handler);
    }

    private void handler(Message<JsonObject> msg) {
        JsonObject data = msg.body();
        log.debug(" handle - " + data);

        String name = data.getString(IN_FIRST_NAME);
        String surname = data.getString(IN_LAST_NAME);
        String mail = data.getString(IN_MAIL);
        Specialities speciality = Specialities.valueOf(data.getString(IN_SPECIALITY));
        Integer GUTIndex = data.getInteger(IN_PG_INDEX);

        StudentDAO.updateStudentNameSurnameByIndex(vertx, GUTIndex, name, surname, mail, speciality, res -> {
            MessageManager.replySimple(msg, res);
        });
    }
}
