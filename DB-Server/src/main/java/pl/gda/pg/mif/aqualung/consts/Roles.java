package pl.gda.pg.mif.aqualung.consts;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class Roles {
    
    public static final String ADMIN = "ADMIN";
    public static final String COORDINATOR = "COORDINATOR";
    public static final String STUDENT = "STUDENT";
//    public static final String REGISTRED = "REGISTRED";
}
