package pl.gda.pg.mif.aqualung.dao.struct;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class SubjectOpinion {

    public static final String TABLE_NAME = "subjectopinion";

    public static final String ID = "id";
    public static final String DESCRIPTION = "description";
    public static final String INDEX = "subjectIndex";

    public static final String[] PUBLIC_PARAMS = {
        ID,
        DESCRIPTION,
        INDEX
    };

    public static final String T_ID = TABLE_NAME + "." + ID;
    public static final String T_DESCRIPTION = TABLE_NAME + "." + DESCRIPTION;
    public static final String T_INDEX = TABLE_NAME + "." + INDEX;

}
