package pl.gda.pg.mif.aqualung.consts;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public enum DocumentTypes {

    AGREEMENT,
    PRACTICE_CARD,
    CONTRACT,
    BHP,
    TIME_SHEDULE,
    OTHER
}
