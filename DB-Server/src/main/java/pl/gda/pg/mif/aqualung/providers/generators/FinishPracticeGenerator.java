package pl.gda.pg.mif.aqualung.providers.generators;

import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Config;
import pl.gda.pg.mif.aqualung.dao.queries.Queries;
import pl.gda.pg.mif.aqualung.dao.struct.PracticePart;
import pl.gda.pg.mif.aqualung.helpers.ConfigHelper;
import pl.gda.pg.mif.aqualung.providers.DictionaryProvider;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology kongo && Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class FinishPracticeGenerator {

    private static final String BASIC_CARD = "PracticesCard.pdf";
    private static final String BLANK_DESCRIPTION_CARD = "PracticeCardClear.pdf";
    private static final String OUTPUT_PREFIX = ConfigHelper.getConfig().getJsonObject(Config.FILE_CONFIG).getString(Config.SERVER_CONFIG_PDF_IN);
    private static final int gapLeft = 0xa; // 10
    private static final int gapDown = 0x1; // 01

    private static final String COURSE = "7";

    private static final int FONT_SIZE = 10; // 01
    private static BaseFont baseFont;

    private static final int WEEK_OFFSET = 78;
    private static final int WEEK_HEIGHT = 22;
    private static final int DESCRIPTION_OFFSET = 73;
    private static final int DESCRIPTION_HEIGHT = 16;
    private static final int SKILL_OFFSET = 103;
    private static final int SKILL_HEIGHT = 18;

    private static final int DESCRIPTION_PAGE = 2;

    private static final int RIGHT_MARGIN = 60;
    private static final int BOTTOM_MARGIN = 100;

    private static final Logger log = LogManager.getLogger(FinishPracticeGenerator.class);

    public static String generate(
            String studentName,
            String PG_Index,
            String speciality,
            String employerName,
            String employerAddress,
            String practiceStart,
            String practiceFinish,
            String BHPCity,
            String BHPDate,
            String BHPEmployee,
            JsonArray practiceDescriptions
    ) {
        try {
            if (baseFont == null) {
                baseFont = BaseFont.createFont("GraviolaSoft-Medium.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            }
            PdfReader basicReader = new PdfReader(BASIC_CARD);
            
            String cardName = "studentPractice/" + studentName + "_finishCard_" + UUID.randomUUID().toString() + ".pdf";

            String output = OUTPUT_PREFIX + cardName;

            PdfStamper pdfStamper = new PdfStamper(basicReader, new FileOutputStream(output));

            PdfContentByte firstPage = pdfStamper.getUnderContent(1);
            firstPage.beginText();
            firstPage.setFontAndSize(baseFont, FONT_SIZE);

            generateFirstPage(firstPage, studentName, PG_Index, speciality, employerName, employerAddress, practiceStart, practiceFinish, BHPCity, BHPDate, BHPEmployee);
            generateSecondPage(basicReader, pdfStamper, practiceDescriptions);

            pdfStamper.close();

            return "pdf/" + cardName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void generateFirstPage(
            PdfContentByte firstPage,
            String studentName,
            String PG_Index,
            String speciality,
            String employerName,
            //            String employerPhone,
            String employerAddress,
            String practiceStart,
            String practiceFinish,
            String BHPCity,
            String BHPDate,
            String BHPEmployee
    ) {
        // Student
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, studentName, 145 + gapLeft, 585 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, PG_Index, 122 + gapLeft, 556 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, COURSE, 427 + gapLeft, 556 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, DictionaryProvider.getTranslation("facultities." + speciality), 176 + gapLeft, 526 + gapDown, 0);
        // Pracodawca
        String companyFirstRow = employerName;
//        if (employerPhone != null) {
//            companyFirstRow += "   tel. " + employerPhone;
//        }
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, companyFirstRow, 71 + gapLeft, 460 + gapDown, 0);
        if (employerAddress != null) {
            employerAddress.replaceAll("\\n", "   ");
            firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, employerAddress, 71 + gapLeft, 430 + gapDown, 0);
        }
        // poczatek praktyk
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, practiceStart, 155 + gapLeft, 389 + gapDown, 0);

        if (practiceFinish != null) {
            firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, practiceFinish, 145 + gapLeft, 261 + gapDown, 0);
        }
        // BHP
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, BHPCity, 71 + gapLeft, 331 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, BHPDate, 177 + gapLeft, 331 + gapDown, 0);
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, BHPEmployee, 265 + gapLeft, 331 + gapDown, 0);

        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, ConfigHelper.getPortalCity(), 71 + gapLeft, 97 + gapDown, 0);
        // Generate today date  YYY-MM-DD
        firstPage.showTextAligned(PdfContentByte.ALIGN_LEFT, Queries.dayNow(), 287 + gapLeft, 97 + gapDown, 0);
        firstPage.endText();
    }

    private static void generateSecondPage(
            PdfReader basicReader,
            PdfStamper pdfStamper,
            JsonArray practiceDescriptions
    ) throws IOException {

        float width = basicReader.getPageSizeWithRotation(2).getWidth();
        int actualPage = DESCRIPTION_PAGE;
        int actualHeight = 733;
        int bottomMargin = 233;
        float actualWidth = width - RIGHT_MARGIN;

        PdfReader blankReader = new PdfReader(BLANK_DESCRIPTION_CARD);

        PdfContentByte page = pdfStamper.getOverContent(actualPage);
        page.beginText();
        page.setFontAndSize(baseFont, FONT_SIZE);

        page.showTextAligned(PdfContentByte.ALIGN_LEFT, ConfigHelper.getPortalCity(), 71 + gapLeft, 233 + gapDown, 0);
        page.showTextAligned(PdfContentByte.ALIGN_LEFT, Queries.dayNow(), 160 + gapLeft, 233 + gapDown, 0);
        page.showTextAligned(PdfContentByte.ALIGN_LEFT, Queries.dayNow(), 108 + gapLeft, 108 + gapDown, 0);

        for (int i = 0, n = practiceDescriptions.size(); i < n; i++) {
            JsonObject descriptionObject = practiceDescriptions.getJsonObject(i);
            List<String> descriptionRows = generateTextRows(page, descriptionObject.getString(PracticePart.DESCRIPTION), 71, actualWidth);
            List<List<String>> skillRows = new ArrayList<>();

            JsonArray skills = descriptionObject.getJsonArray(PracticePart.SKILLS);
            Iterator<Object> iterator = skills.iterator();
            while (iterator.hasNext()) {
                String skill = (String) iterator.next();
                skillRows.add(generateTextRows(page, skill, SKILL_OFFSET, actualWidth));
            }
            int sectionHeight = countDescriptionSectionHeight(page, descriptionRows, skillRows);

            if (actualHeight < bottomMargin + sectionHeight) {
                page.endText();

                pdfStamper.insertPage(++actualPage, basicReader.getPageSizeWithRotation(2));
                pdfStamper.replacePage(blankReader, 1, actualPage);
                page = pdfStamper.getOverContent(actualPage);
                page.beginText();
                page.setFontAndSize(baseFont, FONT_SIZE);
                actualHeight = 733;

                page.showTextAligned(PdfContentByte.ALIGN_RIGHT, "" + actualPage, width - RIGHT_MARGIN, 40, 0);

                bottomMargin = BOTTOM_MARGIN;
            }
            actualHeight = writeDescriptionSection(page, descriptionRows, skillRows, actualHeight, descriptionObject.getInteger(PracticePart.STAGE));
            log.debug(" PRAWIDŁOWO DODAŁEM SEKCJE : " + i);
        }
        page.endText();
    }

    private static int countDescriptionSectionHeight(PdfContentByte page, List<String> description, List<List<String>> skills) {
        int expectedHeight = 0;
        // Week row
        expectedHeight += WEEK_HEIGHT;
        expectedHeight += description.size() * DESCRIPTION_HEIGHT;
        Iterator<List<String>> iterator = skills.iterator();
        while (iterator.hasNext()) {
            List<String> next = iterator.next();
            expectedHeight += next.size() * SKILL_HEIGHT;
        }
        return expectedHeight;
    }

    private static int writeDescriptionSection(
            PdfContentByte page,
            List<String> descriptionRows,
            List<List<String>> skillRows,
            int actualHeight,
            int week) {
        page.showTextAligned(PdfContentByte.ALIGN_LEFT, "Etap " + week, WEEK_OFFSET, actualHeight, 0);
        actualHeight -= WEEK_HEIGHT;

        Iterator<String> iterator = descriptionRows.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            page.showTextAligned(PdfContentByte.ALIGN_LEFT, next, DESCRIPTION_OFFSET + gapLeft, actualHeight, 0);
            actualHeight -= DESCRIPTION_HEIGHT;
        }

        Iterator<List<String>> iterator1 = skillRows.iterator();
        while (iterator1.hasNext()) {
            List<String> next = iterator1.next();
            Iterator<String> iterator2 = next.iterator();
            while (iterator2.hasNext()) {
                String next1 = iterator2.next();
                page.showTextAligned(PdfContentByte.ALIGN_LEFT, "- " + next1, SKILL_OFFSET + gapLeft, actualHeight, 0);
                actualHeight -= SKILL_HEIGHT;
            }
        }
        return (int) actualHeight;
    }

    private static List<String> generateTextRows(PdfContentByte page, String text, int offset, float width) {
        List<String> rows = new ArrayList<>();
        float effectiveWidth = width - offset;
        if (page.getEffectiveStringWidth(text, false) < effectiveWidth) {
            rows.add(text);
        } else {
            String[] words = text.split(" ");
            StringBuilder row = new StringBuilder();

            float spaceWidth = page.getEffectiveStringWidth(" ", false);
            float rowWidth = 0;
            for (int index = 1, n = words.length; index < n; index++) {
                String nextWord = words[0];

                float nextWordWidth = page.getEffectiveStringWidth(nextWord, false);

                if (rowWidth + nextWordWidth < effectiveWidth) {
                    row.append(nextWord).append(" ");
                    rowWidth += nextWordWidth + spaceWidth;
                } else {
                    rows.add(row.toString());
                    row.delete(0, row.length());
                    rowWidth = 0;
                }
            }
            rows.add(row.toString());
        }
        return rows;
    }
}
