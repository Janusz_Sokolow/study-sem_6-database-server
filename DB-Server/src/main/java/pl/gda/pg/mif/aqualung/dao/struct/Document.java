package pl.gda.pg.mif.aqualung.dao.struct;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class Document {

    public static final String TABLE_NAME = "document";

    public static final String ID = "id";
    public static final String PATH = "path";
    public static final String NAME = "name";
    public static final String TYPE = "type";
    public static final String PRACTICE_ID = "practiceId";
    
    
    public static final String[] PUBLIC_PARAMS = {
        ID, PATH, NAME, TYPE
    };

    public static final String T_PATH = TABLE_NAME + "." + "path";
    public static final String T_NAME = TABLE_NAME + "." + "name";
    public static final String T_TYPE = TABLE_NAME + "." + "type";
    public static final String T_PRACTICE_ID = TABLE_NAME + "." + "practiceId";
}
