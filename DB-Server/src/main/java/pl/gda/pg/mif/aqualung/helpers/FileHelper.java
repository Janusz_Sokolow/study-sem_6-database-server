package pl.gda.pg.mif.aqualung.helpers;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Random;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.gda.pg.mif.aqualung.consts.Config;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology Y50
 */
public class FileHelper {

    private static final Logger log = LogManager.getLogger(FileHelper.class);
    private static final String OUTPUT_PREFIX = ConfigHelper.getConfig().getJsonObject(Config.FILE_CONFIG).getString(Config.SERVER_CONFIG_PDF_IN);
    private static final Random generator = new Random();

    public static byte[] getFileWithUtil(String fileName) {
        FileHelper fileHelper = new FileHelper();
        byte[] result = null;
        ClassLoader classLoader = fileHelper.getClass().getClassLoader();
        try {
            InputStream is = classLoader.getResourceAsStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            result = IOUtils.toByteArray(br, "UTF-8");
        } catch (IOException ex) {
            log.warn("Błąd wczytywania pliku - IOException! ", ex);
        } catch (Exception ex) {
            log.warn("Błąd wczytania pliku - Exception! ", ex);
        }
        return result;
    }

    public static String saveImage(byte[] data, String name) {
        log.debug("Zapisuję obraz o nazwie: " + name);
        try {
            // convert byte array to BufferedImage
            InputStream in = new ByteArrayInputStream(data);
            BufferedImage bImageFromConvert = ImageIO.read(in);

            File fotoFile = new File(OUTPUT_PREFIX + name);
            fotoFile.createNewFile();
            Iterator<ImageWriter> iterator
                    = ImageIO.getImageWritersByFormatName("jpg");
            ImageWriter imageWriter = iterator.next();
            ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();
            imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            imageWriteParam.setCompressionQuality(0.9F);

            ImageOutputStream imageOutputStream
                    = new MemoryCacheImageOutputStream(new FileOutputStream(fotoFile));
            imageWriter.setOutput(imageOutputStream);
            IIOImage iioimage = new IIOImage(bImageFromConvert, null, null);
            imageWriter.write(null, iioimage, imageWriteParam);
            imageOutputStream.flush();

            log.debug("Zapis byte'owego obrazu zakończony powodzeniem");
            return "pdf/" + name;
        } catch (IOException ex) {
            log.warn("Błąd zapisu obrazu - IOException! ", ex);
        } catch (Exception ex) {
            log.warn("Błąd zapisu obrazu - Exception! ", ex);
        }
        return null;

    }

    public static String savePdf(byte[] data, String name) {
        log.debug("Zapisuję pdf o nazwie: " + name);
        try {
            PdfReader basicReader = new PdfReader(data);
            // convert byte array to BufferedImage

            String filePath = OUTPUT_PREFIX + name;
            filePath = filePath.replace(' ', '_');
            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            PdfStamper pdfStamper = new PdfStamper(basicReader, fileOutputStream);
            pdfStamper.close();
            log.debug("Zapis byte'owego pdfa zakończony powodzeniem");
            return "pdf/" + name;
        } catch (IOException ex) {
            log.warn("Błąd zapisu pdfa - IOException! ", ex);
        } catch (Exception ex) {
            log.warn("Błąd zapisu pdfa - Exception! ", ex);
        }
        return null;

    }
}
