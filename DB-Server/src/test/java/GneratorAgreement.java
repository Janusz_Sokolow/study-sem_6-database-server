import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.gda.pg.mif.aqualung.providers.generators.PracticeAgreementGenerator;

/**
 *
 * @author Janusz Sokołow, Student of Gdańsk University of Technology
 */
public class GneratorAgreement {
    
    public GneratorAgreement() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     @Test
     public void testGenerator() {
        String filePath = PracticeAgreementGenerator.generate(
                "Janusz", 
                "Sokołow",
                "150238", 
                "Comarch", 
                "1.1", 
                "1/2",
                "ORDER");
         
         Assert.assertNotNull(filePath);
     }
}
