App.config(function ($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /apps
    $urlRouterProvider.otherwise("/app/main");
    //
    // Now set up the states
    $stateProvider
            .state('app', {
                url: "/app",
                abstract: true,
                controller: 'AppController',
                templateUrl: "components/app.html"
            })
            .state('app.student', {
                url: "/student",
                templateUrl: "components/student/student-view.html",
                controller: "StudentController"
            })
            .state('app.student.create', {
                url: "/create/:nickName",
                templateUrl: "components/student/create/create-view.html",
                controller: "CreateStudentController"
            })
            .state('app.student.list', {
                url: "/list",
                templateUrl: "components/student/list/list-view.html",
                controller: "ListStudentController"
            })
            .state('app.student.startPractice', {
                url: "/start/:nickName",
                templateUrl: "components/practice/start/start-view.html",
                controller: "StartPracticeController"
            })

            .state('app.company', {
                url: "/company",
                templateUrl: "components/company/company-view.html",
                controller: "CompanyController"
            })
            .state('app.company.create', {
                url: "/create",
                templateUrl: "components/company/create/create-view.html",
                controller: "CreateCompanyController"
            })
            .state('app.company.list', {
                url: "/list",
                templateUrl: "components/company/list/list-view.html",
                controller: "ListCompanyController"
            })
            .state('app.company.stats', {
                url: "/stats",
                templateUrl: "components/company/stats/stats-view.html",
                controller: "StatsCompanyController"
            })

            .state('app.practice', {
                url: "/practice",
                templateUrl: "components/practice/practice-view.html",
                controller: "PracticeController"
            })
            .state('app.practice.list', {
                url: "/list",
                templateUrl: "components/practice/list/list-view.html",
                controller: "ListPracticeController"
            })
            .state('app.practice.describe', {
                url: "/list/:practiceId",
                templateUrl: "components/practice/describe/describe-view.html",
                controller: "DescribePracticeController"

            })
            .state('app.practice.show', {
                url: "/show/:practiceId",
                templateUrl: "components/practice/show/show-view.html",
                controller: "ShowPracticeController"
            });
});