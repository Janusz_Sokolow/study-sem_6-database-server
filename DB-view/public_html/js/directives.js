App
        .directive('navbar', function () {
            return {
                restrict: 'EA',
                replace: true,
                templateUrl: 'shared/schema/navbar.html',
                controller: ['$scope', '$state', '$http', 'APP_PARAMS', 'UserProfile',
                    function ($scope, $state, $http, APP_PARAMS, UserProfile) {

                        UserProfile.then(function (re) {
                            console.log("TEST === UserProfile ", re);
                            $scope.UserName = re.username;
                        });
                        $scope.logout = function () {
                            $http({
                                method: 'GET',
                                url: '/api/logout'
                            }).then(function (response) {
                                console.log(" good ", response);
                                $state.go('app.main', {}, {reload: true}).then(function () {
                                    location.reload();
                                });
                            }, function (response) {
                                console.log(" bed ", response);
                                alert("Sorry, you can't be signed out!\n\We got any error;")
                            });
                        }
                    }]
            }
        })
        .directive('datePicker', function () {
            return {
                templateUrl: 'shared/datepicker/datepicker.html',
                controller: 'DatepickerCtrl',
                scope: {
                    date: "=",
                    minDate: "=",
                    maxDate: "=",
                    placeholder: "@"
                }
            };
        })

        .directive('loginForm', function () {
            return {
                templateUrl: 'shared/login-form.html'
            };
        })
        .directive('fileLoader', function () {
            return {
                templateUrl: 'shared/file-upload/file-upload.html',
                controller: 'FileUploadController',
                scope: {
                    files: "=",
                    broadcast: "=",
                    maxAmount: "@"
                }
            };
        });