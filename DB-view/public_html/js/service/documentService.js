App.service('documentService', ['eventbusMessageService', '$q', function (eventbusMessageService, $q) {
        return {
            removeDocument: removeDocument
        }

        function removeDocument(id) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.document.remove",
                data: {
                    id: id
                }
            }).getValue('ok')
        }
    }]);