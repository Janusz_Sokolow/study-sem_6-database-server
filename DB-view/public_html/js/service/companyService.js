App.service('companyService', ['eventbusMessageService', '$q', function (eventbusMessageService, $q) {
        return {
            createCompany: createCompany,
            checkCompanyExists: checkCompanyExists,
            findCompanies: findCompanies,
            findCompanyAbleToApply: findCompanies,
            findCompanyStats: findCompanyStats
        };

        function checkCompanyExists(companyName) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.company.existsName",
                data: {companyName: companyName}
            }).getValue('exists');
        }

        function createCompany(newCompany) {
            return  eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.company.create",
                data: {
                    companyName: newCompany.companyName,
//                    phoneNumber: newCompany.phoneNumber,
                    address: newCompany.address
                }
            }).getValue('ok');
        }

        function findCompanies() {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.company.find",
                data: {}
            }).getValue('companiesList')
        }

        function findCompanyStats(companyId) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.company.getStats",
                data: {
                    companyId: companyId
                }
            }).getValue('companiesList');
        }
    }]);