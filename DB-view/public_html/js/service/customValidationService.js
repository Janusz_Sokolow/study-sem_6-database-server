
App.service("customValidationService", ['$q', '$translate', 'studentService', 'companyService', function ($q, $translate, studentService, companyService) {
        return {
            checkUsernameExistsValidation: checkUsernameExistsValidation,
            checkPGIndexExistsValidation: checkPGIndexExistsValidation,
            checkCompanyExistsValidation: checkCompanyExistsValidation
        }

        function checkUsernameExistsValidation(username) {
            console.log("Turn on checkUserNameExistsValidtion ->", username);
            var deferred = $q.defer();

            studentService.checkStudentExists(username).
                    then(function (isExists) {
                        console.log("validation -> ", isExists);
                        deferred.resolve({isValid: !isExists, message: $translate.instant("validation_error.custom.USERNAME_EXISTS")});
                    }, function (e) {
                        deferred.resolve({isValid: false, message: 'Returned error from custom function.'});
                    });
            return deferred.promise;
        }
        function checkPGIndexExistsValidation(pgIndex) {
            console.log("Turn on checkPGIndexExistsValidation ->", pgIndex);
            var deferred = $q.defer();

            studentService.checkPGIndexEsists(pgIndex).
                    then(function (isExists) {
                        console.log("validation -> ", isExists);
                        deferred.resolve({isValid: !isExists, message: $translate.instant("validation_error.custom.PG_INDEX_EXISTS")});
                    }, function (e) {
                        deferred.resolve({isValid: false, message: 'Returned error from custom function.'});
                    });
            return deferred.promise;
        }
        function checkCompanyExistsValidation(companyName) {
            console.log("Turn on checkCompanyExistsValidation ->", companyName);
            var deferred = $q.defer();

            companyService.checkCompanyExists(companyName).then(function (isExists) {
                console.log("validation -> ", isExists);
                deferred.resolve({isValid: !isExists, message: $translate.instant("validation_error.custom.COMPANY_EXISTS")});
            }, function (e) {
                deferred.resolve({isValid: false, message: 'Returned error from custom function.'});
            });
            return deferred.promise;
        }
    }]);