App.service('studentService', ['eventbusMessageService', '$q', function (eventbusMessageService, $q) {
        return {
            createStudent: createStudent,
            updateStudent: updateStudent,
            getStudent: getStudent,
            checkStudentExists: checkStudentExists,
            checkPGIndexEsists: checkPGIndexEsists,
//            updateStudent: updateStudent,
            findStudent: findStudent
        }

        function createStudent(newStudent) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.student.create",
                data: {
                    nickname: newStudent.nickname,
                    password: newStudent.password,
                    PGIndex: newStudent.PGIndex,
                    firstName: newStudent.firstName,
                    lastName: newStudent.lastName,
                    mail: newStudent.mail,
                    speciality: newStudent.speciality
                }
            }).getValue('ok');
        }

//        function updateStudent(student) {
//            return eventbusMessageService.send({
//                address: "pl.gda.pg.mif.practice.student.update",
//                data: {
//                    PGIndex: student.PGIndex,
//                    firstName: student.firstName,
//                    lastName: student.lastName,
//                    mail: student.mail,
//                    speciality: student.speciality
//                }
//            }).getValue('ok');
//        }

        function checkStudentExists(nickname) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.student.existsNickname",
                data: {nickName: nickname}
            }).getValue('exists');
        }
        function checkPGIndexEsists(pgIndex) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.student.existsPGIndex",
                data: {PGIndex: pgIndex}
            }).getValue('exists');
        }

        function getStudent(nickname) {
            return eventbusMessageService.send({
                address: "pl.gda.pg.mif.practice.student.get",
                data: {
                    nickName: nickname
                }
            }).getValue('student');
        }
        function updateStudent(oldStudent) {
            var data = {
                PGIndex: oldStudent.PGIndex,
                firstName: oldStudent.firstName,
                lastName: oldStudent.lastName,
                mail: oldStudent.mail,
                speciality: oldStudent.speciality
            };
            var params = {
                address: "pl.gda.pg.mif.practice.student.update",
                data: data
            }
            eventbusMessageService.send(params).then(function (data) {
                console.log("SUCCESS - Prawidłowo zedytowano studenta", data);
            }, function (reply) {
                console.warn("ERROR - błąd edycji studenta ", reply)
            });
        }

        function findStudent() {
            var deferred = $q.defer();
            var params = {
                address: "pl.gda.pg.mif.practice.student.find",
                data: {}
            }
            eventbusMessageService.send(params).then(function (data) {
                console.log("SUCCESS - Prawidłowo pobrano listę studentów", data);
                deferred.resolve(data.studentsList);
            }, function (reply) {
                console.warn("ERROR - błąd pobierania listy studentów ", reply)
            });
            return deferred.promise;

        }
    }])