
App.service("eventbusMessageService", ['vertxEventBusService', '$q', function (vertxEventBusService, $q) {
        return {
            send: send
        }

        function send(message, outerDeffered) {
            var deferred = $q.defer();
            if (!message.data) {
                message.data = {};
            }
            vertxEventBusService.send(message.address, message.data)
                    .then(function (reply) {
                        console.log("Mam odpowiedź na ", message, " :: ", reply);
                        if (reply === undefined) {
                            reject(deferred, outerDeffered, "Timeout !");
                        } else if (reply.ok === true) {
                            deferred.resolve(reply);
                        } else {
                            alert("Napotkano błąd : " + reply.error + "\n Działanie aplikacji może być niezdefiniowane")
                            reject(deferred, outerDeffered);
                        }
                    }, function (err) {
                        console.error("Błąd obsługi wiadomości : ", message, err);
                        reject(deferred, outerDeffered, err);
                    })
                    .catch(function (err) {
                        reject(deferred, outerDeffered, {message: message, err: err});
                    });
            return deferred.promise;
        }

        function reject(def1, def2, err) {
            if (def1)
                def1.reject(err);
            if (def2)
                def2.reject(err);

        }
    }]);