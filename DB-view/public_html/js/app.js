var App =
        angular.module('PracticePortal', ['ui.router', 'ui.bootstrap', 'knalli.angular-vertxbus', 'ghiscoding.validation', 'ngResource', 'ui.bootstrap.modal', 'pascalprecht.translate', 'ngSanitize', 'textAngular', 'frapontillo.bootstrap-switch', 'isteven-multi-select', 'ngFileUpload'])
        .run(['$rootScope', '$uibModal', '$interval', '$timeout', '$state', '$http', 'UserProfile',
            function ($rootScope, $uibModal, $interval, $timeout, $state, $http, UserProfile) {
                $rootScope.openLoginDialog = openLoginDialog;
                $rootScope.openOpinionDialog = openOpinionDialog;

                $rootScope.listeners = {};
                UserProfile.then(function (loggedProfile) {
                    console.log("TEST === ", loggedProfile);
                    $rootScope.user = loggedProfile;
                });

                function openOpinionDialog() {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'shared/opinionModal.html',
                        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                $scope.cancel = cancel;
                                function cancel() {
                                    console.log("CANCEL");
                                    $uibModalInstance.dismiss('cancel');
                                }
                            }]
                    }).result.then(function () {
                        console.log('Modal done');
                    }, function () {
                        console.log('Modal dismissed at: ' + new Date());
                    });
                }

                function openLoginDialog() {
                    $uibModal.open({
                        animation: true,
                        templateUrl: 'shared/loginModal.html',
                        controller: ['$scope', '$translate', '$uibModalInstance', function ($scope, $translate, $uibModalInstance) {
                                $scope.cancel = cancel;
//                                $scope.submit = submit;
                                function cancel() {
                                    console.log("CANCEL");
                                    $uibModalInstance.dismiss('cancel');
                                }
                                $timeout(function () {
                                    console.log("TEST == ", $('#loginForm'));
                                    $('#login').focus();
                                    $('#loginForm').submit(function (event) {
                                        console.log("TEST == ", event)
                                        event.preventDefault();
                                        $.ajax({
                                            url: $('#loginForm').attr('action'),
                                            type: $('#loginForm').attr('method'),
                                            data: $('#loginForm').serialize(),
                                            success: function (res) {
                                                $state.go('app.main', {}, {reload: true}).then(function () {
                                                    location.reload();
                                                });
                                            },
                                            error: function (res) {
                                                if (res.status === 302) {
                                                    $translate('validation_error.custom.WRONG_LOGIN_DATA').then(function (error) {
                                                        alert(error)
                                                    })
                                                } else if (res.status === 429 ) {
                                                    $translate('validation_error.custom.TOO_MANY_LOGIN_TRIES').then(function (error) {
                                                        alert(error)
                                                    })
                                                } else if (res.status === 500) {
                                                    $translate('validation_error.custom.INTERNAL_ERROR').then(function (error) {
                                                        alert(error)
                                                    });
                                                }
                                                console.log('form submitted error.', res);
                                            }
                                        });
                                    });
                                    return false;
                                }, 500);
//                                    console.log("submit");
//                                    $http.post('/api/auth').then(function (response) {
//                                        console.log("login SUCCESS", response)
//                                    }, function (response) {
//                                        console.log("login ERROR", response)
//                                    });
//                                }
                            }]
                    }).result.then(function () {
                        console.log('Modal done');
                    }, function () {
                        console.log('Modal dismissed at: ' + new Date());
                    });
                }


                $interval(function () {
                    console.log("Send ping");
                    $http({
                        method: 'GET',
                        url: '/ping'
                    }).then(function (response) {
                        console.log("PING SUCCESS", response)
                    }, function (response) {
                        console.log("PING ERROR", response)
                    });
                }, 1000 * 60 * 5)
            }]);
