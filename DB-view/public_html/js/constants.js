App
        .constant('SERVER_CONFIG', {
            starsAmount : 5
        })
        .constant('PRACTICE_TYPES', [
            "CONTRACT_PG",
            "CONTRACT_OF_EMPLOYMENT",
            "CONTRACT_WORK",
            "CONTRACT_COMMISSION"
        ])
        .constant('PRACTICE_STATUSES', [
            "NEW", "APPROVED",
            "REJECTED", "DURING",
            "FINISHED", "ARCHIVE"
        ])
        .constant('FILE_TYPES', [
            "CONTRACT", "BHP",
            "TIME_SHEDULE", "OTHER"
        ])
        .constant('APP_PARAMS', {
            TEST_MODE: true
        })
        .constant('FACULTIETIES', {
            actual: ["TECHNICAL_PHYSICS", "APPLIED_MATHEMATICS", "CONVERTION_OF_ENERGY", "APPLIED_INFORMATHICS"],
            all: ["TECHNICAL_PHYSICS", "APPLIED_MATHEMATICS", "CONVERTION_OF_ENERGY", "APPLIED_INFORMATHICS"]
        })
        .constant('MOCKED_FAQS', {
            'SERVER_SITE': [{
                    title: "Gdzie mogę znaleźć regulamin zaliczenia praktyki",
                    isBinding: true,
                    content: "<a href='pdf/praktyki-zasady-pg.pdf' source='_blank'>Tu</a> <span style='color:red; padding-left:200px'>rees </span>"
                }, {
                    title: "Gdzie mogę znaleźć więcej informacji na temat praktyki praktyki",
                    isBinding: true,
                    content: "<a href='pdf/Praktyki-prezentacja.pdf' source='_blank'>Tu</a> <span style='color:red; padding-left:200px'>rees </span>"
                }, {
                    title: "Czy istnieje wzór umowy między Politechniką a Pracodawcą",
                    isBinding: true,
                    content: "Tak, możesz go znaleźć pod tym <a href='pdf/Wzor.pdf' source='_blank'> linkiem </a>"
                }, {
                    title: "Kiedy aplikacja będzie gotowa?",
                    content: "Premera podstawowej wersji jest planowana na koniec września. Wtedy dostanie rozwinięta szata graficzna, do aplikacji zotaną dopuszczeni testerzy, pierwsze praktyki będą mogły zostać zgłoszone."
                }
            ],
            'PRACTICE_SITE': [
                {
                    title: "Czy mogę zaliczać praktykę w grudniu?",
                    content: "Tak, lecz nie zaliczysz studiów i nie bedziesz mógł zacząć kolejnegostopnia"
                }, {
                    title: "Gdzie mogę znaleźć koordynatora mojej praktyki?",
                    content: "Koordynatorem praktyk dla specjalizacji informatyka stosowana jest Pani Marta Łabuda."
                }
            ]
        })
        .constant('QUESTIONNAIRE', [
            {
                type: "STARS",
                id: "opinionStart"
            }, {
                type: "DESCRIPTION",
                id: "opinion"
            }, {
                type: "BOOLEAN",
                id: "TEAM_FRIENDSHIP"
            }, {
                type: "STARS",
                id: "TEAM_ATMOSPHERE"
            }, {
                type: "DESCRIPTION",
                id: "WORK_CONDITIONS"
            }, {
                type: "BOOLEAN",
                id: "RECOMENDATION"
            }

        ])

        .constant("LESSONS", [{
                index: "FIZ1B001",
                name: "Analiza matematyczna I"}, {
                index: "FIZ1B002",
                name: "Mechanika i ciepło"}, {
                index: "FIZ1B003",
                name: "Planowanie i analiza eksperymentu"}, {
                index: "FIZ1B004",
                name: "Wstęp do informatyki"}, {
                index: "FIZ1B005",
                name: "Zajęcia wyrównawcze z matematyki"}, {
                index: "FIZ1D001",
                name: "Historia fizyki i techniki"}, {
                index: "FIZ1B006",
                name: "Algebra liniowa z geometrią"}, {
                index: "FIZ1B007",
                name: "Analiza matematyczna II"}, {
                index: "FIZ1B008",
                name: "Elektryczność i magnetyzm"}, {
                index: "FIZ1B009",
                name: "Pracownia fizyczna I (mechanika i ciepło)"}, {
                index: "FIZ1C300",
                name: "Metody matematyczne informatyki"}, {
                index: "FIZ1C301",
                name: "Proceduralne języki programowania"}, {
                index: "FIZ1D002",
                name: "Etyka nauki i techniki"}, {
                index: "FIZ1A001",
                name: "Język angielski I"}, {
                index: "FIZ1A006",
                name: "Wychowanie fizyczne I"}, {
                index: "FIZ1C302",
                name: "Algorytmy i struktury danych"}, {
                index: "FIZ1C303",
                name: "Fale i optyka"}, {
                index: "FIZ1C304",
                name: "Kryptografia"}, {
                index: "FIZ1C305",
                name: "Obiektowe języki programowania I"}, {
                index: "FIZ1C306",
                name: "Oblizenia symboliczne"}, {
                index: "FIZ1C307",
                name: "Proceduralne języki programowania II"}, {
                index: "FIZ1A102",
                name: "Język angielski II"}, {
                index: "FIZ1A007",
                name: "Wychowanie fizyczne II"}, {
                index: "FIZ1C110",
                name: "Metody matematyczne fizyki I"}, {
                index: "FIZ1C114",
                name: "Wstęp do elektroniki i elektrotechniki"}, {
                index: "FIZ1C308",
                name: "Obiektowe języki programowania II"}, {
                index: "FIZ1C309",
                name: "Podstawy metod numerycznych"}, {
                index: "FIZ1C310",
                name: "Wykład specjalistyczny I (IS)"}, {
                index: "FIZ1C311",
                name: "Wykład specjalistyczny II (IS)"}, {
                index: "FIZ1E001",
                name: "Podstawy ekonomii i zarządzania"}, {
                index: "FIZ1A003",
                name: "Język angielski III"}, {
                index: "FIZ1C122",
                name: "Układy elektroniczne"}, {
                index: "FIZ1C312",
                name: "Algorytmy rzproszone I"}, {
                index: "FIZ1C313",
                name: "Obiektowe języki programowania III"}, {
                index: "FIZ1C314",
                name: "Podstawy fizyki technicznej"}, {
                index: "FIZ1C315",
                name: "Programowanie współbieżne i równoległe"}, {
                index: "FIZ1C316",
                name: "Sieci teleinformatyczne"}, {
                index: "FIZ1E002",
                name: "Prawo gospodarcze"}, {
                index: "FIZ1A004",
                name: "Język angielski fizyki, informatyki i techniki I"}, {
                index: "FIZ1C126",
                name: "Komputerowe wspomaganie projektowania"}, {
                index: "FIZ1C317",
                name: "Algorytmy rozproszone !!"}, {
                index: "FIZ1C318",
                name: "Architektura i administracja systemów operacyjnych"}, {
                index: "FIZ1C319",
                name: "Inżynieria oprogramowania"}, {
                index: "FIZ1C320",
                name: "Projektowanie baz danych"}, {
                index: "FIZ1C321",
                name: "Wstęp do programowania niskiego poziomu"}, {
                index: "FIZ1C322",
                name: "Wykład specjalistyczny III (IS)"}, {
                index: "FIZ1D003",
                name: "Filozofia przyrody"}, {
                index: "FIZ1A005",
                name: "Język angielski fizyki, informatyki i techniki II"}, {
                index: "FIZ1B010",
                name: "Praktyka zawodowa"}, {
                index: "FIZ1C133",
                name: "Wykład obieralny"}, {
                index: "FIZ1C323",
                name: "Oprogramowanie aplikacyjne/projekt grupowy"}, {
                index: "FIZ1C324",
                name: "Technologie tworzenia stron internetowych"}, {
                index: "FIZ1C325",
                name: "Seminarium dyplomowe (IS)"}, {
                index: "FIZ1C326",
                name: "Projekt dyplomowy (IS)"}, {
                index: "FIZ1B011",
                name: "Przygotowanie do egzaminu dyplomowego"
            }]);