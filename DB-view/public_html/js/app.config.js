App
        .config(['vertxEventBusProvider', function (vertxEventBusProvider) {
                vertxEventBusProvider
                        .enable()
                        .useDebug(true)
                        .useReconnect()
                        .useUrlServer(window.location.protocol + '//' + window.location.host);
            }])
        .config(function ($translateProvider) {
            $translateProvider.useStaticFilesLoader({
                files: [{
                        prefix: 'assets/i18n/validation/',
                        suffix: '.json'
                    }, {
                        prefix: 'assets/i18n/practice/',
                        suffix: '.json'
                    }, {
                        prefix: 'assets/i18n/system/',
                        suffix: '.json'
                    }]
            });
            // define translation maps you want to use on startup
            $translateProvider.preferredLanguage('pl').useSanitizeValueStrategy('escape');
            ;
        });