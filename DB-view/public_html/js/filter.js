App
        .filter('highlight', function ($sce) {
            return function (text, phrase) {
//                console.log("highlight -> ", phrase);
                if (phrase)
                    text = text.replace(new RegExp('(' + phrase + ')', 'gi'),
                            '<span class="highlighted">$1</span>')
                return $sce.trustAsHtml(text)
            }
        })
        .filter('capitalizeFirst', function () {
            return function (input) {
                return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
            }
        })
        .filter('propsFilter', function () {
            return function (items, props) {
                var out = [];

                if (angular.isArray(items)) {
                    items.forEach(function (item) {
                        var itemMatches = false;

                        var keys = Object.keys(props);
                        for (var i = 0; i < keys.length; i++) {
                            var prop = keys[i];
                            var text = props[prop].toLowerCase();
                            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                                itemMatches = true;
                                break;
                            }
                        }

                        if (itemMatches) {
                            out.push(item);
                        }
                    });
                } else {
                    // Let the output be the input untouched
                    out = items;
                }

                return out;
            }
        })
        .filter('dateFilter', function () {
            return function (items, props) {
                var out = [];

                if (angular.isArray(items)) {
                    items.forEach(function (item) {
                        var itemMatches = false;
                        var dateFrom = props.dateFrom;
                        var dateTo = props.dateTo;
                        var itemFrom = item[props.propFrom];
                        var itemTo = item[props.propTo];
                        if (!dateFrom && !dateTo) {
                            itemMatches = true;
                        } else if (!dateFrom) {
                            itemMatches = itemFrom < dateTo;
                        } else if (!dateTo) {
                            itemMatches = itemTo > dateFrom;
                        } else {
                            itemMatches = itemFrom < dateTo || itemTo > dateFrom;
                        }
                        if (itemMatches) {
                            out.push(item);
                        }
                    });
                } else {
                    // Let the output be the input untouched
                    out = items;
                }

                return out;
            }
        });