App
        .directive('faqs', function () {
            return {
                templateUrl: 'shared/FAQ/FAQ-view.html',
                controller: 'FAQController',
                scope: {
                }
            }
        })
        .controller('FAQController', FAQController);

FAQController.$inject = ['$scope', 'MOCKED_FAQS'];

function FAQController($scope, MOCKED_FAQS) {
    $scope.faqs = MOCKED_FAQS;

    $scope.myHTML =
            'I am an <code>HTML</code>string with ' +
            '<a href="#">links!</a> and other <em>stuff</em>';

    angular.forEach($scope.faqs, function (key, category) {
        category.isOpen = false;
        console.log(category)
        angular.forEach(category, function (f, ind) {
            f.isOpen = ind < 3;
        });
    });
}