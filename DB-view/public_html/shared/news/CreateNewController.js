(function () {
    'use strict';

    App.controller('CreateNewController', CreateNewController)

    CreateNewController.$inject = ['$scope', 'ValidationService', '$uibModalInstance'];

    function CreateNewController($scope, ValidationService, $uibModalInstance ) {
        console.log('INIT -- CreateNewController');

        var myValidation = new ValidationService();
        myValidation.setGlobalOptions({scope: $scope.newCreateForm, isolatedScope: $scope});


        $scope.nnew = {
            title: '',
            content: ''
        };
        $scope.cancel = function () {
             $uibModalInstance.dismiss();
        }
        $scope.submit = function () {
            if ($scope.newCreateForm.$valid) {

                 $uibModalInstance.close({
                    title: $scope.nnew.title,
                    content: $scope.nnew.content
                });
            } else {
                console.log("INVALID", $scope);
                $scope.displayValidationSummary = true;
            }
        }
    }
})();