(function () {
    'use strict';
    App.controller('FileUploadController', FileUploadController);

    FileUploadController.$inject = ['$scope', '$rootScope', 'Upload'];

    function FileUploadController($scope, $rootScope, Upload) {
        (function init() {
            console.log("FileUploadController init ::");
            if ($rootScope.listeners[$scope.broadcast]) {
                $rootScope.listeners[$scope.broadcast]();
            }
            $rootScope.listeners[$scope.broadcast] = $rootScope.$on($scope.broadcast, upload);
        })();

        function upload(id, data, callback) {
            console.log("Upload -> upload()");
            Upload.upload({
                url: '/upload/form',
                data: {
                    file: data.files, 
                    practiceId: data.practiceId,
                    type: data.type
                }
            }).then(function (resp) {
                console.log("OK");
                callback(resp);
            }, function (resp) {
                console.log('Error status: ', resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ', evt);
            });
        }
        ;
    }
})();