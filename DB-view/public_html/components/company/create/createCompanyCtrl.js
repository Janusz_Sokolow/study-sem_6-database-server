(function () {
    'use strict';

    App.controller('CreateCompanyController', CreateCompanyController)

    CreateCompanyController.$inject = ['$scope', 'ValidationService', 'companyService', 'customValidationService'];

    function CreateCompanyController($scope, ValidationService, companyService, customValidationService) {
        console.log('INIT -- CreateCompanyController');
        $scope.send = send;
        $scope.newCompany = {
            companyName: '',
            phoneNumber: '',
        }

        $scope.checkCompanynameUsed = checkCompanynameUsed;

        var myValidation = new ValidationService();
        myValidation.setGlobalOptions({scope: $scope.createCompanyForm, isolatedScope: $scope});

        function checkCompanynameUsed() {
            console.log("createCompany :: return :: CHECK")
            return customValidationService.checkCompanyExistsValidation($scope.newCompany.companyName);
        }

        function send() {
            if ($scope.createCompanyForm.$valid) {
                console.log("VALID", $scope);
                console.log("send() ", $scope.newCompany);
                companyService.createCompany($scope.newCompany).then(function (ok) {
                    if ($scope.modalCallback) {
                        $scope.modalCallback({
                            ok: ok,
                            companyName: $scope.newCompany.companyName
                        });
                    }
                }, function () {

                });
            } else {
                console.log("INVALID", $scope);
                $scope.displayValidationSummary = true;
            }
        }
    }

})();