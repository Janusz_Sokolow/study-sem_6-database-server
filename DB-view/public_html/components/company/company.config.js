(function () {
    'use strict';

    App.directive('createCompany', ['companyService', 'ValidationService', 'customValidationService', function () {
            return {
                scope: 'E',
                templateUrl: '/components/company/create/create-directive.html',
                controller: "CreateCompanyController"
            }
        }])
            .directive('companyOpinion', ['companyService', 'ValidationService', 'customValidationService', function () {
                    return {
                        scope: 'E',
                        templateUrl: '/components/company/showOpinion/show-view.html',
                        controller: "ShowCompanyOpinionsController"
                    }
                }]);
})();