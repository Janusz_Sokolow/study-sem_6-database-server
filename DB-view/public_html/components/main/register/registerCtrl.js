(function () {
    'use strict';

    App.controller('RegisterController', RegisterController)

    RegisterController.$inject = ['$scope', '$http', '$timeout', '$state', '$stateParams', 'ValidationService', 'studentService', 'customValidationService', 'FACULTIETIES'];

    function RegisterController($scope, $http, $timeout, $state, $stateParams, ValidationService, studentService, customValidationService, FACULTIETIES) {
        console.log('INIT -- RegisterController');

        $scope.updateMode = ($stateParams.nickName != '' && $stateParams.nickName != undefined ? true : false);
        $scope.checkUsernameUsed = checkUsernameUsed;
        $scope.checkPGIndexUsed = checkPGIndexUsed;
        $scope.register = register;
        $scope.update = update;
        $scope.loading = false;
        $scope.newStudent = {
            nickname: '',
            password: '',
            PGIndex: undefined,
            firstName: '',
            lastName: '',
            mail: '',
            speciality: ''
        };
        $scope.availableFacultities = FACULTIETIES.actual;
        var myValidation = new ValidationService();
        myValidation.setGlobalOptions({scope: $scope.createStudentForm, isolatedScope: $scope});

        (function () {
//            if ($scope.updateMode) {
//                console.log("Edytuję użytkownika : ", $stateParams.nickName);
//                studentService.getStudent($stateParams.nickName).then(function (student) {
//                    console.log("TEST ====  ", student);
//                    $scope.newStudent = student;
//                });
//
//            }
            $timeout(function () {
                $('#nickname').focus();
            });
        })();

        function checkUsernameUsed() {
            return customValidationService.checkUsernameExistsValidation($scope.newStudent.nickname);
        }
        function checkPGIndexUsed() {
            return customValidationService.checkPGIndexExistsValidation($scope.newStudent.PGIndex);
        }

        function register() {
            if ($scope.createStudentForm.$valid) {
                console.log("VALID", $scope);
                console.log("register() ", $scope.newStudent);
                $scope.loading = true;
                studentService.createStudent($scope.newStudent).then(function () {
                    $http({
                        method: 'POST',
                        url: '/api/auth',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        transformRequest: function (obj) {
                            var str = [];
                            for (var p in obj)
                                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                            return str.join("&");
                        },
                        data: {
                            password: $scope.newStudent.password,
                            login: $scope.newStudent.nickname
                        }
                    }).then(function successCallback(response) {
                        console.log("TEST ===== SUCCESS")
                        $state.go('app.main', {}, {reload: true}).then(function () {
                            location.reload();
                        });
                    }, function () {
                        console.log("TEST ===== ERROR");
                        $scope.loading = false;
                    });
                });
                ;
            } else {
                console.log("INVALID", $scope);
                alert("Uzupełnij wymagane pola");
            }
        }


        function update() {
            if ($scope.createStudentForm.$valid) {
                console.log("VALID", $scope);
                console.log("update() ", $scope.newStudent);
                studentService.updateStudent($scope.newStudent).then(function () {
                    $state.go('app.student.list');
                });
            } else {
                console.log("INVALID", $scope);
            }
        }
    }
})()