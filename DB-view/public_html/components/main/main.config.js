App
        .config(function ($stateProvider) {

            $stateProvider
                    .state('app.main', {
                        url: "/main",
                        controller: 'MainController',
                        templateUrl: 'components/main/main-view.html',
                        resolve: {
                            user: ['$q', '$timeout', 'Access', function ($q, $timeout, Access) {
                                    console.log("start resolve");
                                    return Access;
                                }]
                        }
                    })
                    .state('app.main.unregistrated', {
                        templateUrl: 'components/main/unregistrated-view.html',
//                url: "/main",
                        controller: 'UnregistratedController'
                    })
                    .state('app.main.student', {
                        templateUrl: 'components/main/student-view.html'
//                url: "/main",
//                controller: 'MainController'
                    })
                    .state('app.main.coordinator', {
                        templateUrl: 'components/main/coordinator-view.html'
//                url: "/main",
//                controller: 'MainController'
                    })
                    .state('app.main.admin', {
                        templateUrl: 'components/main/admin-view.html'
//                url: "/main",
//                controller: 'MainController'
                    })
                    .state('app.register', {
                        url: "/register",
                        controller: 'RegisterController',
                        templateUrl: 'components/main/register/register-view.html'
                    });

        });