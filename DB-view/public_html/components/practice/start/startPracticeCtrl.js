(function () {
    'use strict';

    App.controller('StartPracticeController', StartPracticeController)

    StartPracticeController.$inject = ['$scope', '$state', '$stateParams', 'ValidationService', 'companyService', 'practiceService', '$uibModal', 'PRACTICE_TYPES'];

    function StartPracticeController($scope, $state, $stateParams, ValidationService, companyService, practiceService, $uibModal, PRACTICE_TYPES) {
        console.log('INIT - StartPracticeController');

        $scope.send = send;
        $scope.loading = false;
        $scope.openCreationModal = openCreationModal;
        $scope.companies = [];
        $scope.types = PRACTICE_TYPES;
        $scope.newPractice = {
            dateFrom: new Date(),
            dateTo: null,
            type: '',
            company: undefined
        };

        var myValidation = new ValidationService();
        myValidation.setGlobalOptions({scope: $scope.startPracticeForm, isolatedScope: $scope});

        loadAvailableCompanies();

        function openCreationModal() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'components/company/create/modal/modal-view.html',
//                controller: 'ModalInstanceCtrl',
                size: 'md',
                scope: $scope
            });
            modalInstance.result.then(function (newCompanyName) {
                console.log("MODAL OK");
                loadAvailableCompanies(newCompanyName);
            }, function () {
                console.log("MODAL ERROR");
            });
            $scope.modalCallback = function (res) {
                console.log("IT's callback!!");
                if (res.ok) {
                    modalInstance.close(res.companyName);
                }
            }
        }

        function send() {
            console.log($state.current);
            if ($scope.startPracticeForm.$valid) {
                $scope.loading = true;
                console.log("VALID", $scope);
                practiceService.startPractice($stateParams.nickName || $scope.nickName, $scope.newPractice)
                        .then(function (ok) {
                            $state.go('^', $stateParams, {
                                reload: true, inherit: false, notify: true
                            });
                        });
            } else {
                console.log("INVALID", $scope);
            }
        }

        function loadAvailableCompanies(selectCompany) {
            companyService.findCompanyAbleToApply().then(function (result) {
                $scope.companies = result;
                if (selectCompany) {
                    for (var i = result.length - 1; i > 0; i--) {
                        if ($scope.companies[i].companyName === selectCompany) {
                            $scope.newPractice.companyId = $scope.companies[i].id;
                            break;
                        }
                    }
                }
            }, function (reply) {

            });
        }
    }
})()