(function () {
    'use strict';

    App.controller('DescribePracticeController', DescribePracticeController)

    DescribePracticeController.$inject = ['$scope', '$timeout', '$stateParams', '$uibModalInstance', 'practiceService', 'ValidationService'];

    function DescribePracticeController($scope, $timeout, $stateParams, $uibModalInstance, practiceService, ValidationService) {
        console.log('INIT - DescribePracticeController ' + $scope.practicePartID + " " + typeof $scope.practicePartID);

        $scope.addSkill = addSkill;
        $scope.removeSkill = removeSkill;

        $scope.inputPressed = function(event) {
            if (event.keyCode === 13) {
                addSkill();
                $timeout(function(){
                    $(event.delegateTarget).parent().first().next().find('input').focus();
                },1);
            }
        }
        $scope.send = send;
        $scope.newDescription = {
            note: '',
            skills: [""]
        };

        var practiceId = $stateParams.practiceId || $scope.practice.ID;
        var myValidation = new ValidationService();
        myValidation.setGlobalOptions({scope: $scope.startPracticeForm, isolatedScope: $scope});

        (function init() {
            practiceService.findPracticeDescriptions(practiceId).then(function (list) {
                console.log("DescribePracticeController :: init ", list, $scope.practicePartID);
                if ($scope.practicePartID) {
                    for (var i = 0, n = list.length; i < n; i++) {
                        if (list[i].PP_ID === $scope.practicePartID) {
                            $scope.newDescription.week = list[i].practiceWeek;
                            $scope.newDescription.note = list[i].workDescription;
                            $scope.newDescription.skills = list[i].newSkills;
                            break;
                        }
                    }
                    console.log("DescribePracticeController :: init ", $scope.newDescription);
                } else {
                    $scope.newDescription.week = list.length + 1;
                }
            }, function (err) {
                console.warn("DescribePracticeController :: init ", err);
            })
        })();

        $scope.cancel = function () {
            $uibModalInstance.dismiss();
        }
        $scope.submit = function () {
            practiceService.newPracticeDescription(practiceId, $scope.newDescription, $scope.practicePartID).then(function (ok) {
                if (ok) {
                    $uibModalInstance.close({
                        note: $scope.newDescription.note,
                        skills: $scope.newDescription.skills
                    });
                }
            });
        }

        function addSkill() {
            console.log("Dodaje");
            $scope.newDescription.skills.push("");
        }
        function removeSkill(index) {
            $scope.newDescription.skills.splice(index, 1);
        }

        function send() {
            if ($scope.describePracticeForm.$valid) {
                console.log("VALID", $scope);
            } else {
                console.log("INVALID", $scope);
            }
        }
    }
})()