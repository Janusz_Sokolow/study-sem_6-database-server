(function () {
    'use strict';

    App.controller('StatsCompanyController', StatsCompanyController)

    StatsCompanyController.$inject = ['$scope', 'companyService'];

    function StatsCompanyController($scope, companyService) {
        console.log('INIT -- StatsCompanyController');

        (function () {
            companyService.findCompanyStats().then(function (result) {
                $scope.foundCompanies = result;
            });
        })();
        
    }
})()