(function () {
    'use strict';

    App.controller('ListCompanyController', ListCompanyController)

    ListCompanyController.$inject = ['$scope', 'companyService'];

    function ListCompanyController($scope, companyService) {
        console.log('INIT -- ListCompanyController');

        $scope.removeCompany = removeCompany;
        
        (function () {
            companyService.findCompanies().then(function (result) {
                $scope.foundCompanies = result;
            });
        })();
        
        function removeCompany(companyName) {
            
            console.log("TEST - " + companyName);
            companyService.removeCompany(companyName);
        }
    }
})()