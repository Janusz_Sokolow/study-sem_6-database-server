(function () {
    'use strict';

    App.controller('CreateStudentController', CreateStudentController)

    CreateStudentController.$inject = ['$scope', '$state', '$stateParams', 'ValidationService', 'studentService', 'customValidationService', 'FACULTIETIES'];

    function CreateStudentController($scope, $state, $stateParams, ValidationService, studentService, customValidationService, FACULTIETIES) {
        console.log('INIT -- CreateStudentController');

        $scope.updateMode = ($stateParams.nickName != '' && $stateParams.nickName != undefined ? true : false);
        $scope.checkUsernameUsed = checkUsernameUsed;
        $scope.checkPGIndexUsed = checkPGIndexUsed;
        $scope.register = register;
        $scope.update = update;
        $scope.newStudent = {
            nickname: '',
            password: '',
            PGIndex: undefined,
            firstName: '',
            lastName: '',
            mail: '',
            speciality: ''
        };
        $scope.availableFacultities = FACULTIETIES.actual;
        var myValidation = new ValidationService();
        myValidation.setGlobalOptions({scope: $scope.createStudentForm, isolatedScope: $scope});

        (function () {
//            if ($scope.updateMode) {
//                console.log("Edytuję użytkownika : ", $stateParams.nickName);
//                studentService.getStudent($stateParams.nickName).then(function (student) {
//                    console.log("TEST ====  ", student);
//                    $scope.newStudent = student;
//                });
//
//            }
        })();

        function checkUsernameUsed() {
            return customValidationService.checkUsernameExistsValidation($scope.newStudent.nickname);
        }
        function checkPGIndexUsed() {
            return customValidationService.checkPGIndexExistsValidation($scope.newStudent.PGIndex);
        }

        function register() {
            if ($scope.createStudentForm.$valid) {
                console.log("VALID", $scope);
                console.log("register() ", $scope.newStudent);
                studentService.createStudent($scope.newStudent).then(function () {
                    $state.go('app.main');
                });
                ;
            } else {
                console.log("INVALID", $scope);
            }
        }


        function update() {
            if ($scope.createStudentForm.$valid) {
                console.log("VALID", $scope);
                console.log("update() ", $scope.newStudent);
                studentService.updateStudent($scope.newStudent).then(function () {
                    $state.go('app.student.list');
                });
            } else {
                console.log("INVALID", $scope);
            }
        }
    }
})()