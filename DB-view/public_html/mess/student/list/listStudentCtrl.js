(function () {
    'use strict';

    App.controller('ListStudentController', ListStudentController)

    ListStudentController.$inject = ['$scope', 'studentService', 'FACULTIETIES'];

    function ListStudentController($scope, studentService, FACULTIETIES) {
        console.log('INIT -- ListStudentController');

        $scope.filter = {speciality: ''};
        
        $scope.availableFacultities = FACULTIETIES.all;
        (function () {
            studentService.findStudent().then(function (result) {
                $scope.foundStudents = result;
            });
        })();
    }
})()